# Main SharkFest Site

This repository contains the assets for sharkfest.wireshark.org and a GitLab CI configuration that can be used for staging and production deployments.

Changes are automatically deployed to staging.sharkfest.wireshark.org and must be manually deployed to sharkfest.wireshark.org.

## Deploying To Staging

On each push to the main branch, GitLab CI runs the "Build site" job, which builds a static site in the top-level _public_ directory by running `npm run build`.
It then tars up the site and copies it to a Backblaze B2 endpoint and triggers a webhook on the server side that downloads and deploys the tarball.
Deployment takes about 4 minutes.

You can re-run the job manually via the [pipelines](https://gitlab.com/wireshark/sharkfest-web/-/pipelines) or [jobs](https://gitlab.com/wireshark/sharkfest-web/-/jobs) pages.

## Deploying To Production

Production deployment is done via the Deploy to production job.
It must be run manually via the [pipelines](https://gitlab.com/wireshark/sharkfest-web/-/pipelines) or [jobs](https://gitlab.com/wireshark/sharkfest-web/-/jobs) pages.

The public URL for the site is https://sharkfest.wireshark.org/.

The site is behind CloudFlare, so if you run into a caching issue you can bypass the cache by adding a parameter to the URL, e.g. https://www.sharkfestfoundation.org?cache=nope
Alternatively you can run the "Purge CloudFlare Cache" job.
