SharkFest'18 EUROPE Retrospective
=================================

October 29th - November 2nd, 2018  
Imperial Riding School Renaissance Hotel | Vienna, Austria

### Keynote Presentation

<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/FUrqlWv_K7Y" title="Wireshark and Open Source video" frameborder="0"  allowfullscreen style="position:absolute; top:0; bottom:0;"></iframe></div>

### SharkBytes

[Watch the SharkBytes from SharkFest'18 EUROPE](https://www.youtube.com/playlist?list=PLz_ZpPUgiXqN6BexvXHGiGLgI99hO5ofJ)

### Blogs

[Sharkfest'18 EUROPE Recap](https://t.co/n8DePR6oCu), by Jasper Bongertz  

### Sponsor Videos

### Wednesday Classes

*   01: [Back to the Basics](/retrospective/sfeu/presentations18eu/01-04.pptx), by Hansang Bae

*   [Presentation Video](https://youtu.be/y13zH-8OPE8 "Presentation video on YouTube") (1:10:05)

*   02: 802.11n/ac: complexity & solutions in capturing MIMO traffic, by Thomas d'Otreppe
*   03: [Writing a Wireshark Dissector: 3 Ways to Eat Bytes](/retrospective/sfeu/presentations18eu/03.7z), by Graham Bloice

*   [Presentation Video](https://www.youtube.com/watch?v=IbOVR7QQkvI "Presentation video on YouTube") (1:23:38)

*   04: [Back to the Trenches](/retrospective/sfeu/presentations18eu/01-04.pptx), by Hansang Bae

*   [Presentation Video](https://youtu.be/UlQtslfZFYA "Presentation video on YouTube") (1:12:12)

*   05: [Handcrafted Packets: build network packets with Scapy](/retrospective/sfeu/presentations18eu/05.pdf), by Uli Heilmeier

*   [Presentation Video](https://youtu.be/vuX0KZgNT94 "Presentation video on YouTube") (55:45)

*   06: Using More of the Features of Wireshark to Write Better Dissectors, by Richard Sharpe
*   07: TCP - Tips, Tricks & Traces, by Chris Greer
*   08: [Packet Analysis in the Cloud](/retrospective/sfeu/presentations18eu/08.pdf), by Matthew York

*   [Presentation Video](https://youtu.be/2XStp91GJmc "Presentation video on YouTube") (1:00:31)

*   09: [Crash Course: IPv6 and Network Protocols](/retrospective/sfeu/presentations18eu/09.pdf), by Johannes Weber

*   [Presentation Video](https://youtu.be/FBfCbCB_Ui4 "Presentation video on YouTube") (1:15:44)

*   10: The Unusual Suspects: open source tools for enhancing bigdata & network forensics analysis, by Jasper Bongertz

*   [Presentation Video](https://www.youtube.com/watch?v=a7q8WHTtPds "Presentation video on YouTube") (1:18:09)

*   11: [IoT - Buy and Install your own Destruction! (Part 1)](/retrospective/sfeu/presentations18eu/11-14.zip), by Phill Shade

*   [Presentation Video](https://youtu.be/wa9SCzyn4qc "Presentation video on YouTube") (1:35:59)

*   12: [Slow start and TCP Reno demystified: How congestion avoidance modes can influence a session](/retrospective/sfeu/presentations18eu/12.zip), by Christian Reusch
*   13: [Wireshark CLI Tools & Scripting](/retrospective/sfeu/presentations18eu/13.pdf), by Sake Blok
*   14: [IoT - Buy and Install your own Destruction!](/retrospective/sfeu/presentations18eu/11-14.zip) (Part 2), by Phill Shade

*   [Presentation Video](https://youtu.be/wa9SCzyn4qc "Presentation video on YouTube") (1:35:59)

*   15: [TLS 1.2/1.3 and Data Loss Prevention](/retrospective/sfeu/presentations18eu/15.pptx), by Ross Bagurdes

### Thursday Classes

*   16: SMB in the Star Wars Universe, by Eddi Blenkers
*   17: [To Send or Not to Send? How TCP congestion control algorithms work](/retrospective/sfeu/presentations18eu/17.pdf), by Vladimir Gerasimov

*   [Presentation Video](https://youtu.be/aoHXYLuU9-I "Presentation video on YouTube") (1:23:13)

*   18: Generating Wireshark Dissectors: A status report, by Richard Sharpe
*   19: [Hands-on Analysis of Multi-Point Captures](/retrospective/sfeu/presentations18eu/19.pdf), by Christian Landström
*   20: [TCP SACK Overview and Impact on Performance](/retrospective/sfeu/presentations18eu/20.zip), by John Pittle

*   [Presentation Video](https://youtu.be/B3dLe-jGyoM "Presentation video on YouTube") (1:15:44)

*   21: [sFlow: theory & practice of a sampling technology and its analysis with Wireshark](/retrospective/sfeu/presentations18eu/21.pdf), by Simone Mainardi

*   [Presentation Video](https://youtu.be/T8H1ZVvARYU "Presentation video on YouTube") (59:53)

*   22: Writing a TCP Analysis Expert System, by Jasper Bongertz
*   23: [BGP is not only a TCP Session: learning about the protocol that holds networks together](/retrospective/sfeu/presentations18eu/23.pdf), by Werner Fischer

*   [Presentation Video](https://youtu.be/CXlP0SbKPFo "Presentation video on YouTube") (1:10:47)

*   24: [Developer Bytes Lightning Talks](/retrospective/sfeu/presentations18eu/24.pptx), by Wireshark Core Developers
*   25: [Using Wireshark to Solve Real Problems for Real People: step- by-step case studies in packet analysis](/retrospective/sfeu/presentations18eu/25.pptx), by Kary Rogers

*   [Presentation Video](https://youtu.be/UBfSgjUCEi0 "Presentation video on YouTube") (1:14:49)

*   26: [Troubleshooting WLANs (Part 1): Layer 1 & 2 analysis using multi- channel hardware, Wi-Spy & Other Tools](/retrospective/sfeu/presentations18eu/26.pdf), by Rolf Leutert

*   [Presentation Video](https://youtu.be/dXbO-5_DkRI "Presentation video on YouTube") (1:10:26)

*   27: [IEEE802.11ac Debugging in a Windows Environment: new ways of debugging with Wireshark in a post-AirPcap era](/retrospective/sfeu/presentations18eu/27.zip), by Megumi Takeshita
*   28: The Packet Doctors are In! Packet trace examinations with the experts, by Drs. Bae, Bongertz, Landström, Blok, and Rogers
*   29: [Troubleshooting WLANs (Part 2): Using 802.11 management & control frames](/retrospective/sfeu/presentations18eu/29.pdf), by Rolf Leutert
*   30: [SSL/TLS decryption: uncovering secrets](https://lekensteyn.nl/files/wireshark-ssl-tls-decryption-secrets-sharkfest18eu.pdf), by Peter Wu

### Friday Classes

*   31: [Packet Monitoring in the Days of IoT and Cloud](https://www.dropbox.com/s/yr169s825v75vkr/Sharkfest_EU_2018.pdf?dl=0), by Luca Deri

*   [Presentation Video](https://youtu.be/irFTiNZRNj4 "Presentation video on YouTube") (1:16:32)

*   32: [Analyzing Kerberos with Wireshark](/retrospective/sfeu/presentations18eu/16.pdf), by Eddi Blenkers
*   33: [Reliable Packet Capture](/retrospective/sfeu/presentations18eu/33.pptx), by Christian Reusch || [Download Presentation Materials Here](/retrospective/sfeu/presentations18eu/33.pptx)
*   34: OPEN FORUM: Aha! Moments in Packet Analysis, by Chris Greer

*   [Presentation Video](https://www.youtube.com/watch?v=_v_oE7327eQ "Presentation video on YouTube") (1:15:57)

*   35: [TCP Split Brain: Compare/ Contrast TCP Effects on Client and Server with Wireshark](/retrospective/sfeu/presentations18eu/35.zip), by John Pittle

*   [Presentation Video](https://www.youtube.com/watch?v=rXH7svgisgw "Presentation video on YouTube") (1:13:53)

*   36: Developing an Online Packet Analysis Platform, by Oliver-Tobias Ripka