SharkFest'24 EUROPE Retrospective
=====================================

4 Nov - 8 Nov
Vienna, Austria

### **Keynote Presentations**

#### **[Ecosystem Expansion](/retrospective/sfeu/presentations24eu/gerald-keynote.pdf)**  
Gerald Combs & Core Developers
<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/_alpoB55oTY" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0;"></iframe></div>

<!-- ### [Presentation Video Playlist](https://www.youtube.com/playlist?list=PLz_ZpPUgiXqNAASkZ1wda6AOvoavtbgep) -->

### **Wednesday Classes**

*   01:  [3GPP, a walk through the LTE, and 5G networks](/retrospective/sfeu/presentations24eu/01.pptx), by Mark Stout
*   02:  [Logray: Or how to inspire your DevOps team to use Wireshark](/retrospective/sfeu/presentations24eu/02.pdf), by Uli Heilmeier
*   03:  [Mastering Wireshark Filtering](/retrospective/sfeu/presentations24eu/03.pdf), by Sake Blok
*   04:  [Automatically trigger captures via tcpdump when a suspicious event occurs in your Kubernetes cluster](/retrospective/sfeu/presentations24eu/04.pdf), by Thomas Labarussias
*   05:  [Capturing WiFi7, understand WiFi again with catching up an Extremely High Throughput mode of IEEE802.11be](/retrospective/sfeu/presentations24eu/05.zip), by Megumi Takeshita
*   06:  [Beyond Network Latency: Chasing Latency up the Stack](https://github.com/je-clark/sf24eu_latency), by Josh Clark
*   07:  [Communication breakdown - making online conferencing work in secured company networks](/retrospective/sfeu/presentations24eu/07.pdf), by Robert Hess
*   08:  [Unlocking Security Insights: Wireshark Techniques for Security Analysts](https://gitlab.com/aspampinato/sf24eu-presentation-files), by Walter Hofstetter
*   09:  [IPsec VPN Analysis and troubleshooting](/retrospective/sfeu/presentations24eu/09.zip), by Jean-Paul Archier
*   10:  [Kerberos Deep Dive](https://gitlab.com/aspampinato/sf24eu-presentation-files), by Eddi Blenkers
*   11:  [Let’s dissect malwares by collecting their syscalls with eBPF](https://gitlab.com/aspampinato/sf24eu-presentation-files), by Thomas Labarussias
*   12:  Deep packet inspection analyses: Unveiling a shocking RDP Attack through unusual protocol combinations, by Michał Sołtysik

### **Thursday Classes**
*   13: [A Deep Dive Into Traffic Fingerprints using Wireshark](https://www.dropbox.com/scl/fo/zm5amy8fkwz2pj3ojz12a/AMKbeuIToNPH9wCAqB1OWdQ?rlkey=ihnva3yz5heonw59m8br3lxvj&e=1&dl=0), by Luca Deri & Ivan Nardi
*   14: [Unveiling Network Errors: A Deep Dive into ICMP 'Destination Unreachable' Messages](/retrospective/sfeu/presentations24eu/14.pdf), by Johannes Weber
*   15: The Packet Doctors are in! Packet trace examinations with the experts
*   16: [VXLAN, EVPN and other intricacies unpacked](https://gitlab.com/aspampinato/sf24eu-presentation-files), by Pierre Besombes
*   17: [Advanced TCP Troubleshooting](/retrospective/sfeu/presentations24eu/17.pptx), by Jasper Bongertz
*   18: [Everything is encrypted](/retrospective/sfeu/presentations24eu/18.pdf), by André Luyer
*   19: [Dissector developer design notes](/retrospective/sfeu/presentations24eu/19.pdf), by Jaap Keuter
*   20: [Network traffic @ your home](https://gitlab.com/aspampinato/sf24eu-presentation-files), by Ville Haapakangas & Tom Cordemans
*   21: [Optimizing Server Settings Using Packet Captures](https://github.com/je-clark/sf24_eu_server_optimization), by Josh Clark
*   22: [Compare the accuracy of trace files captured with a tap and Cisco ACI](/retrospective/sfeu/presentations24eu/22.pptx), by Eddi Blenkers & Markus Liechti
*   23: [Dissecting the Client Hello with Pyshark](/retrospective/sfeu/presentations24eu/23.pdf), by Katherine Leese

### **Friday Classes**
*   24: Sharksniff 3000 - the Wireless Decrypting Cyberdeck, by Ross Bagurdes
*   25: [Cloud doesn't have Packets!](/retrospective/sfeu/presentations24eu/25.pptx), by Stephen Donnelly 
*   26: [Gotta catch 'em all! A field test of portable gigabit taps](/retrospective/sfeu/presentations24eu/26.pdf), by Sake Blok

* * *

#### Angel Shark Sponsors

[![](/img/sponsors/endace.png)](https://endace.com/) [![](/img/sponsors/profitap.png)](https://profitap.com/)[![](/img/sponsors/scos.png)](http://www.scos.nl/) [![](/img/sponsors/allegro.png)](https://allegro-packets.com/en/) [![](/img/sponsors/neox-networks.webp)](https://www.neox-networks.com/en/)