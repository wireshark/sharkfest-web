SharkFest'23 EUROPE Retrospective
=====================================

30 Oct - 3 Nov
Brussels, Belgium

### **Keynote Presentations**

#### **Latest Wireshark Developments**  
Gerald Combs & Friends
<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/wJkR0S1yEB4" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0;"></iframe></div>

<!-- ### [Presentation Video Playlist](https://www.youtube.com/playlist?list=PLz_ZpPUgiXqNAASkZ1wda6AOvoavtbgep) -->

### **Wednesday Classes**

*   01: WebRTC and the Art of Conferencing, by Matthias Kaiser
*   02: From Packets to System Calls: Evolving Cloud Visibility to Detect Attacks (part 1 of 2-part workshop), by Pablo Musa
*   03: Wireshark & AI, by Roland Knall
*   04: From Packets to System Calls: Evolving Cloud Visibility to Detect Attacks (part 2 of 2-part workshop), by Pablo Musa
*   05: Packet Capture in Public Cloud, by Stephen Donnelly
*   06: Analysing WebRTC connectvity issues on the packet level - a field report, by Robert Hess
*   07: Stepping up your packet analysis game, by leveraging the Wireshark CLI tools (part 1 of 2-part workshop), by Sake Blok
*   08: Real-world post-quantum TLS, by Peter Wu
*   09: Stepping up your packet analysis game, by leveraging the Wireshark CLI tools (part 2 of 2-part workshop), by Sake Blok
*   10: Multicast & Broadcast Reconnaissance - What Can a Hacker Learn From Your Packets?, by Betty DuBois

### **Thursday Classes**
*   11: Sniffing in da cloud: A IaC blueprint, by Uli Heilmeier
*   12: Where does the feed from home cameras go? Traffic analysis and discussions on data privacy and security, by Andrea Cyber-Monitor
*   13: The Packet Doctors are in! Packet trace examinations with the experts
*   14: Live capture in containers from the comfort of your desktop Wireshark, by Harald Albracht
*   15: Bake your own Pi: Building a TLS Decrypting Wireless Traffic Sniffer, by Ross Bagurdes
*   16: Wireshark on Rails: A Tale of Trains, Taps, and Teamwork, by Eddi Blenkers and Dirk Haseloff
*   17: Network Security Monitoring with Wireshark, by Christian Landström
*   18: ChatGPT in Wireshark, by Megumi Takeshita
*   19: It's Encrypted - now What? - Evaluating Encrypted Traffic

### **Friday Classes**
*   20: Empowering Higher Education & Research with Wireshark: Innovative Approaches & Best Practices, by Ville Haapakangas & Tom Cordemans
*   21: Unraveling the Packet Mysteries: A Wireshark Journey with Machine Learning! (part 1 of 2-part workshop), by Dr. Nathanael Weill
*   22: It's Always DNS!, by Johannes Weber
*   23: Unraveling the Packet Mysteries: A Wireshark Journey with Machine Learning! (part 2 of 2-part workshop), by Dr. Nathanael Weill
*   24: Understanding and troubleshooting IPsec VPNs, by Jean-Paul Archier
*   25: Weird captures, what can you do, by André Luyer 

* * *


#### Angel Shark Sponsors

[![](/img/sponsors/b-yond.webp)](https://www.b-yond.com/) [![](/img/sponsors/endace.png)](https://endace.com/) [![](/img/sponsors/kadiska.png)](https://kadiska.com/) [![](/img/sponsors/liveaction.png)](https://www.liveaction.com/) [![](/img/sponsors/profitap.jpg)](https://profitap.com/)[![](/img/sponsors/scos.png)](http://www.scos.nl/)