SharkFest'16 EUROPE Retrospective
=================================

October 17-19  
Papendal Hotel | Arnhem, the Netherlands

### **Keynote Presentation**

**Wireshark Behind The Scenes**  
Gerald Combs

<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/K7vb8YoeEyA" title="Wireshark Behind The Scenes video" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0;"></iframe></div>

### **Blogs**

[SharkFest '16 Europe Recap](https://blog.packet-foo.com/2016/10/sharkfest-europe-2016-retrospective/), by Jasper Bongertz  
[The Network Capture Playbook Part 1](https://blog.packet-foo.com/2016/10/the-network-capture-playbook-part-1-ethernet-basics/), by Jasper Bongertz  
[The Network Capture Playbook Part 2](https://blog.packet-foo.com/2016/10/the-network-capture-playbook-part-2-speed-duplex-and-drops/), by Jasper Bongertz  
[The Network Capture Playbook Part 3](https://blog.packet-foo.com/2016/11/the-network-capture-playbook-part-3-network-cards/), by Jasper Bongertz

### **Tuesday Classes**

*   01: [In the Packet Trenches (Part 1)](/retrospective/sfeu/presentations16eu/01.pptx), by Hansang Bae

*   [Presentation Video](https://youtu.be/Gs6Ogvh7uKY "Presentation video on YouTube") (1:16:32)

*   [Presentation Materials](/retrospective/sfeu/presentations16eu/01.zip)

*   02: [Scaling Up: How to Profitably use Wireshark for Analyzing Large Traces & High-Speed Network Links](/retrospective/sfeu/presentations16eu/02.pdf), by Luca Deri
*   03: [In the Packet Trenches (Part 2)](/retrospective/sfeu/presentations16eu/01.pptx), by Hansang Bae

*   [Presentation Materials](/retrospective/sfeu/presentations16eu/01.zip)

*   [Presentation Video](https://youtu.be/Gs6Ogvh7uKY?t=51m54s "Presentation video on YouTube") (1:16:32)

*   04: [Common Packets in a Windows/Active Directory Environment: How a Windows Client Communicates in an Enterprise Network](/retrospective/sfeu/presentations16eu/04.pdf), by Ulrich Heilmeier
*   05: [Tackling the Haystack: How to Process Large Numbers of Packets](/retrospective/sfeu/presentations16eu/05.pdf), by Jasper Bongertz

*   [Presentation Video](https://youtu.be/ec3UOed2knk "Presentation video on YouTube") (1:19:36)

*   06: [Troubleshooting 802.11 with Monitoring Mode: Finding Patterns in your pcap](/retrospective/sfeu/presentations16eu/06.pdf), by Thomas Baudelet
*   07: [SSL troubleshooting with Wireshark (and Tshark)](/retrospective/sfeu/presentations16eu/07.pdf), by Sake Blok

*   [Presentation Video](https://youtu.be/oDaDY9QCnXk "Presentation video on YouTube") (1:15:44)

*   08: [Wireshark as a Spy Watermark Pen: Decrypting & Retrieving Information from Packets](/retrospective/sfeu/presentations16eu/08.pdf), by Megumi Takeshita

*   [Presentation Materials](/retrospective/sfeu/presentations16eu/08.zip)

*   09: [The Little Thing Called “Micro Burst”: Why Massive Packet Loss can occur even in Low- Bandwidth-Usage Scenarios](/retrospective/sfeu/presentations16eu/09.pptx), by Christian Reusch

*   [Presentation Video](https://youtu.be/KbXc3c_Uboo "Presentation video on YouTube") (0:51:36)
*   [Presentation Materials](/retrospective/sfeu/presentations16eu/09.zip)

*   10: [Dissecting Man-on-the Side Attacks: Analysis of Active TCP Packet Injection Attacks](/retrospective/sfeu/presentations16eu/10.pdf), by Erik Hjelmvik

### **Wednesday Classes**

*   11: [Forensic Network Analysis in the Time of APTs](/retrospective/sfeu/presentations16eu/11.pdf), by Christian Landström

*   [Presentation Video](https://youtu.be/UbNqfQFQHZw "Presentation video on YouTube") (1:17:06)

*   12: [Writing a Wireshark Dissector using WSGD, Lua & C: 3 Ways to Eat Bytes](/retrospective/sfeu/presentations16eu/12.zip), by Graham Bloice
*   13: [Using Wireshark to Solve Real Problems for Real People: Step-by-Step Real-World Case Studies in Packet Analysis](/retrospective/sfeu/presentations16eu/13.pptx), by Kary Rogers
*   14: [Viewing Snort Alerts using Wireshark: Understanding Alerts and Using the Snort Dissector to See Why & How Snort Detects Them](/retrospective/sfeu/presentations16eu/14.pptx), by Martin Mathieson
*   15: [Troubleshooting WLANs (Part 1): Layer 1 & 2 Analysis using WiSpy & AirPcap](/retrospective/sfeu/presentations16eu/15.pdf), by Rolf Leutert

*   [Presentation Video](https://youtu.be/do8yMqguPDk "Presentation video on YouTube") (1:14:23)

*   16: [Top 5 False Positives: Knowing When a Finding Doesn't Help](/retrospective/sfeu/presentations16eu/16.pdf), by Jasper Bongertz
*   17: [Troubleshooting WLANs (Part 2): Layer 1 & 2 Analysis using WiSpy & AirPcap](/retrospective/sfeu/presentations16eu/17.pdf), by Rolf Leutert

*   [Presentation Video](https://youtu.be/rU-PR1p1Njc "Presentation video on YouTube") (1:12:01)

*   18: [Troubleshooting with Layer 2 Control Protocols](/retrospective/sfeu/presentations16eu/18.pdf), by Werner Fischer
*   19: [Windows Filesharing De-Mystified: SMB with a Eureka! Effect](/retrospective/sfeu/presentations16eu/19.pdf "Presentation slides"), by Eduard Blenkers

*   [Presentation Video](https://www.youtube.com/watch?v=lbTsiAnhfQk "Presentation video on YouTube") (1:04:07)

*   20: [Inside the Wireshark Core Developer Team: How Wireshark Development Works!](/retrospective/sfeu/presentations16eu/20.pdf "Presentation slides"), by Alexis LaGoutte
