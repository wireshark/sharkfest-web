SharkFest'17 EUROPE Retrospective
=================================

November 7-10, 2017  
Palacio Estoril Hotel | Portugal

### Keynote Presentation

#### **Wireshark and Expectations**  
Gerald Combs

<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/HiofIifbitQ" title="Wireshark and Expectations video" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0;"></iframe></div>

### SharkFest'17 Europe Movie

<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/WyIp_c5Rax4" title="YouTube video player" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0;"></iframe></div>

### Blogs

[Sharkfest '17 Europe - First Thoughts](https://t.co/hnF26RYjT4?ssr=true), by Paul Offord  
[Sharkfest 2017 EU Recap](https://blog.packet-foo.com/2017/12/sharkfest-2017-eu-recap/), by Jasper Bongertz  

### Wednesday Classes

*   01: [Hands-on TCP Analysis: Packets, Sequences & Fun](/retrospective/sfeu/presentations17eu/01.pdf), by Jasper Bongertz

*   [Presentation Video](https://youtu.be/R3nuuZiIbgU "Presentation video on YouTube") (1:21:19)

*   02: [Using Wireshark to Solve Real Problems for Real People: Step-by Step Case Studies in Packet Analysis](/retrospective/sfeu/presentations17eu/02.pdf), by Kary Rogers
*   03: [Writing a Wireshark Dissector: 3 Ways to Eat Bytes](/retrospective/sfeu/presentations17eu/03.7z), by Graham Bloice
*   04: [Augmenting Packet Capture with Contextual Meta-Data: the What, Why & How](/retrospective/sfeu/presentations17eu/04.pptx), by Dr. Stephen Donnelly

*   [Presentation Video](https://youtu.be/V88t6AnHyiY "Presentation video on YouTube") (54:58)

*   05: [Troubleshooting WLANs (Part 1): Layer 1 & 2 Analysis Using AirPcap, Wi-Spy & Other Tools](/retrospective/sfeu/presentations17eu/05.pdf), by Rolf Leutert
*   06: [Generating Wireshark Dissectors from XDR Files: Why you don’t want to write them, by hand](/retrospective/sfeu/presentations17eu/06.pdf), by Richard Sharpe
*   07: [SMB/CIFS Analysis: Using Wireshark to Efficiently Analyze & Troubleshoot SMB/CIFS](https://start.me/p/Vw0GEN/sharkfest), by Betty DuBois
*   08: [Troubleshooting WLANs (Part 2): Troubleshooting WLANs Using 802.11 Management & Control Frames](/retrospective/sfeu/presentations17eu/08.pdf), by Rolf Leutert
*   09: Developer Bytes Lightning Talks–Development Track, by Wireshark Core Developers
*   10: SMB Handshake: The Devil Lies in the Detail, by Eduard Blenkers

*   [Presentation Video](https://youtu.be/L5U9dPR4UCY "Presentation video on YouTube") (56:15)

*   11: Wireshark & Time: Accurate Handling of Time when Capturing Frames, by Werner Fischer
*   12: Developer Bytes Lightning Talks–Usage Track, by Wireshark Core Developers
*   13: [Practical Tracewrangling: exploring trace file manipulation/extraction scenario](/retrospective/sfeu/presentations17eu/13.pdf), by Jasper Bongertz

*   [Presentation Video](https://youtu.be/xzCNEB40DT0 "Presentation video on YouTube") (1:06:08)

*   14: [Transmission Control Protocol Illustrated: everything you always wanted to know about TCP\* (\*but were afraid to ask)](/retrospective/sfeu/presentations17eu/14.pdf), by Ulrich Heilmeier
*   15: [SSL/TLS Decryption: uncovering secrets](/retrospective/sfeu/presentations17eu/15.pdf), by Peter Wu

### Thursday Classes

*   16: [My TCP ain’t your TCP: Stack behavior back then, now and in the future](/retrospective/sfeu/presentations17eu/16.pdf), by Simon Lindermann

*   [Presentation Video](https://youtu.be/8RI9wtmcpeY "Presentation video on YouTube") (1:07:39)

*   17: [Troubleshooting 802.11 with Monitoring Mode: Finding Patterns in your pcap](/retrospective/sfeu/presentations17eu/17.pdf), by Thomas Baudelet
*   18: extcap - Packet Capture beyond libpcap/winpcap: Bluetooth sniffing, Android dumping & other fun stuff, by Roland Knall
*   19: [Turning Wireshark into a Traffic Monitoring Tool: Moving from packet details to the big picture](/retrospective/sfeu/presentations17eu/19.pdf), by Luca Deri

*   [Presentation Video](https://youtu.be/dp5FJMhxzio "Presentation video on YouTube") (1:13:11)

*   20: QUIC Dissection: Using Wireshark to Understand QUIC Quickly, by Megumi Takeshita
*   [PDF](/retrospective/sfeu/presentations17eu/20.pdf)[Supplemental Files](/retrospective/sfeu/presentations17eu/20.zip)
*   21: [Custom LUA dissectors to the rescue in root cause analysis](/retrospective/sfeu/presentations17eu/21.pdf), by Sake Blok
*   22: [Troubleshooting Layer 7 with Wireshark: because you don’t know what you don’t know](https://start.me/p/Vw0GEN/sharkfest), by Betty DuBois
*   23: [The Network is Slow! Finding the Root Cause of Slow Application Performance](/retrospective/sfeu/presentations17eu/23.pdf), by Lorna Robertshaw
*   24: [Slow Start & TCP Reno Demystified: How Congestion Avoidance Modes are Working](/retrospective/sfeu/presentations17eu/24.zip), by Christian Reusch
*   25: Hands-On Analysis of Multi-Point Captures, by Christian Landström
*   26: [How Did They Do That? Network Forensic Case Studies](/retrospective/sfeu/presentations17eu/26.pdf), by Phill Shade
*   27: Developer Bytes Lightning Talks–Development Track, by Wireshark Core Developers
*   28: [Designing a Requirements-based Packet Capture Strategy](/retrospective/sfeu/presentations17eu/28.pdf), by John Pittle

*   [Presentation Video](https://youtu.be/8edHFtbDOTU "Presentation video on YouTube") (1:18:32)

*   29: The Packet Doctors are In!, by Drs. Bongertz, Landström, Blok
*   30: Developer Bytes Lightning Talks–Usage Track, by Wireshark Core Developers

### Friday Classes

*   31: New Ways to Find the Cause of Slow Response Times, by Paul Offord

*   [Presentation Video](https://youtu.be/aehMMf0VpNg "Presentation video on YouTube") (1:10:46)

*   32: Real World Troubleshooting Tales, by Graeme Bailey
*   33: [TShark Command Line using PowerShell](/retrospective/sfeu/presentations17eu/33.7z), by Graham Bloice
*   34: [TCP Analysis – When Things Get Complicated](/retrospective/sfeu/presentations17eu/34.pdf), by Jasper Bongertz

*   [Presentation Video](https://youtu.be/Tz6IfyfodKo "Presentation video on YouTube") (1:17:16)

*   35: [Sneaking in, by the Back Door – Hacking the Non-Standard Layers with Wireshark (BYOD)](/retrospective/sfeu/presentations17eu/35.pdf), by Phill Shade
*   36: Introduction to ICS Protocols, by Thomas Bringewald & Lars Seifert
