SharkFest'21 Virtual EUROPE Retrospective
=========================================

June 14th - 18th, 2021  
Online

### Gerald Combs Keynote: Latest Wireshark Developments & Road Map

[Presentation PDF](/retrospective/sfeu/presentations21eu/gerald-keynote.pdf)

### Guillaume Valadon Keynote: Scapy Turned 18. Boy They Grow Up Fast, Don’t They!

<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/krZ3fOCTlfs" title="Guillaume Valadon Keynote video" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0;"></iframe></div>

### Thursday Classes

*   01: [Know your preferences](/retrospective/sfeu/presentations21eu/01.pdf), by Uli Heilmeier

*   [Presentation Video](https://www.youtube.com/watch?v=l0LiNiIZjCU "Presentation video on YouTube")

*   02: [Analysis and Troubleshooting of IPsec VPNs](/retrospective/sfeu/presentations21eu/02.pdf), by Jean-Paul Archier

*   [Presentation Video](https://www.youtube.com/watch?v=KXVVrX_XECQ "Presentation video on YouTube")

*   03: [Chasing application performance with Wireshark](/retrospective/sfeu/presentations21eu/03.pdf), by Matthias Kaiser

*   [Presentation Video](https://www.youtube.com/watch?v=ZPoLa42Qjo8 "Presentation video on YouTube")

*   04: [Automate your Analysis: tshark, the Swiss army knife](/retrospective/sfeu/presentations21eu/04.zip), by André Luyer

*   [Presentation Video](https://youtu.be/1TxAq1xIj1M "Presentation video on YouTube")

*   05: [DDoS from the packet level](/retrospective/sfeu/presentations21eu/05.pdf), by Eddi Blenkers
*   06: [When It's NOT a "Network Problem" - Identifying Higher-Layer Issues in Packet Data](/retrospective/sfeu/presentations21eu/06.pdf), by Wes Morgan

*   [Presentation Video](https://www.youtube.com/watch?v=nuLyzMZp0V8 "Presentation video on YouTube")

*   07: [Network Forensic Case Studies: Those Who Don't Learn from the Past Are Doomed to Repeat It](/retrospective/sfeu/presentations21eu/07.pdf), by Phill Shade

*   [Presentation Video](https://www.youtube.com/watch?v=MFvKkq_MEWQ "Presentation video on YouTube")

*   08: [Back to the Packet Trenches](/retrospective/sfeu/presentations21eu/08.zip), by Hansang Bae

*   [Presentation Video](https://www.youtube.com/watch?v=CT-JdEXaY64 "Presentation video on YouTube")

*   09: Intro to QUIC - The TCP Killer?, by Chris Greer

*   [Presentation Video](https://www.youtube.com/watch?v=seRVM8sEf0g "Presentation video on YouTube")

*   10: [Introduction to WAN Optimization](/retrospective/sfeu/presentations21eu/10.pdf), by John Pittle

*   [Presentation Video](https://www.youtube.com/watch?v=jWUaVbwcSLQ "Presentation video on YouTube")

### Friday Classes

*   11: How long is a packet? And does it really matter?, by Dr. Stephen Donnelly

*   [Presentation Video](https://www.youtube.com/watch?v=BMtH9WiKxc8 "Presentation video on YouTube")

*   12: [How to analyze SACK and DSACK with Wireshark](/retrospective/sfeu/presentations21eu/12.pptx), by Christian Reusch
*   13: Make the bytes speak to you, by Roland Knall

*   [Presentation Video](https://www.youtube.com/watch?v=NNLvNCFhkbU "Presentation video on YouTube")

*   14: The Packet Doctors are in! Packet trace examinations with the experts, by Drs. Bae, Blok, Bongertz, and Landström
*   15: [Cybersecurity-oriented Network Traffic Analysis, by Luca Deri, Matteo Biscose, and Martin Scheu](/retrospective/sfeu/presentations21eu/15.zip)
[](/retrospective/sfeu/presentations21eu/15.zip)

[](/retrospective/sfeu/presentations21eu/15.zip)*   [](/retrospective/sfeu/presentations21eu/15.zip)[Presentation Video](https://www.youtube.com/watch?v=YX1JG96GFlE "Presentation video on YouTube")

*   16: [Dissecting WiFi6 using Wireshark, by Megumi Takeshita](/retrospective/sfeu/presentations21eu/16.pdf)
[](/retrospective/sfeu/presentations21eu/16.pdf)

[](/retrospective/sfeu/presentations21eu/16.pdf)*   [](/retrospective/sfeu/presentations21eu/16.pdf)[Presentation Video](https://youtu.be/-zL-bpW0jaY "Presentation video on YouTube")

*   [Presentation Video](https://www.youtube.com/watch?v=-zL-bpW0jaY "Presentation video on YouTube")

*   17: Discovering IPv6 with Wireshark, by Rolf Leutert

*   [Presentation Video](https://www.youtube.com/watch?v=B8bNidd7Kdc "Presentation video on YouTube")

*   18: [Trace Files Case Files](/retrospective/sfeu/presentations21eu/18.pdf), by Jasper Bongertz

*   [Presentation Video](https://www.youtube.com/watch?v=9ZVla0uIlY0 "Presentation video on YouTube")

*   19: Walk through of the creation and challenges of the CTF, by Sake Blok
*   20: imnurnet - Exploiting Your IPv4 Network with IPv6, by Jeff Carrell

*   [Presentation Video](https://www.youtube.com/watch?v=YPq4e8kn9yk "Presentation video on YouTube")
