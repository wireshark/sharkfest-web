SharkFest'22 EUROPE Retrospective
=====================================

Oct 31 - Nov 4
Estoril, Portugal

### **Keynote Presentations**

#### **Latest Wireshark Developments**  
Gerald Combs & Friends
<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/XLWziy_kxHQ" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0;"></iframe></div>

### [Presentation Video Playlist](https://www.youtube.com/playlist?list=PLz_ZpPUgiXqNAASkZ1wda6AOvoavtbgep)

### **Wednesday Classes**

*   01: [Network Troubleshooting from Scratch](/retrospective/sfus/presentations23/01.pdf), by Jasper Bongertz
*   [Presentation Video](https://youtu.be/xaWZITDbTrw "Presentation video on YouTube")

*   02: Chasing packet loss of TCP based applications using Wireshark, by George Cragg
*   03: Intro to QUIC: The TCP Killer?, by Chris Greer
*   04: Wireshark and Mitre Attack, by Matteo Biscosi & Marco Favilli
*   05: Wild PCAPs: The weird stuff is in the weeds, by Chris Bidwell
*   06: Introduction to WiFi Security (session 1), by Thomas d'Otreppe
*   07: [Wireshark at Enterprise Scale](/retrospective/sfus/presentations22/07.pptx), by Dr. Stephen Donnelly
*   08: Introduction to WPA Enterprise Exploitation (session 2), by Thomas d'Otreppe
*   09: Ask the Experts: Wireshark Q&A: New Features, Feature Requests, Bug Reports
*   10: Decrypt Kerberos/NTLM “encrypted stub data” in Wireshark, by Clément Notin

### **Thursday Classes**
*   11: Contribute to Wireshark - the low hanging fruits, by Uli Heilmeier
*   12: Advanced IEC 60870-5-104 analysis with Wireshark, by Martin Scheu
*   13: The Packet Doctors are in! Packet trace examinations with the experts
*   14: [Automate your Analysis, tshark, the Swiss army knife](14.zip), by Andre Luyer
*   15: [Spotting Hacking Attacks in a Trace File](/retrospective/sfus/presentations22/15.pdf), by Eddi Blenkers
*   16: IPv6 Crash Course, by Johannes Weber
*   17: Security Monitoring for SMBs, by Christian Landström
*   18: [TopN analysis using Wireshark](/retrospective/sfus/presentations22/18.zip), by Megumi Takeshita
*   19: LOG4SHELL: Getting to know your adversaries, by Sake Blok

### **Friday Classes**
*   20: Understanding TCP Throughput, by Kary Rogers
*   21: Learning Bluetooth Low Energy with Wireshark, by Ville Haapakangas
*   22: Wireshark with LTE and 5G Packet Core, by Mark Stout
*   23: Visualizing and Decrypting TLS 1.3, by Ross Bagurdes

* * *

#### Host Sponsor

[![](/img/sponsors/sysdig.png)](https://www.sysdig.com/)

#### Angel Shark Sponsors

[![](/img/sponsors/allegro.png)](https://www.allegropackets.com/) [![](/img/sponsors/endace.png)](https://endace.com/) [![](/img/sponsors/profitap.jpg)](https://profitap.com/)[![](/img/sponsors/scos.png)](http://www.scos.nl/)

#### Honorary Sponsor

[![](/img/sponsors/networkdatapedia.png)](https://www.networkdatapedia.com/) 