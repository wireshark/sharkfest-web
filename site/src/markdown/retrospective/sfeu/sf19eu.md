SharkFest'19 EUROPE Retrospective
=================================

November 4th - 8th, 2019  
Palacio Estoril Hotel | Estoril, Portugal

### Keynote Presentation

#### **Wireshark and Open Source**  
Gerald Combs

<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/9azl2wxnSoE" title="Wireshark and Open Source video" frameborder="0"  allowfullscreen style="position:absolute; top:0; bottom:0;"></iframe></div>

### Wednesday Classes

*   01: Back to the Packet Trenches, by Hansang Bae
*   02: [Troubleshooting WLANs (Part 1): Layer 1 & 2 analysis using multi-channel hardware, Wi-Spy & other tools](/retrospective/sfeu/presentations19eu/02.pdf), by Rolf Leutert

*   [Presentation Video](https://www.youtube.com/watch?v=N_Ybv7EfAk8 "Presentation video on YouTube") (1:18:32)

*   03: [Writing a Wireshark Dissector: 3 Ways to Eat Bytes](/retrospective/sfeu/presentations19eu/03.zip), by Graham Bloice
*   04: [How Long is a Packet?](/retrospective/sfeu/presentations19eu/04.pptx), by Stephen Donnelly

*   [Presentation Video](https://youtu.be/1Rbx_LRPABM "Presentation video on YouTube") (56:27)

*   05: [Troubleshooting WLANs (Part 2): Layer 1 & 2 analysis using multi-channel hardware, Wi-Spy & other tools, by Rolf Leutert](/retrospective/sfeu/presentations19eu/05.pdf)

*   [Presentation Video](https://www.youtube.com/watch?v=Ku68vxdowQQ "Presentation video on YouTube") (1:12:35)

*   06: Creating dissectors like apro, by generating dissectors, by Richard Sharpe
*   07: Solving (SharkFest) packet challenges using tshark alone, by Sake Blok

*   [Presentation Video](https://www.youtube.com/watch?v=PaUX5YT7uj0 "Presentation video on YouTube") (1:11:19)

*   08: Audio & Video with Wireshark, by Megumi Takeshita \[[Traces](http://www.ikeriri.ne.jp/wireshark/sf19eu-ikeriri-traces.zip)\] \[[Tools](http://www.ikeriri.ne.jp/wireshark/sf19eu-ikeriri-tools.zip)\]

*   [Presentation Video](https://www.youtube.com/watch?v=JTdbQSm5MXo "Presentation video on YouTube") (1:18:22)

*   09: Schrödinger‘s packets: they lie as long as they don’t lie, by Uli Heilmeier
*   10: Tracing Uncharted Networks, by Eddi Blenkers

*   [Presentation Video](https://www.youtube.com/watch?v=x_Jj-abSzM4 "Presentation video on YouTube") (53:39)

*   11: [TCP Split Brain: Compare/contrast TCP effects on client & server with Wireshark (Part 1)](/retrospective/sfeu/presentations19eu/11.pdf), by John Pittle

*   [Presentation Video](https://www.youtube.com/watch?v=BMtH9WiKxc8 "Presentation video on YouTube") (1:08:59)

*   12: BACnet & Wireshark for beginners, by Werner Fischer
*   13: [Packet-less traffic analysis using Wireshark](https://www.ntop.org/wp-content/uploads/2019/11/Sharkfest_EU_2019.pdf), by Luca Deri & Samuele Sabella

*   [Presentation Video](https://www.youtube.com/watch?v=6N72dSXdPJY "Presentation video on YouTube") (1:12:44)

*   14: [TCP Split Brain: Compare/contrast TCP effects on client & server with Wireshark (Part 2)](/retrospective/sfeu/presentations19eu/14.pdf), by John Pittle

*   [Presentation Video](https://www.youtube.com/watch?v=gOcP_pWkWsU&t=4308s "Presentation video on YouTube") (1:22:21)

*   15: TLS 1.2/1.3 and Data Loss Prevention, by Ross Bagurdes

### Thursday Classes

*   16: War story: troubleshooting issues on encrypted links, by Christian Landström
*   17: [Wireshark as a part of your DevSecOps Cycle](/retrospective/sfeu/presentations19eu/17.pptx), by Milorad Imbra
*   18: Reliable Packet Capture, by Christian Reusch
*   19: EXPERT PANEL: Pros & Cons of Building your own Capture Appliance, by Sake Blok, Jasper Bongertz, Hansang Bae, Luca Deri, and Chris Greer

*   [Presentation Video](https://www.youtube.com/watch?v=fmVMkyQqDzg "Presentation video on YouTube") (1:13:18)

*   20: [Automate your analysis: tshark, the Swiss army knife](/retrospective/sfeu/presentations19eu/20.pdf), by Andre Luyer
*   21: Analysis & Troubleshooting of IPsec VPNs, by Jean-Paul Archier
*   22: Using Wireshark to solve real problems for real people: step-by-step case studies in packet analysis, by Kary Rogers
*   23: Is it the network? (Part 1), by Matthias Kaiser

*   [Presentation Video](https://www.youtube.com/watch?v=eJlpz66r2yI "Presentation video on YouTube") (1:18:22)

*   24: IPv6 Crash Course:Understanding IPv6 as seenon the wire, by Johannes Weber
*   25: The Packet Doctors are In!, by Drs. Bae, Blok, Bongertz, Landström & Rogers
*   26: Is it the network? (Part 2), by Matthias Kaiser

*   [Presentation Video](https://www.youtube.com/watch?v=dDv1xF_MqS4 "Presentation video on YouTube") (1:25:42)

*   27: WiFi Security 101 (Part 1, by Thomas D'Otreppe
*   28: Case studies of a cloud-based packet analysis and learning platform, by Oliver-Tobias Ripka
*   29: Troubleshooting Cisco Software-Defined Access architectures with Wireshark, by Josh Halley

*   [Presentation Video](https://www.youtube.com/watch?v=G7lbAsq87pw "Presentation video on YouTube") (1:13:07)

*   30: WiFi Security 101 (Part 2, by Thomas D'Otreppe

### Friday Classes

*   31: Gentlemen’s Software Set for Transport Protocols Testing & Learning, by Vladimir Gerasimov
*   32: [Green Locks are not enough: Plaintext information in network protocols](https://www.ntop.org/wp-content/uploads/2019/11/Plaintext-Information-In-Network-Protocols-2.pdf), by Simone Mainardi
*   33: [Analysing VoIP Protocols: Discover Wireshark’s numerous features to troubleshoot VoIP](/retrospective/sfeu/presentations19eu/33.pdf), by Rolf Leutert
*   34: LTE Explained...The Other Protocols, by Mark Stout
*   35: [Debugging TLS issues with Wireshark](https://lekensteyn.nl/files/wireshark-tls-debugging-sharkfest19eu.pdf), by Peter Wu
*   36: Automating cloud infrastructure for packet capture and analysis, by Brad Palm & Ryan Richter