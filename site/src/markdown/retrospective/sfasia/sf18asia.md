
SharkFest'18 ASIA Retrospective
=================================

April 9th - 11th, 2018  
Nanyang Technological University, Singapore

### Keynote Presentation
#### Wireshark: Past, Present & Future
Gerald Combs  
  
<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/IZ0kmvQL5JM" title="Wireshark and Open Source video" frameborder="0"  allowfullscreen style="position:absolute; top:0; bottom:0;"></iframe></div>

**Tuesday Classes**
-------------------

*   01: In the Packet Trenches - Part 1, by Hansang Bae
*   02: [Writing a Wireshark Dissector: 3 Ways to Eat Bytes](/retrospective/sfasia/presentations18/02.7z), by Graham Bloice
*   03: In the Packet Trenches - Part 2, by Hansang Bae

*   04: [Wireshark Saves the Day! A Beginner’s Guide to Packet Analysis](/retrospective/sfasia/presentations18/04.pdf), by Maher Adib
*   05: [Sneaking in by the Back Door: Hacking the non-standard layers with Wireshark - Part 1](/retrospective/sfasia/presentations18/05.pdf), by Phill Shade
*   06: [Developer Bytes Lightning Talks](/retrospective/sfasia/presentations18/06.pdf), by Wireshark Core Developers

*   [Presentation Video](https://youtu.be/gBMMGHVfMGE) (1:07:00)

*   07: [Sneaking in by the Back Door: Hacking the non-standard layers with Wireshark - Part 2](/retrospective/sfasia/presentations18/05.pdf), by Phill Shade
*   08: [TCP SACK Overview and Impact on Performance](/retrospective/sfasia/presentations18/08.zip), by John Pittle
*   09: [Using Wireshark to Solve Real Problems for Real People: Step-by-Step Case Studies in Packet Analysis](/retrospective/sfasia/presentations18/09.pptx), by Kary Rogers

*   [Presentation Video](https://youtu.be/TLjEYPFZCf8 "Presentation video on YouTube") (1:10:38)

*   10: Augmenting Packet Capture with Contextual Meta-Data: the what, why and how, by Dr. Stephen Donnelly

*   [Presentation Video](https://youtu.be/WcVS6AZyDng "Presentation video on YouTube") (57:06)

**Wednesday Classes**
---------------------

*   11: [Wireshark CLI Tools and Scripting](/retrospective/sfasia/presentations18/11.pdf), by Sake Blok
*   12: [Filter Maniacs: Tips & Techniques for Wireshark display & winpcap/libpcap capture filters](/retrospective/sfasia/presentations18/12.pdf), by Megumi Takeshita
*   13: [Designing a Packet Capture Strategy...and how it fits into an overall performance visibility strategy](/retrospective/sfasia/presentations18/13.zip), by John Pittle

*   [Presentation Video](https://youtu.be/sKO0IVgjOTk "Presentation video on YouTube") (1:17:42)

*   14: [Wireshark and Layer 2 Control Protocols](/retrospective/sfasia/presentations18/14.pdf), by Werner Fischer
*   15: LTE Explained... The Other Protocols, by Mark Stout

*   [Presentation Video](https://youtu.be/zPZgeCRyz9c "Presentation video on YouTube") (58:21)

*   16: [extcap – Packet Capture beyond libpcap/winpcap: Bluetooth Sniffing, Android Dumping & other Fun Stuff!](/retrospective/sfasia/presentations18/16.pdf), by Roland Knall

*   [Presentation Video](https://youtu.be/6opeMOl7uJo "Presentation video on YouTube") (46:26)

*   17: The Packet Doctors are In!, by Drs: Hansang Bae, Phill Shade, Sake Blok
*   18: [Understanding Throughput & TCP Windows: factors that can limit TCP throughput performance](/retrospective/sfasia/presentations18/18.pdf), by Kary Rogers

*   [Presentation Video](https://youtu.be/ZxSi4M941Bs "Presentation video on YouTube") (59:40)

*   19: [SSL/TLS Decryption: Uncovering Secrets](/retrospective/sfasia/presentations18/19.pdf "Presentation slides"), by Peter Wu

*   [Presentation Video](https://youtu.be/bwJEBwgoeBg "Presentation video on YouTube") (1:06:28)

*   20: [How Did They Do That? Network Forensic Case Studies](/retrospective/sfasia/presentations18/20.pdf "Presentation slides"), by Phill Shade

* * *

#### A Word of Thanks

SharkFest'18 ASIA, on the 20th anniversity of the Wireshark Project, and the first ever in the Asia Pacific was a roaring success thanks to the highly engaged community of core developers and Wireshark users in attendance. Special thanks to Gerald Combs for tirelessly, fearlessly guiding the Wireshark open source project and maintaining its relevancy, to core developers for traveling long distances to advance Wireshark code together, to Laura Chappell for creating another highly-anticipated Packet Challenge, to Sake Blok for the many man-hours dedicated to creating a thrilling group packet competition, to a staff and volunteer crew who went far beyond caring to serve attendees during the conference, to instructors who voluntarily shuffled lives and schedules to educate participants and learn from one another, to sponsors who so generously provided resources that made the conference possible, to the NTU social hosts who made our social events truly social, and to the team for working through months of minutiae to help stage the conference on the lush and beautiful NTU campus.

#### Sponsors

**Host Sponsors**

[![](/img/sponsors/riverbed.png)](http://www.riverbed.com/) [![](/img/sponsors/wireshark_university_logo.png)](http://www.wiresharktraining.com/)

**Angel Shark Sponsors**

[![](/img/sponsors/profitap.png)](http://www.profitap.com/)

**Group Packet Challenge Sponsor**

[![](/img/sponsors/endace_big.png)](https://www.endace.com/)

**Honorary Sponsors**

[![](/img/sponsors/lovemtytool_Logo.png)](http://www.lovemytool.com/)