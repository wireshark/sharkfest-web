SharkFest'11 Retrospective
==========================

June 13th - 16th, 2011  
Stanford University | Stanford, California

### **Blogs**

[Event Photos](https://www.facebook.com/media/set/?set=a.10150222206636871.321177.51502216870)

### **Presentation Videos**

[Love My Tool](https://www.lovemytool.com/blog/sharkfest/)  

### **Keynote Video and Presentation**

SHARKFEST '11 Keynote Address, by Steve McCanne with an introduction, by Gerald Combs and Loris Degioanni.

Keynote Presentation:[The Architecture and Optimization Methodology of the libpcap Packet Capture Library](/retrospective/sfus/presentations11/McCanne-Sharkfest'11_Keynote_Address.pdf "Download presentation PDF"), by Steve McCanne, co-creator tcpdump, co-founder and CTO, Riverbed.

### **Beginner Track Presentations**

*   [B-1 I've Downloaded Wireshark...Now What?](/retrospective/sfus/presentations11/B-1_DuBois-%20I_Just_Downloaded_Wireshark-Now_What.pdf), by Betty DuBois
*   [B-2 and B-9 Mobile Application Analysis with Wireshark](/retrospective/sfus/presentations11/B-2_Bardwell-Mobile_App_Analysis.pdf), by Joe Bardwell
*   [B-3 Discovering IPv6 with Wireshark](/retrospective/sfus/presentations11/B-3_Leutert-Discovering_IPv6_with_Wireshark.pdf), by Rolf Leutert
*   [B-4 Visualizing RF](/retrospective/sfus/presentations11/B-4_Woodings-Visualizing_RF.pdf), by Ryan Woodings
*   [B-6 Network Mysteries and How To Solve Them Part 1](/retrospective/sfus/presentations11/B-6_DuBois-Network_Mysteries-Case_of_the_Slow_Network.pdf), by Betty DuBois
*   [B-7 Discovering WLAN 802.11n MIMO](/retrospective/sfus/presentations11/B-7_Leutert-Discovering_WLAN_802.11n_MIMO.pdf), by Rolf Leutert
*   [B-10 Using Wireshark to Support the Application](/retrospective/sfus/presentations11/B-10_Poth-Using_Wireshark_to_Support_the_Application.pdf), by Tim Poth + [Trace File](/retrospective/sfus/presentations11/B-10_Trace_Files_PCAP.7z)
*   [B-11 Network Mysteries and How To Solve Them Part 2](/retrospective/sfus/presentations11/B-11_DuBois-Network_Mysteries-Case_of_the_Missing_Download.pdf), by Betty DuBois

### **Intermediate Track Presentations**

*   [I-1 Packet Trace Whispering](/retrospective/sfus/presentations11/I-1_Bae-Packet_Trace_Whispering.pdf), by Hansang Bae + [Trace Files](/retrospective/sfus/presentations11/I-1_Bae-Packet_Trace_Whispering_TraceFile.zip)
*   [I-2 Increasing the Throughput of Network Appliances through Virtualization](/retrospective/sfus/presentations11/I-2_Sanders-Increasing_Throughput_of_Network_Appliances_Through_Virtualization.pdf), by Pete Sanders
*   [I-3 and I-4 Hands-On Lab: Customizing Wireshark for Different Use Scenarios](/retrospective/sfus/presentations11/I-3_Chappell-Customizing_Wireshark_for_Different_Use_Scenarios.pdf), by Laura Chappell
*   [I-6 Taking Wireshark to the Future Network](/retrospective/sfus/presentations11/I-6_Leong-Taking_Wireshark_to_the_Future_Network.pdf), by Patrick Leong
*   [I-7 Using NetFlow to Analyze Your Network](/retrospective/sfus/presentations11/I-7_White-Using_NetFlow_to_Analyze_Your_Network.pdf), by Christopher J. White
*   [I-8 Troubleshooting Application Performance Issues](/retrospective/sfus/presentations11/i-8_Canney-Troubleshooting_Application_Performance_Issues.pdf), by Mike Canney
*   [I-9 and I-10 Hands-On Lab: Troubleshooting Tips & Tricks for TCP/IP Networks](/retrospective/sfus/presentations11/I-9_Chappell-Troubleshooting_Tips_and_Tricks_for_TCP_IP_Networks.pdf), by Laura Chappell

### **Mixed Bag Track Presentations**

*   [A-1 Writing Wireshark Dissectors and Plug-Ins](/retrospective/sfus/presentations11/A-1_Fisher_Combs-Writing_Wireshark_Dissectors_Plug-Ins.pdf), by Stephen Fisher and Gerald Combs
*   [A-2 and A-5 Using Wireshark Command Line Tools & Scripting](/retrospective/sfus/presentations11/A-2_Blok-Using_Wireshark_Command_Line_Tools_&_Scripting.pdf), by Sake Blok
*   [A-3 PacketFu, by Example](http://prezi.com/mw0_9qfb2d6d/packetfu-by-example/) and [Metasploit: Open Source Penetration Testing](http://prezi.com/290abtkiola3/metasploit-open-source-penetration-testing/), by Tod Beardsley
*   [A-4 Wireshark vs. the Cloud](/retrospective/sfus/presentations11/A-4_Bongertz-Wireshark_vs_the_Cloud.pdf), by Jasper Bongertz
*   [A-6 Open Source Security Tools](/retrospective/sfus/presentations11/A-6_Lyon-Open_Source_Security_Tools.pdf), by Gordon Lyon, Tod Beardsley and Thomas D'Otreppe
*   [A-7 Have Wireshark – Will Travel](/retrospective/sfus/presentations11/A-7_Bongertz-Have_Wireshark_Will_Travel.pdf), by Jasper Bongertz
*   [A-8 Wireshark in the Large Enterprise](/retrospective/sfus/presentations11/A-8_Bae-Wireshark_in_the_Large_Enterprise.pdf), by Hansang Bae + [Trace Files](presentations/A-8_Bae-Wireshark_in_the_Large_Enterprise_TraceFile.zip)
*   [A-10 VoIP Troubleshooting and Analysis](/retrospective/sfus/presentations11/A-10_Giordano_and_Degioanni-VoIP_Troubleshooting_and_Analysis.pdf), by Pietro Giordano and Loris Degioanni
*   [A-11 Trace File Anonymization](/retrospective/sfus/presentations11/A-11_Bongertz-Trace_File_Anonymization.pdf), by Jasper Bongertz

* * *

#### A Word of Thanks

Another SharkFest has come and gone, and we thank each participant, sponsor, presenter, keynote speaker, coordinator, caterer, staff and general support person for making this a successful, richly educational event once again. All session presentations are available from this page, and recordings of many sessions can be found at [www.lovemytool.com](www.lovemytool.com).