SharkFest'08 Retrospective
==========================

March 31st - April 2nd, 2008  
Foothill College | Los Altos Hills, California

### **Links**

*   [Laura Chappell's blog](http://laurachappell.blogspot.com/)
*   [SHARKFEST coverage at LoveMyTool](http://www.lovemytool.com/blog/sharkfest/)
*   [Compute Magazine](http://computemagazine.com/technology/open-source-founders-reflect-on-project-milestones.html)
*   [Scott Haugdahl](http://thenetworkguy.typepad.com/)

### **Pictures**

*   [Snapfish](http://sharkfest08.snapfish.com/snapfish) (account required)

### **Keynote Presentation**

[/retrospective/sfus/presentations08/Sharkfest08\_Keynote\_Cerf.pdf](http://sharkfest.wireshark.org/retrospective/sfus/presentations08/Sharkfest08_Keynote_Cerf.pdf)

### **Presentations**

*   [BoF\_Varenni\_ WinPcap Do's and Don'ts.zip](http://sharkfest.wireshark.org/retrospective/sfus/presentations08/BoF_Varenni_%20WinPcap%20Do's%20and%20Don'ts.zip)
*   [D01\_Blok\_Advanced Scripting, Command Line Usage with tshark.ppt](http://sharkfest.wireshark.org/retrospective/sfus/presentations08/D01_Blok_Advanced%20Scripting,%20Command%20Line%20Usage%20with%20tshark.ppt)
*   [D02\_Combs\_Intro to Writing Wireshark Packet Dissectors.ppt](http://sharkfest.wireshark.org/retrospective/sfus/presentations08/D02_Combs_Intro%20to%20Writing%20Wireshark%20Packet%20Dissectors.ppt)
*   [D03\_Harris\_Writing Wireshark Dissectors\_Advanced.pdf](http://sharkfest.wireshark.org/retrospective/sfus/presentations08/D03_Harris_Writing%20Wireshark%20Dissectors_Advanced.pdf)
*   [D04\_Varenni\_Writing your own Packet Capture Tool with WinPcap and AirPcap.zip](http://sharkfest.wireshark.org/retrospective/sfus/presentations08/D04_Varenni_Writing%20your%20own%20Packet%20Capture%20Tool%20with%20WinPcap%20and%20AirPcap.zip)
*   [D05\_Leutert\_Analysing 802.11n MIMO.pdf](http://sharkfest.wireshark.org/retrospective/sfus/presentations08/D05_Leutert_Analysing%20802.11n%20MIMO.pdf)
*   [D07\_Lamping\_Extending the Wireshark UI.zip](http://sharkfest.wireshark.org/retrospective/sfus/presentations08/D07_Lamping_Extending%20the%20Wireshark%20UI.zip)
*   [D06\_Johnson\_802.11 Packet Dissection with AirPcap, WinPcap.pdf](http://sharkfest.wireshark.org/retrospective/sfus/presentations08/D06_Johnson_802.11%20Packet%20Dissection%20with%20AirPcap,%20WinPcap.pdf)
*   [D09\_Sharpe\_File and Disk Sharing Protocols.ppt](http://sharkfest.wireshark.org/retrospective/sfus/presentations08/D09_Sharpe_File%20and%20Disk%20Sharing%20Protocols.ppt)
*   [R01\_Bae\_Protocol Analysis in Complex Ent. Infrastructures.ppt](http://sharkfest.wireshark.org/retrospective/sfus/presentations08/R01_Bae_Protocol%20Analysis%20in%20Complex%20Ent.%20Infrastructures.ppt)
*   [T1-1\_DuBois\_Downloaded Wireshark - Now What.ppt](http://sharkfest.wireshark.org/retrospective/sfus/presentations08/T1-1_DuBois_Downloaded%20Wireshark%20-%20Now%20What.ppt)
*   [T1-2\_Tviet\_Virtues of Continuous, Complete Packet Capture.pdf](http://sharkfest.wireshark.org/retrospective/sfus/presentations08/T1-2_Tviet_Virtues%20of%20Continuous,%20Complete%20Packet%20Capture.pdf)
*   [T1-3\_Chappell\_-Performance Problems.pdf](http://sharkfest.wireshark.org/retrospective/sfus/presentations08/T1-3_Chappell_-Performance%20Problems.pdf)
*   [T1-5\_Bardwell\_Intro to WLAN Analysis.pdf](http://sharkfest.wireshark.org/retrospective/sfus/presentations08/T1-5_Bardwell_Intro%20to%20WLAN%20Analysis.pdf)
*   [T1-6\_Wright and Kershaw\_Leveraging Wireshark for Net Analysis.ppt](http://sharkfest.wireshark.org/retrospective/sfus/presentations08/T1-6_Wright%20and%20Kershaw_Leveraging%20Wireshark%20for%20Net%20Analysis.ppt)
*   [T1-7\_DOtreppe\_WLAN Analysis and Security.pdf](http://sharkfest.wireshark.org/retrospective/sfus/presentations08/T1-7_DOtreppe_WLAN%20Analysis%20and%20Security.pdf)
*   [T1-8\_Leong\_Non-Intrusive OOB Network Monitoring.ppt.pps](http://sharkfest.wireshark.org/retrospective/sfus/presentations08/T1-8_Leong_Non-Intrusive%20OOB%20Network%20Monitoring.ppt.pps)
*   [T1-10\_Walberg\_Expose VoIP problems with Wireshark.ppt](http://sharkfest.wireshark.org/retrospective/sfus/presentations08/T1-10_Walberg_Expose%20VoIP%20problems%20with%20Wireshark.ppt)
*   [T1-12\_Tuexen\_SCTP\_SIGTRAN and SS7.pdf](http://sharkfest.wireshark.org/retrospective/sfus/presentations08/T1-12_Tuexen_SCTP_SIGTRAN%20and%20SS7.pdf)
*   [T1-13\_Bihary\_Tapping Basics.ppt](http://sharkfest.wireshark.org/retrospective/sfus/presentations08/T1-13_Bihary_Tapping%20Basics.ppt)
*   [T2-1\_Fortunato\_Analyzer Placement and Baseline Techniques.pdf](http://sharkfest.wireshark.org/retrospective/sfus/presentations08/T2-1_Fortunato_Analyzer%20Placement%20and%20Baseline%20Techniques.pdf)
*   [T2-2\_Chappell\_TCPIP\_Resolution.pdf](http://sharkfest.wireshark.org/retrospective/sfus/presentations08/T2-2_Chappell_TCPIP_Resolution.pdf)
*   [T2-4\_Chappell\_Trace-File-Analysis\_Latency.pdf](http://sharkfest.wireshark.org/retrospective/sfus/presentations08/T2-4_Chappell_Trace-File-Analysis_Latency.pdf)
*   [T2-5\_Fortunato\_Adv. Capture and Display Filtering.pdf](http://sharkfest.wireshark.org/retrospective/sfus/presentations08/T2-5_Fortunato_Adv.%20Capture%20and%20Display%20Filtering.pdf)
*   [T2-6\_Chappell\_DuBois\_Trace file analysis - TCP window.pdf](http://sharkfest.wireshark.org/retrospective/sfus/presentations08/T2-6_Chappell_DuBois_Trace%20file%20analysis%20-%20TCP%20window.pdf)
*   [T2-7\_Chappell\_Network-Forensics.pdf](http://sharkfest.wireshark.org/retrospective/sfus/presentations08/T2-7_Chappell_Network-Forensics.pdf)
*   [T2-8\_Chappell\_Trace-File-Analysis\_Loss.pdf](http://sharkfest.wireshark.org/retrospective/sfus/presentations08/T2-8_Chappell_Trace-File-Analysis_Loss.pdf)
*   [T2-9\_Chappell\_DuBois\_Top 10.pdf](http://sharkfest.wireshark.org/retrospective/sfus/presentations08/T2-9_Chappell_DuBois_Top%2010.pdf)
*   [T2-10\_Chappell\_DuBois\_Trace file analysis - Case Studies.pdf](http://sharkfest.wireshark.org/retrospective/sfus/presentations08/T2-10_Chappell_DuBois_Trace%20file%20analysis%20-%20Case%20Studies.pdf)
*   [T2-11\_Fortunato\_HTTP Trace File Analysis.pdf](http://sharkfest.wireshark.org/retrospective/sfus/presentations08/T2-11_Fortunato_HTTP%20Trace%20File%20Analysis.pdf)
*   [T2-12\_Chappell\_Complementary-Tools.pdf](http://sharkfest.wireshark.org/retrospective/sfus/presentations08/T2-12_Chappell_Complementary-Tools.pdf)
*   [T2-13\_Chappell\_Trace-File-Analysis\_Infected.pdf](http://sharkfest.wireshark.org/retrospective/sfus/presentations08/T2-13_Chappell_Trace-File-Analysis_Infected.pdf)

* * *

#### A Word of Thanks

We would like to thank all of the Sharkfest attendees, presenters, and supporters for helping to create a great conference. See you next year!