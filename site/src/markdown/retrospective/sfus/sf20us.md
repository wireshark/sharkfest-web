SharkFest'20 Virtual US Retrospective
=====================================

October 12th - 16th, 2020
Online

### **Keynote Presentations**

#### **Latest Wireshark Developments & Road Map**  
Gerald Combs & Friends
<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/2Q0juwavIzU" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0;"></iframe></div>

#### **A Bit About Zeek & Spicy**  
Vern Paxson & Robin Sommer
<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/7jxvhpxAOfg" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0;"></iframe></div>

### **Thursday Classes**

*   01: [BACNet and Wireshark for Beginners](/retrospective/sfus/presentations20/1.zip), by Werner Fischer
    *   [Presentation Video](https://youtu.be/QxCMxXjqkyA "Presentation video on YouTube") 

*   02: [Going down the retransmission hole](/retrospective/sfus/presentations20/02.pdf), by Sake Blok
*   03: IPv6 security assessment tools (aka IPv6 hacking tools), by Jeff Carrell
    *   [Presentation Video](https://youtu.be/yNoBIpWt-v8 "Presentation video on YouTube") 

*   04: Improving packet capture in the DPDK, by Stephen Hemminger
*   [Presentation Video](https://youtu.be/YhYZ-fGTa_s "Presentation video on YouTube") 

*   05: [Kismet and Wireless Security 101](/retrospective/sfus/presentations20/05.pptx), by Mike Kershaw
    *   [Presentation Video](https://youtu.be/z6MzIDwjUmc "Presentation video on YouTube") 

*   06: Packets! Wait... What? A very improvised last-minute Wireshark talk about things you can find in pcap files that are funny, interesting or weird. I don't know. Let's find out together, by Jasper Bongertz
    *   [Presentation Video](https://youtu.be/S7OsIOrvCKw "Presentation video on YouTube") 

*   07: TLS encryption and decryption: What every IT engineer should know about TLS, by Ross Bagurdes
    *   [Presentation Video](https://youtu.be/9cAyDAbMtZY "Presentation video on YouTube") 

*   08: Why an Enterprise Visibility Platform is critical for effective Packet Analysis?, by Keval Shah
*   [Presentation Video](https://youtu.be/cFGC13xUvKg "Presentation video on YouTube") 

*   09: [Troubleshooting Cloud Network Outages](/retrospective/sfus/presentations20/09.zip), by Chris Hull
    *   [Presentation Video](https://youtu.be/1CdGwWw8YUI "Presentation video on YouTube") 

*   10: [TCP SACK overview & impact on performance](/retrospective/sfus/presentations20/10.zip), by John Pittle
    *   [Presentation Video](https://youtu.be/4wEDcBZb7fU "Presentation video on YouTube") 

*   11: [Automation TIPS & tricks Using Wireshark/tshark in Windows](https://www.ikeriri.ne.jp/wireshark/sf20.zip), by Megumi Takeshita
    *   [Presentation Video](https://youtu.be/kv97gYCMM5Q "Presentation video on YouTube")

*   12: [How Long is a Packet? And Does it Really Matter?], by Dr. Stephen Donnelly
*   [Presentation Video](https://youtu.be/1fU8lvr1Zds "Presentation video on YouTube")

### **Friday Classes**

*   13: Make the bytes speak to you, by Roland Knall
    *   [Presentation Video](https://youtu.be/Sc9kDIidrxA "Presentation video on YouTube")

*   14: [USB Analysis 101](/retrospective/sfus/presentations20/14.zip), by Tomasz Mon
    *   [Presentation Video](https://youtu.be/cUljKImph4s "Presentation video on YouTube")

*   15: TLS decryption examples, by Peter Wu
    *   [Presentation Video](https://youtu.be/v-lDEiA7JPE "Presentation video on YouTube")
*   16: The Packet Doctors are in! Packet trace examinations with the experts with Drs. Bae, Blok, Bongertz & Landström

*   17: [Analyzing Honeypot Traffic](/retrospective/sfus/presentations20/17.pdf), by Tom Peterson
    *   [Presentation Video](https://youtu.be/aQJW3Kx56sY "Presentation video on YouTube")

*   18: Intrusion Analysis and Threat Hunting with Suricata, by Josh Stroschein and Jack Mott
    *   [Presentation Video](https://youtu.be/0WgyiOxCFzE "Presentation video on YouTube")

*   19: The Other Protocols (used in LTE), by Mark Stout
    *   [Presentation Video](https://youtu.be/6n9jTYLX70s "Presentation video on YouTube")

*   20: Practical Signature Development for Open Source IDS, by Jason Williams and Jack Mott
    *   [Presentation Video](https://youtu.be/nTQYA432h_k "Presentation video on YouTube")

*   21: [Ostinato - craft packets, generate traffic](/retrospective/sfus/presentations20/21.zip), by Srivats P
    *   [Presentation Video](https://youtu.be/1DIs2VIT3bI "Presentation video on YouTube")

*   22: Introduction to WAN Optimization, by John Pittle
    *   [Presentation Video](https://youtu.be/IyvlvmdbvZM "Presentation video on YouTube")

*   23: Solving Real World Case Studies, by Kary Rogers
    *   [Presentation Video](https://youtu.be/Iqk9waColDo "Presentation video on YouTube")

*   24: Analyzing 802.11 Powersave Mechanisms with Wireshark, by George Cragg
    *   [Presentation Video](https://youtu.be/IFIuCHQBOfE "Presentation video on YouTube")


* * *

#### Host Sponsor

[![](/img/sponsors/riverbed.png)](http://www.riverbed.com/)

#### Angel Shark Sponsors

[![](/img/sponsors/endace.png)](https://www.endace.com/) [![](/img/sponsors/gnet.png)](https://gnet-inc.com/) [![](/img/sponsors/fmadio.jpg)](https://fmad.io/)[![](/img/sponsors/gigamon-new.png)](https://gigamon.com)[![](/img/sponsors/veelong.png)](https://veelong.com)

#### Honorary Sponsor

[![](/img/sponsors/lovemtytool_Logo.png)](http://www.lovemytool.com/)