SharkFest'16 Retrospective
==========================

June 13th - 16th, 2016  
Computer History Museum | Mountain View, California

### **Keynote Presentation**

**The Ancient History of Computers and Network Sniffers**  
Len Shustek
<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/x0hCRSfFN78" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0;"></iframe></div>

### **Blogs**

[SharkFest'16 Recap](https://blog.packet-foo.com/2016/07/sharkfest-2016-recap/), by Jasper Bongertz  
[The Network Capture Playbook Part 1](https://blog.packet-foo.com/2016/10/the-network-capture-playbook-part-1-ethernet-basics/), by Jasper Bongertz  
[The Network Capture Playbook Part 2](https://blog.packet-foo.com/2016/10/the-network-capture-playbook-part-2-speed-duplex-and-drops/), by Jasper Bongertz  
[The Network Capture Playbook Part 3](https://blog.packet-foo.com/2016/11/the-network-capture-playbook-part-3-network-cards/), by Jasper Bongertz  
[The Network Capture Playbook Part 4](https://blog.packet-foo.com/2016/11/the-network-capture-playbook-part-4-span-port-in-depth/), by Jasper Bongertz  
[The Network Capture Playbook Part 5](https://blog.packet-foo.com/2016/12/the-network-capture-playbook-part-5-network-tap-basics/), by Jasper Bongertz  

### **SharkBytes**
<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/3vtZ97BNqvg" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0;"></iframe></div>

### **Tuesday Classes**

*   01: [In the Packet Trenches](/retrospective/sfus/presentations16/01.pptx), by Hansang Bae

*   [Presentation Video](https://youtu.be/22F0Q0tAxTw "Presentation video on YouTube") (1:16:32)

*   02: [We Still Don’t Get It! Security is Still Hard](/retrospective/sfus/presentations16/02.pdf), by Mike Kershaw
*   03: [Writing a Dissector: 3 Ways to Eat Bytes](/retrospective/sfus/presentations16/03.7z), by Graham Bloice

*   04: [T-Shark for the Win](/retrospective/sfus/presentations16/04.pdf), by Christian Landström

*   [Presentation Video](https://youtu.be/CEOMw650D_Y "Presentation video on YouTube") (1:14:05)

*   05: [TCP Tips, Tricks, and Traces: Let’s Chat About What Makes Applications Crawl](/retrospective/sfus/presentations16/05.pptx), by Chris Greer

*   [Presentation Video](https://youtu.be/15wDU3Wx1h0 "Presentation video on YouTube") (1:02:21)

*   06: Analyzing and Re-Implementing a Proprietary Protocol, by Jonah Stiennon
*   07: [Tackling the Haystack: How to Process Large Numbers of Packets – Part 1](/retrospective/sfus/presentations16/07.pdf), by Jasper Bongertz
*   08: [Network Baselining with Wireshark to Identify and Stop Unwanted Communications](/retrospective/sfus/presentations16/08.pdf), by Jon Ford
*   09: [Troubleshooting IPv6 with Wireshark – Part 1](/retrospective/sfus/presentations16/09.pdf), by Jeff Carrell
*   10: [Tackling the Haystack: How to Process Large Numbers of Packets – Part 2](/retrospective/sfus/presentations16/07.pdf), by Jasper Bongertz

*   [Presentation Video](https://youtu.be/8WVahHErC-0 "Presentation video on YouTube") (1:17:22)

*   11: Top 10 Wireshark 2 Features, by Laura Chappell
*   12: [Troubleshooting IPv6 with Wireshark – Part 2](/retrospective/sfus/presentations16/09.pdf), by Jeff Carrell

### **Wednesday Classes**

*   [13: Capture Filter Sorcery: How to Use Complex BPF Capture Filters in Wireshark](/retrospective/sfus/presentations16/13.pdf), by Sake Blok

*   [Presentation Video](https://youtu.be/DS4j9pwVuog "Presentation video on YouTube") (1:06:13)

*   14: [Cisco ACI and Wireshark: Getting Back Our Data](/retrospective/sfus/presentations16/14.pdf), by Karsten Hecker
*   15: Adventures in Packet Analysis: Run Wireshark Everywhere!, by Maher Adib
*   16: [Advanced Wireshark Display Filters: How to Zoom in on the 10 Packets You Actually Need](/retrospective/sfus/presentations16/16.pdf), by Betty DuBois

*   [Presentation Video](https://youtu.be/EKPef0BFTQY "Presentation video on YouTube") (1:09:22)

*   17: [Tempering tshark & tcpdump with tmux](/retrospective/sfus/presentations16/17.pdf), by Boyd Stephens
*   18: [Determining Topology from a Capture File](/retrospective/sfus/presentations16/18.pptx), by Chris Bidwell
*   19: [Markers – Beacons in an Ocean of Packets](/retrospective/sfus/presentations16/19.pptx "Presentation slides"), by Matthew York

*   [Presentation Video](https://youtu.be/M2SE8hf4h3s "Presentation video on YouTube") (54:56)

*   20: [Troubleshooting with Layer 2 Control Protocols](/retrospective/sfus/presentations16/20.pdf "Presentation slides"), by Werner Fischer
*   21: [Wireshark 2.0 Tips for HTTP1/2 Analysis](/retrospective/sfus/presentations16/21.pdf), by Megumi Takeshita

*   [Presentation Materials](/retrospective/sfus/presentations16/HTTP2.zip)

*   22: [Detection and Verification of IoCs (Indicators of Compromise)](/retrospective/sfus/presentations16/22.pdf "Presentation slides"), by Jasper Bongertz

*   [Presentation Video](https://youtu.be/V-KM4p0x-po "Presentation video on YouTube") (1:13:00)

*   23: [Troubleshooting a Multi-Tier Application in a Production Environment](/retrospective/sfus/presentations16/23.pdf), by Captain Brad Palm
*   24: [The Packet A(nalysis) Team: Case Studies in Helping Solve Problems with Packet Analysis](/retrospective/sfus/presentations16/24.pptx), by Kary Rogers

### **Thursday Classes**

*   25: Troubleshooting in the Large Enterprise – Part 1, by Hansang Bae
*   26: [Forensic Network Analysis in the Time of APTs](/retrospective/sfus/presentations16/26.pdf), by Christian Landström
*   27: [WiFi Capture and Injection on Various OSes – Revisited](/retrospective/sfus/presentations16/27.pdf), by Thomas D’Otreppe
*   28: Troubleshooting in the Large Enterprise – Part 2, by Hansang Bae

*   [Presentation Video]( https://youtu.be/dnGdmMCchjk "Presentation video on YouTube") (1:13:49)

*   29: Detecting Suspicious Traffic, by Laura Chappell
*   30: Learning About Networking, by Using Wireshark with GNS3: Learn Safely in an Emulator, by John Schreiner
*   31: [Using Wireshark Command Line Tools & Scripting](/retrospective/sfus/presentations16/31.zip "Presentation slides"), by Sake Blok

*   [Presentation Video](https://youtu.be/k2PfYOoRiKM "Presentation video on YouTube") (1:16:21)

*   32: [SDN/OpenFlow Analysis](/retrospective/sfus/presentations16/32.pdf), by Jeff Carrell
*   33: Using Wireshark to Prove Root Cause: Real-World Troubleshooting Tales, by Graeme Bailey
*   34: [Top 5 False Positives](/retrospective/sfus/presentations16/34.pdf), by Jasper Bongertz

*   [Presentation Video](https://youtu.be/GRAY1OceGc4 "Presentation video on YouTube") (1:12:26)

*   35: [TCP Tips, Tricks, and Traces: Let’s Chat About What Makes Applications Crawl](/retrospective/sfus/presentations16/05.pptx), by Chris Greer

*   [Presentation Video](https://youtu.be/15wDU3Wx1h0 "Presentation video on YouTube") (1:02:21)

*   36: [We Still Don’t Get It! Security is Still Hard](/retrospective/sfus/presentations16/02.pdf), by Mike Kershaw

* * *

#### A Word of Thanks

The success of SharkFest'16 is due in very large part to Gerald Combs and the core developers excellent stewardship of the Wireshark open source project, to the tireless efforts of Laura Chappell who provides innumerable hours of expert instruction before and during the conference, to Laura's merry band of packet analysis mentors who guide and inform all comers to The Reef, to the world-class presenters who contribute their time and talent to educate the gathered Wireshark community, to the sponsors who so generously donate their time and resources to make the event possible every year, and to the SharkFest crew of volunteer staff without whom the curtain for this event could not be raised. Yottabytes of thanks to all these, and to the attendees who show up every year to immerse themselves in this unique educational experience.

#### Sponsors

**Host Sponsor**

[![](/img/sponsors/riverbed.png)](http://www.riverbed.com/)

**Reef Sponsors**

[![](/img/sponsors/bigswitch.png)](http://www.bigswitch.com/) [![](/img/sponsors/cpacket.png)](http://www.cpacket.com/index.php) [![](/img/sponsors/pluribus-white.png)](http://www.pluribusnetworks.com/)

**Angel Shark Sponsors**

[![](/img/sponsors/datacom.png)](http://datacomsystems.com/) [![](/img/sponsors/garland.png)](http://www.garlandtechnology.com/) [![](/img/sponsors/inside_products_logo.png)](http://www.insidethestack.com/) [![](/img/sponsors/ixia.png)](http://www.ixiacom.com/) [![](/img/sponsors/niagara.png)](http://www.niagaranetworks.com/) [![](/img/sponsors/tribelab.png)](http://www.tribelab.com/)

**In Absentia Sponsors**

[![](/img/sponsors/accolade.png)](http://accoladetechnology.com/) [![](/img/sponsors/toyo.png)](https://www.toyotechus.com/)

**Honorary Sponsors**

[![](/img/sponsors/lovemtytool_Logo.png)](http://www.lovemytool.com/) [![](/img/sponsors/wiresharku.png)](http://www.wiresharktraining.com/)