SharkFest'14 Retrospective
==========================

June 16th - 20th, 2014  
Dominican University of California | San Rafael, California

### **Blogs**

[SharkFest 2014 Recap](http://blog.packet-foo.com/2014/06/sharkfest-2014-recap/), by Jasper Bongertz

### **Packet Challenge**

The SharkFest 2014 [Packet Challenge answer key](http://wiresharktraining.com/sharkfest2014.html) is online at Wireshark University.

### **SharkFest Jeopardy**

Click here for the full [PDF](</retrospective/sfus/presentations14/Sharkfest Jeopardy.pdf>) of SharkFest Jeopardy

### **Keynote Presentations**

**Continuous Measurement**  
Vinton Cerf
<iframe width="560" height="315" style="margin: 10px 0;" src="https://www.youtube.com/embed/sPSxWWXWKpk" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0;"></iframe></div>

**The Past and Future**  
Tim O'Neill
<iframe width="560" height="315" style="margin: 10px 0;" src="https://www.youtube.com/embed/dqfw_pEin0g" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0;"></iframe></div>

**The Future of Wireshark: Inside Wireshark Qt**  
Gerald Combs and Laura Chappell

### **Beginner Track**

*   B1: Art of Packet Analysis \[[PDF](/retrospective/sfus/presentations14/B1.pdf) || [PPT](</retrospective/sfus/presentations14/B1-Wireshark for Beginners-Hansang_Bae.pptx>)\], by Hansang Bae

*   [Presentation Video](http://www.youtube.com/embed/qvDmdv1-Xik "Presentation video on YouTube") (1:09:09)

*   B4: [Fun with Traces](/retrospective/sfus/presentations14/Fun-With-Traces-Beginner-Deck.pdf), by Stuart Kendrick
*   B5: TCP Analysis - First Steps \[[PDF](</retrospective/sfus/presentations14/B5 -TCP Analysis - First Steps.pdf>) || [PPT](</retrospective/sfus/presentations14/B5 - TCP Analysis - First Steps.pptx>)\], by Jasper Bongertz
*   B6: [Get Started with HTTP Analysis](</retrospective/sfus/presentations14/B6-Get Started with HTTP Analysis.pdf>), by Robert Bullen
*   B10: [Understanding Wiresharks Reassembly Features](</retrospective/sfus/presentations14/B10 - Understanding Wiresharks Reassembly Features.pdf>), by Christian Landström
*   B11: [IPv6 Security Assessment Tools and Infrastructure Mitigation](</retrospective/sfus/presentations14/B11 - IPv6 Security  Assessment Tools and Infrastructure Mitigation_JeffCarrell_finals1.pdf>), by Jeff Carrell
*   B12: [VoIP Troubleshooting](</retrospective/sfus/presentations14/B12 - VoIP Troubleshooting - Phillip Shade.pdf>), by Phillip Shade

### **Intermediate Track**

*   I1: [Best Practices for Packet Collection, Aggregation & Distribution in the Enterprise](</retrospective/sfus/presentations14/I1 Final - Haugdahl_b.pdf>), by J. Scott Haugdahl
*   I2: [Common Mistakes In Packet Collection](/retrospective/sfus/presentations14/I2.pdf), by Chris Greer

*   [Presentation Video](http://www.youtube.com/embed/rHQ1zg0rjX0 "Presentation video on YouTube") (38:16)

*   I3: Maximizing Packet Capture Performance \[[PDF](</retrospective/sfus/presentations14/I3: Sharkfest_2014_ABrown - Copy.pdf) || [PPT](/retrospective/sfus/presentations14/I3.pptx>)\], by Andrew Brown
*   I4: [How to Troubleshoot the Top 5 Causes for Poor Application Performance with Wireshark/Pilot](/retrospective/sfus/presentations14/I4.pptx), by Mike Canney
*   I5: Monitoring Mobile Network Traffic (3G/LTE) \[[PDF](/retrospective/sfus/presentations14/I5.pdf) || [PPT](/retrospective/sfus/presentations14/I5.pptx)\], by Luca Deri
*   I6: [Getting the Most out of Your SDN](/retrospective/sfus/presentations14/I6.pptx), by Steve Riley
*   I8: Wireshark in the Large Enterprise \[[PDF](</retrospective/sfus/presentations14/I8-Wireshark in the Large Enterprise-Hansang_Bae.pdf>) || [PPT](</retrospective/sfus/presentations14/I8-Wireshark in the Large Enterprise-Hansang_Bae.pptx>)\], by Hansang Bae
*   I9: [Understanding the TCP Expert](</retrospective/sfus/presentations14/I9 - Understanding the Wireshark TCP Expert.pdf>), by Jasper Bongertz
*   I10: [Anatomy of a Cyber Attack](</retrospective/sfus/presentations14/I10 Anatomy of a CYber Attack.pptx>), by Tim O'Neill
*   I11: [Visualizing Problems through Packets](</retrospective/sfus/presentations14/Visualizing_Problems Through_Packets.pdf>), by Kevin Burns

*   [Presentation Video](https://www.youtube.com/embed/uLhpe6T1kr8 "Presentation video on YouTube") (1:07:10)

*   I12: [Capturing a Packet from Wire to Wires](/retrospective/sfus/presentations14/i12-capturing-a-packet_from-wire-to-wireshark_0.5-upload.pdf), by Joerg Mayer
*   I13: [Analysis and Visualization](</retrospective/sfus/presentations14/I13-Analysis and Visualizations.pdf>), by Robert Bullen

### **Advanced Track**

*   A1: [Writing a Wireshark Dissector](</retrospective/sfus/presentations14/A1 Dissectors.pptx>), by Graham Bloice
*   A2: [Defending the Network](</retrospective/sfus/presentations14/A2 - Defending the Network.pdf>), by Jasper Bongertz and Christian Landström
*   A3: [Packet Analysis and Visualization with SteelScript](https://support.riverbed.com/apis/steelscript/SharkFest2014.slides.html), by Christopher White
*   A5: [Diagramming IT Environments](/retrospective/sfus/presentations14/Diagramming-IT-Environments-Deck.pdf), by Stuart Kendrick
*   A6: [Large-Scale Passive Network Monitoring using Ordinary Switches](/retrospective/sfus/presentations14/A6.pptx), by Justin Scott and Rich Groves
*   A7: [Dive Even Deeper - Capturing, Analyzing and Filtering System Events](</retrospective/sfus/presentations14/A7 - Dive Even Deeper.pptx>), by Loris Degioanna and Davide Schiera
*   A8: [Old & Busted: C-code… New Hotness: Lua!](</retrospective/sfus/presentations14/A8 and A13 Lua.pptx>), by Hadriel Kaplan
*   A9: [Wi-Fi Threats and Countermeasures](/retrospective/sfus/presentations14/A9.pdf), by Gopinath KN
*   A11: [Definitive Diagnostic Data Deck](/retrospective/sfus/presentations14/Definitive-Diagnostic-Data-Deck.pdf), by Stuart Kendrick
*   A12: [(Not So) False Positives in Application Performance Analysis](</retrospective/sfus/presentations14/A12 - (Not So) False Positives in Application Performance Analysis.pdf>), by Christian Landström
*   A13: [Old & Busted: C-code… New Hotness: Lua!](</retrospective/sfus/presentations14/A8 and A13 Lua.pptx>), by Hadriel Kaplan

### SharkBytes

SharkBytes consist of "little crunchy bits of wisdom". Like the immensely popular TED talks, Shark Bytes aim to inform, inspire, surprise, and delight as presenters share their personal perspective on a topic in under 5 minutes.

* * *

#### A Word of Thanks

Post-conference feedback is in, and the preponderance of attendees agree that SharkFest’14 was a smashing success! We’d like to express our immense gratitude to the developers, keynote speakers, presenters, attendees, sponsors, events staff, Dominican University of California summer conference staff, and volunteers who generously collaborated to coordinate the 7th annual Wireshark community event.

#### **Great White Shark Sponsor**

[![](/img/sponsors/cpacket.png)](http://www.cpacket.com/index.php)

#### **Angel Shark Sponsors**

[![](/img/sponsors/apcon.png)](http://www.apcon.com/about-us) [![](/img/sponsors/endace.png)](http://www.endace.com/) [![](/img/sponsors/gigamon-new.png)](http://www.gigamon.com/) [![](/img/sponsors/bigswitch.png)](http://www.bigswitch.com/)

#### **Silvertip Shark Sponsors**

[![](/img/sponsors/dualcomm.png)](http://www.dual-comm.com/) [![](/img/sponsors/ntop.png)](http://www.ntop.org/) [![](/img/sponsors/garland-white.png)](http://www.garlandtechnology.com/) [![](/img/sponsors/lovemtytool_Logo.png)](http://www.lovemytool.com/) [![](/img/sponsors/napatech.png)](http://www.napatech.com/) [![](/img/sponsors/arista-bright.png)](http://www.aristanetworks.com/) [![](/img/sponsors/wiresharku.png)](http://www.wiresharktraining.com/) [![](/img/sponsors/ixia.png)](http://www.ixiacom.com/) [![](/img/sponsors/interface.png)](http://www.interfacemasters.com/) [![](/img/sponsors/inside.png)](http://www.insidethestack.com/)

#### **Hosting Sponsor**

[![](/img/sponsors/riverbed.png)](http://www.riverbed.com/)