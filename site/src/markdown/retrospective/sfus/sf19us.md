SharkFest'19 Retrospective
==========================

June 8th - 13th, 2019  
UC Berkeley | Berkeley, California

### **Keynote Presentations**

#### **[Latest Wireshark Developments & Road Map](/retrospective/sfus/presentations19/gerald-keynote.pptx)**  
Gerald Combs
<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/lAc6DCxdF2o" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0;"></iframe></div>

#### **Mental Models for Using Network Evidence**  
Chris Sanders
<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/bafqVHBT2sA" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0;"></iframe></div>

#### **A Brief Annotated History of Capture**  
Loris Degioanni
<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/KfA8jW4HQcQ" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0;"></iframe></div>

### **Tuesday Classes**

*   01: [War story: troubleshooting issues on encrypted links](/retrospective/sfus/presentations19/01.pdf), by Christian Landström
*   02: [TLS encryption & decryption: What every IT engineer should know about TLS](/retrospective/sfus/presentations19/02-26.pptx), by Ross Bagurdes

    *   [Presentation Video](https://www.youtube.com/watch?v=qbPOXoNXtGY "Presentation video on YouTube") (1:22:25)

*   03: [Writing a Wireshark Dissector: 3 ways to eat bytes](/retrospective/sfus/presentations19/03.zip), by Graham Bloice

    *   [Presentation Video](https://www.youtube.com/watch?v=Fp_7g5as1VY "Presentation video on YouTube") (1:18:07)

*   04: Solving (SharkFest) packet capture challenges with only tshark, by Sake Blok

    *   [Presentation Video](https://www.youtube.com/watch?v=gMg8ttIzry0 "Presentation video on YouTube") (1:14:11)

*   05: [How long is a packet? And does it really matter?](/retrospective/sfus/presentations19/05.pptx), by Stephen Donnelly

    *   [Presentation Video](https://www.youtube.com/watch?v=2GFCzAYSJx0&list=PLz_ZpPUgiXqMeN9ly6-lbr6Gdf3mknGIe&index=8&t=0s "Presentation video on YouTube") (1:17:54)

*   06: Creating dissectors like a pro, by generating dissectors, by Richard Sharpe

    *   [Presentation Video](https://www.youtube.com/watch?v=XFFkC4PdCbI&list=PLz_ZpPUgiXqMeN9ly6-lbr6Gdf3mknGIe&index=9&t=0s) (1:20:38)

*   07: [To Send or not to Send? How TCP congestion control algorithms work](/retrospective/sfus/presentations19/07-23.pdf), by Vladimir Gerasimov

    *   [Presentation Video](https://www.youtube.com/watch?v=EO8BaUwQHNI "Presentation video on YouTube") (1:30:56)

*   08: Taking a bite out of 100GB files, by Betty DuBois

    *   [Presentation Video](https://www.youtube.com/watch?v=olhr1V5J-1c "Presentation video on YouTube") (1:11:33)

*   09: [Debugging TLS issues with Wireshark](https://lekensteyn.nl/files/wireshark-tls-debugging-sharkfest19us.pdf), by Peter Wu

    *   [Presentation Video](https://www.youtube.com/watch?v=Ha4SLHceF6w "Presentation video on YouTube") (1:10:44)

*   10: IPv6 troubleshooting with Wireshark, by Jeff Carrell
*   11: [When TCP reassembly gets complicated](/retrospective/sfus/presentations19/11.key), by Tom Peterson

    *   [Presentation Video](https://www.youtube.com/watch?v=D9GHb4QQI6Q "Presentation video on YouTube") (41:47)

*   12: Jumbo frames & how to catch them, by Patrick Kinnison
*   13: Kismet & wireless security 101, by Mike Kershaw

    *   [Presentation Video](https://www.youtube.com/watch?v=Kk4sImFR4z4 "Presentation video on YouTube") (1:20:16)

*   14: Tracing the untraceable with Wireshark: a view under the hood, by Roland Knall

### **Wednesday Classes**

*   15: [Automating cloud infrastructure for analysis of large network captures](/retrospective/sfus/presentations19/15.pdf), by Brad Palm & Brian Greunke

    *   [Presentation Video](https://www.youtube.com/watch?v=MdjlHy2aFJc "Presentation video on YouTube") (1:30:15)

*   16: [My TCP ain't your TCP - ain't no TCP?](/retrospective/sfus/presentations19/16.pdf), by Simon Lindermann

    *   [Presentation Video](https://www.youtube.com/watch?v=2coqZ07-Yhw "Presentation video on YouTube") (1:26:14)

*   17: [TLS1.3, DNS over HTTPs, DNS over TLS, QUIC, IPv6 PDM & more!](/retrospective/sfus/presentations19/17.pdf)by Nalini Elkins
*   18: Practical Tracewrangling: Exploring capture file manipulation/extraction, by Jasper Bongertz

    *   [Presentation Video](https://www.youtube.com/watch?v=n1IKMoQE7yY "Presentation video on YouTube") (1:24:32)

*   19: [TCP SACK overview & impact on performance](/retrospective/sfus/presentations19/19.zip "Presentation slides"), by John Pittle

    *   [Presentation Video](https://www.youtube.com/watch?v=6cNVEc2nQUk "Presentation video on YouTube") (1:11:54)

*   20: IPv6 security assessment tools (aka IPv6 hacking tools), by Jeff Carrell

    *   [Presentation Video](https://www.youtube.com/watch?v=G9coloYylBc "Presentation video on YouTube") (1:35:25)

*   21: Troubleshooting slow networks, by Chris Greer

    *   [Presentation Video](https://www.youtube.com/watch?v=h9stVIfug5Y)(1:10:57)

*   22: [Analyzing Windows malware traffic with Wireshark (Part 1)](https://malware-traffic-analysis.net/2019/sharkfest/), by Brad Duncan

    *   [Presentation Video](https://www.youtube.com/watch?v=eQItiKZpuSc)(1:10:57)

*   23: [To send or not to send? How TCP congestion control algorithms work](/retrospective/sfus/presentations19/07-23.pdf), by Vladimir Gerasimov

    *   [Presentation Video](https://www.youtube.com/watch?v=EO8BaUwQHNI "Presentation video on YouTube") (1:30:56)

*   24: The packet doctors are in! Packet trace examinations with the experts, by Drs. Blok, Bongertz, and Landström
*   25: [Analyzing Windows malware traffic with Wireshark (Part 2)](https://malware-traffic-analysis.net/2019/sharkfest/), by Brad Duncan

    *   [Presentation Video](https://youtu.be/ibSqqWZq9sk) (1:04:26)

*   26: [TLS encryption & decryption: what every IT engineer should know about TLS](/retrospective/sfus/presentations19/02-26.pptx), by Ross Bagurdes
*   27: Developer bytes lightning talks, by Wireshark Core Developers
*   28: [Wireshark visualization TIPS & tricks](/retrospective/sfus/presentations19/28-37.pdf), by Megumi Takeshita
*   29: [Kismet & wireless security 101](/retrospective/sfus/presentations19/32.pdf), by Mike Kershaw

    *   [Presentation Video](https://www.youtube.com/watch?v=Kk4sImFR4z4 "Presentation video on YouTube") (1:20:16)

### **Thursday Classes**

*   30: [Using Wireshark to solve real problems for real people: step-by-step case studies in packet analysis](/retrospective/sfus/presentations19/30.pptx), by Kary Rogers

    *   [Presentation Video](https://www.youtube.com/watch?v=ClqlK7OEFCc "Presentation video on YouTube") (1:20:01)

*   31: [TCP split brain: compare/contrast TCP effects on client & server with Wireshark (Part 1)](/retrospective/sfus/presentations19/31.zip), by John Pittle

    *   [Presentation Video](https://www.youtube.com/watch?v=7uC1CLq8BaI "Presentation video on YouTube") (1:24:11)

*   32: [Kismet & wireless security 101](/retrospective/sfus/presentations19/32.pdf), by Mike Kershaw

    *   [Presentation Video](https://www.youtube.com/watch?v=Kk4sImFR4z4 "Presentation video on YouTube") (1:20:16)

*   33: Capture file format deep dive, by Jasper Bongertz

    *   [Presentation Video](https://youtu.be/IZ439VNvJqo "Presentation video on YouTube") (1:11:14)

*   34: [TCP split brain: compare/contrast TCP effects on client & server with Wireshark (Part 2)](/retrospective/sfus/presentations19/34.zip), by John Pittle

    *   [Presentation Video](https://www.youtube.com/watch?v=YpiORadeiy0 "Presentation video on YouTube") (1:27:25)

*   35: Solving the impossible, by Mike Canney

    *   [Presentation Video](https://youtu.be/YLg91sAcQdw "Presentation video on YouTube") (1:02:20)

*   36: A deep dive into LDAP: Everything you need to know to debug and troubleshoot LDAP packets, by Betty DuBois
*   37: [Wireshark visualization TIPS & tricks](/retrospective/sfus/presentations19/28-37.pdf), by Megumi Takeshita
*   38: Enrich your network visibility & analysis with Wireshark & ELK, by Tajul Ariffin

    *   [Presentation Video](https://youtu.be/IXdR3HWDB7A "Presentation video on YouTube") (1:05:55)

*   39: A walkthrough of the SharkFest Group & Individual Packet Challenges, by Sake Blok, Christian Landström, and Jasper Bongertz

* * *

#### A Word of Thanks

SharkFest'19 US proved to be a blazing success thanks to the generous, giving community in attendance. Particular thanks to Gerald Combs and his merry band of core developers for inspiring the many first-time participants, by opening with a keynote that illuminated the 20-year history of the project, to instructors who selflessly donated time and wisdom to educate and mentor participants, to sponsors who so generously provided the resources to make the conference possible, to the UC Berkeley Events team for their expert guidance, to tireless caterers who served up varied and delicious daily fare, and to a staff and volunteer crew who once again went overboard in making the conference as smooth and pleasant an experience as possible for attendees!

[SF19US Photo Gallery](https://photos.app.goo.gl/jxpxDZ5miMXH5WFW6)

#### Sponsor Videos
<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/videoseries?list=PLz_ZpPUgiXqOTlBn_W0x3tFstX6A0BY_6" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0;"></iframe></div>

#### Host Sponsor

[![](/img/sponsors/riverbed.png)](http://www.riverbed.com/)

#### Angel Shark Sponsors

[![](/img/sponsors/accedian-new.png)](https://accedian.com/) [![](/img/sponsors/comworth.png)](http://www.comworth.com.sg/) [![](/img/sponsors/endace.png)](https://www.endace.com/) [![](/img/sponsors/gigamon-new-big.jpg)](https://www.gigamon.com/) [![](/img/sponsors/influx.png)](https://www.influxdata.com/)

#### Tiger Shark Group Packet Competition Sponsor

[![](/img/sponsors/endace.png)](https://www.endace.com/)

#### Silky Shark Welcome Gift Sponsors

[![](/img/sponsors/counterflow.png)](https://counterflow.ai/) [![](/img/sponsors/profitap.png)](https://www.profitap.com/)

#### Copper Shark Welcome Dinner Sponsor

[![](/img/sponsors/bigswitch.png)](http://go.bigswitch.com/meet-big-switch-at-sharkfest-us-19.html) [![](/img/sponsors/sysdig.png)](https://sysdig.com/)

#### Honorary Sponsors

[![](/img/sponsors/lovemtytool_Logo.png)](http://www.lovemytool.com/)