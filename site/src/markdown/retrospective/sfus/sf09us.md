SharkFest'09 Retrospective
==========================

June 15th - 18th, 2009  
Stanford University | Stanford, California

### **Post Fest Reviews and Press**

*   [Keith Parsons](http://wlaniconoclast.blogspot.com/2009/06/sharkfest.html)
*   [Michael Patterson](http://www.plixer.com/blog/netflow-analyzer/sharkfest-2009-a-great-event/)
*   [Laura Chappell](http://laurachappell.blogspot.com/2009/06/iphone-youre-sexy-but-you-talk-too-much.html)
*   [Wireshark – ein Business-Modell steht Kopf](http://www.heise.de/open/Wireshark-ein-Business-Modell-steht-Kopf--/artikel/141265)
*   [SharkFest 2009: Zusammenkunft der Netzwerk-Haie](http://www.heise.de/newsticker/SharkFest-2009-Zusammenkunft-der-Netzwerk-Haie--/meldung/140870)
*   [Show Notes from Sharkfest 2009](http://www.lovemytool.com/blog/2009/06/joke_snelders1.html)

### **Pictures**

*   [Joan Snelders](http://www.facebook.com/album.php?aid=91415&id=51502216870&l=b587bd9039)
*   [Angela Sherman](http://www.facebook.com/album.php?aid=2014140&id=1372519431&l=cc0c1de03b)

### **Keynote Presentation**

[Key-2 (Roberts) Evolution of the Internet](/retrospective/sfus/presentations09/Key-2_Roberts_Evolution%20of%20the%20Internet.ppt)

### **Basic Track Presentations**

*   [BU-2 (Tompkins) How Protocols Work](/retrospective/sfus/presentations09/bu-2-tompkins-gearbit-wireshark-how-protocols-work-sharkfest09.pdf)
*   [BU-3 (Carpio) Fundamentals Of Passive Monitoring Access](/retrospective/sfus/presentations09/BU3_Carpio_FundamentalsOfPassiveMonitoringAccessFINAL.pps)
*   [BU-4 (DuBois) I just downloaded Wireshark - Now what do I do](/retrospective/sfus/presentations09/BU-4%20(DuBois)%20I%20just%20downloaded%20Wireshark%20-%20Now%20what%20do%20I%20do.zip)
*   [BU-5 (Leutert) Analyzing WLANs with Wireshark & AirPcap](/retrospective/sfus/presentations09/BU5_Leutert_Analyzing%20WLANs%20with%20Wireshark%20&%20AirPcap.pdf)
*   [BU-6 (Webb) Powering Network Visibility](/retrospective/sfus/presentations09/BU-6_Webb_PoweringNetworkVisibility_rev1.zip)
*   [BU-7 (O'Donnell) The Reality of 10G Analysis](/retrospective/sfus/presentations09/BU-7_O'Donnell_The%20Reality%20of%2010G%20Analysis.pptx)
*   BU-8 (Degioanni) Complementing Wireshark in Wireless Troubleshooting.  
    _Live demonstration Session, no slides available._
*   [BU-9 (Tompkins) Wireshark Charts & I/O Graphs](/retrospective/sfus/presentations09/bu-9-tompkins-gearbit-wireshark_charts_&_io_graphs-sharkfest09.pdf)
*   [BU-10 (Bardwell) Wireshark Saves the WLAN!](/retrospective/sfus/presentations09/BU-10_Bardwell_Sharkfest2009-HANDOUT-J0615.pdf)
*   [BU-11 (DuBois) SPAN vs. Taps - When to use what - Benefits and Caveats](/retrospective/sfus/presentations09/BU-11%20(DuBois)%20SPAN%20vs.%20Taps%20-%20When%20to%20use%20what%20-%20Benefits%20and%20Caveats.zip)

### **Advanced Track Presentations**

*   [AU-1 (Heine) Wireless Network Optimization with Wireshark](/retrospective/sfus/presentations09/AU-1_Heine_Wireless%20Network%20Optimization%20with%20Wireshark.pdf)
*   AU-3 (Degioanni) CACE Labs SNEAK PEAK of Pilot v2.0.  
    _Live demonstration Session, no slides available._
*   [AU-2 (Blok) SSL Troubleshooting with Wireshark and Tshark](/retrospective/sfus/presentations09/AU2_Blok_SSL_Troubleshooting_with_Wireshark_and_Tshark.pps)
*   [AU-4, AU-5 (Bae) Protocol Analysis in a Complex Enterprise](/retrospective/sfus/presentations09/AU-5_Bae_Protocol%20Analysis%20in%20a%20Complex%20Enterprise.ppt)
*   [AU-6 (Patterson) Successful Ways to Use NetFlow](/retrospective/sfus/presentations09/AU-6_Patterson_SuccessWithNetFlow.zip)
*   [AU-7 (Hinz) Industrial Ethernet](/retrospective/sfus/presentations09/au7-hinz-yr20-industrialethernet-sharkfest09.ppt)
*   [AU-8 (Tompkins) Finding the Latency](/retrospective/sfus/presentations09/au-8-tompkins-gearbit-wireshark-finding-the-latency-sharkfest09.pdf)
*   [AU-9 (Walberg) Expose VoIP Problems with Wireshark](/retrospective/sfus/presentations09/AU9-Walberg-Expose%20VoIP%20problems%20with%20wireshark.ppt)
*   [AU-10 (Chappell) Network Forensics - Wireshark as Evidence Collector](/retrospective/sfus/presentations09/AU-10_Chappell_Network_Forensics-Wireshark_as_Evidence_Collector.pptx)
*   [AU-11 (Tüxen) SCTP](/retrospective/sfus/presentations09/AU11_Tuexen_SCTP.pdf)
*   [AU-12 (Chappell) Tips and Tricks - Enterprise Case Studies](/retrospective/sfus/presentations09/AU-12_Chappell_Tips_and_Tricks-Enterprise_Case_Studies.pptx)

### **Developer Track Presentations**

*   [DT-1 (Combs) Writing Wireshark Dissectors & Plugins](/retrospective/sfus/presentations09/DT-1_Combs_Writing%20Wireshark%20Dissectors%20and%20Plugins.zip)
*   [DT-2 (Tüxen) Source Code](/retrospective/sfus/presentations09/DT-2_Tuexen_SourceCode.pdf)
*   [DT-2 (Combs) API Additions](/retrospective/sfus/presentations09/DT-2_Combs_API%20Additions.pdf)
*   [DT-3 (Donnelly) Now and Then How and When](/retrospective/sfus/presentations09/DT-3_Donnelly_Now%20and%20Then%20How%20and%20When.ppt)
*   [DT-5 (Varenni) WinPcap Do's & Dont's](/retrospective/sfus/presentations09/DT5_Varenni_WinPcapDosDonts.pptx)
*   [DT-6 (Bjørlykke) Lua Scripting in Wireshark](/retrospective/sfus/presentations09/DT06_Bjorlykke_Lua%20Scripting%20in%20Wireshark.pdf)
*   [DT-7 (Kershaw) Get Thinking About WiFi Security!](/retrospective/sfus/presentations09/DT-7_Kershaw_Get%20Thinking%20About%20WiFi%20Security!.ppt)
*   [DT-8 (Sanders) Wireshark in a Multi-Core Environment Using Hardware Acceleration](/retrospective/sfus/presentations09/DT-8-Napatech-Presentation-Sharkfest-2009.pdf)
*   [DT-9 (Fisher) Extend Wireshark With GTK](/retrospective/sfus/presentations09/DT9_Fisher_ExtendWiresharkWithGTK.ppt)
*   [DT-10 (Varenni) Writing Your Own Packet Capture Tool With WinPcap & AirPcap](/retrospective/sfus/presentations09/DT10_Varenni_WritingYourOwnPacketCaptureToolWithWinPcapAndAirPcap.zip)

* * *

#### A Word of Thanks

Another Sharkfest has come and gone, and we thank each participant, sponsor, presenter, keynote speaker, coordinator, caterer, staff and general support person for making this a successful, richly educational event once again. All session presentations are available from this page, and recordings of many sessions can be found at [www.lovemytool.com](www.lovemytool.com).