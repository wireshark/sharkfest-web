SharkFest'17 Retrospective
==========================

June 19th - 22nd, 2017  
Carnegie Mellon University | Pittsburgh, PA

### **Keynote Presentations**

**The Past, Present & Future of the Wireshark Project**  
Gerald Combs
<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/oqxw3ePtZTM" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0;"></iframe></div>

**[Experience with the eXpressive Internet Architecture](/retrospective/sfus/presentations17/steenkiste-keynote.pdf)**  
Peter Steenkiste
<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/WyVgEof2uKg" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0;"></iframe></div>

### **SharkFest’17 US Attendee Feedback**

> “It’s a wonderful time to have the privilege to participate with such talent and good will as that which is found in the Wireshark community. I believe that as the risks present within the Internet increase, Wireshark and the Wireshark community stand poised at the intersection of the Internet nexus to identify, mitigate, and secure network resources everywhere.  
>   
> Thank you again for all that you do to bring together the Wireshark community annually for SharkFest!”

> “Thank you for organizing a fabulous event for everyone. It was a great experience and I learned a lot.”

> “In my humble opinion, Sharkfest is a great success. Many of the presentations show common case issues, and how Wireshark helps to identify them in their own ways. Most of the people I met at Sharkfest are small to medium size companies. We all have similar issues, but sometimes we just don’t know how and where to begin tackling the issue. It looks so easy when the experts present, trust me it’s not quite similar in real life. This conference helps us to navigate through these obstacle courses and helps us to do our job better.”

> “Thank you for the great SharkFest' 17 US conference in Pittsburgh, Pennsylvania for the past days. All of you – and don’t forget the guys behind the scenes - have done a great job again and we all together survived.”

### **Blogs**

[SharkFest'17 US Recap](https://blog.packet-foo.com/2017/07/sharkfest-2017-us-recap-10-years-of-sharkfest/), by Jasper Bongertz

### **SharkBytes**
<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/videoseries?list=PLz_ZpPUgiXqPH_KUS074DvdLM0NSG8c8C" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0;"></iframe></div>

### **Tuesday Classes**

*   01: [Practical Tracewrangling: Exploring Capture File Manipulation/Extraction Scenarios - Part 1](/retrospective/sfus/presentations17/01-04.pdf), by Jasper Bongertz

*   [Presentation Video](https://youtu.be/d62QJlSqlgs "Presentation video on YouTube") (1:12:42)

*   02: [An Introduction to Wireshark: Rookie to Vet in 75 Minutes](https://prezi.com/view/wsDlLM8dJSTl8P3pgYam/), by Betty DuBois
*   03: [Using Wireshark to Solve Real Problems for Real People: Step-by-Step Real-World Case Studies in Packet Analysis](/retrospective/sfus/presentations17/03.pdf), by Kary Rogers

*   04: [Practical Tracewrangling: Exploring Capture File Manipulation/Extraction Scenarios - Part 2](/retrospective/sfus/presentations17/01-04.pdf), by Jasper Bongertz

*   [Presentation Video](https://youtu.be/UOvs0e5df8U "Presentation video on YouTube") (1:14:11)

*   05: [Network Security...Haven't We Solved It Yet?](/retrospective/sfus/presentations17/05.pdf), by Mike Kershaw
*   06: [Workflow-based Analysis of Wireshark Traces: Now we can all be Experts](/retrospective/sfus/presentations17/06.pdf), by Paul Offord

*   [Presentation Video](https://youtu.be/7800vK1FAh0) (1:04:26)

*   07: [Undoing the Network Blame Game and Getting to the Real Root Cause of Slow Application Performance](/retrospective/sfus/presentations17/07.pptx), by Chris Greer

*   [Presentation Video](https://youtu.be/o53DFI_Y3QQ "Presentation video on YouTube") (1:14:06)

*   08: [Command Line Review of Wireshark CLI Tools, tshark & more](/retrospective/sfus/presentations17/08.pdf), by Christian Landström
*   09: [Designing a Requirements-Based Packet Capture Strategy](/retrospective/sfus/presentations17/09.pdf), by John Pittle
*   10: [Knowing the Unknown: How to Monitor & Troubleshoot an Unfamiliar Network](/retrospective/sfus/presentations17/10.pdf), by Luca Deri

*   [Presentation Video](https://youtu.be/4jZw3kFQ0jI?list=PLz_ZpPUgiXqNzojkgOSULR7RK8X7vdID- "Presentation video on YouTube") (1:18:28)

*   11: [HANDS-ON TCP Analysis](/retrospective/sfus/presentations17/11.pdf), by Jasper Bongertz
*   12: [Baselining with Wireshark to Identify & Stop Unwanted Communications](/retrospective/sfus/presentations17/12.pdf), by Jon Ford

### **Wednesday Classes**

*   13: [Augmenting Packet Capture with Contextual Meta-Data: the What, Why & How](/retrospective/sfus/presentations17/13.pptx), by Dr. Stephen Donnelly

*   [Presentation Video](https://youtu.be/YnYAJSM-Jms "Presentation video on YouTube") (1:06:37)

*   14: [Wireshark Case Study Exploration](/retrospective/sfus/presentations17/14.pdf), by Sake Blok
*   15: [Wireshark & Time: Accurate Handling of Timing When Capturing Frames](/retrospective/sfus/presentations17/15.pdf), by Werner Fischer
*   16: Hands-On Analysis of Multi-Point Captures – Part 1, by Jasper Bongertz and Christian Landström

*   [Presentation Video](https://youtu.be/6Yf1f3_YBnw "Presentation video on YouTube") (1:16:37)

*   17: [WiFiBeat...Visualize Data with Kibana & ElasticSearch](/retrospective/sfus/presentations17/17.pptx)by Thomas d'Otreppe
*   18: [Analyzing Exploit Kit Traffic with Wireshark](/retrospective/sfus/presentations17/18.pptx), by Bradley Duncan
*   19: Hands-On Analysis of Multi-Point Captures – Part 2, by Jasper Bongertz and Christian Landström

*   [Presentation Video](https://youtu.be/ll2JGgHHA3E "Presentation video on YouTube") (1:08:02)

*   20: [Work-Shmerk/Mirai-Shmiraii: What are Those Evil Little IoT Devices Doing & How Can You Control Them?](/retrospective/sfus/presentations17/20.pdf "Presentation slides"), by Brad Palm
*   21: [Analysis Visualizations: Creating charts inside and outside of Wireshark to speed up your Analysis](/retrospective/sfus/presentations17/21.pdf), by Robert Bullen
*   22: [Understanding Throughput & TCP Windows: A Walk-Through of the Factors that can limit TCP Throughput Performance](/retrospective/sfus/presentations17/22.pdf "Presentation slides"), by Kary Rogers

*   [Presentation Video](https://youtu.be/tyk2-0MY9p0 "Presentation video on YouTube") (1:07:08)

*   23: [Top 10 Wireshark TIPS & Tricks](/retrospective/sfus/presentations17/23.zip), by Megumi Takeshita
*   24: [Undoing the Network Blame Game and Getting to the Real Root Cause of Slow Application Performance](/retrospective/sfus/presentations17/07.pptx), by Chris Greer

*   [Presentation Video](https://youtu.be/o53DFI_Y3QQ "Presentation video on YouTube") (1:14:06)

### **Thursday Classes**

*   25: [Workflow-based Analysis of Wireshark Traces: Now we can all be Experts](/retrospective/sfus/presentations17/06.pdf), by Paul Offord

*   [Presentation Video](https://youtu.be/7800vK1FAh0) (1:04:26)

*   26: Network Security...Haven't We Solved it Yet?, by Mike Kershaw
*   27: Network Forensics with Wireshark, by Laura Chappell
*   28: The Doctor is In! Packet Trace Reviews with the Experts, by Hansang Bae, Jasper Bongertz, Christian Landström, Sake Blok
*   29: [A Web-Based Approach to Enhance Network Packet Capture & Decode Analysis Techniques using the Wireshark Command Line Tools](/retrospective/sfus/presentations17/29.pdf), by Ronald Henderson
*   30: Using the Python/Django Web Framework to Build a Remote Packet Capture Portal with tshark, by Kevin Burns
*   31: [SMB/CIFS Analysis: Using Wireshark to Efficiently Analyze & Troubleshoot SMB/CIFS](https://prezi.com/view/JAbMcRiCJvox1fT5aSBX/ "Presentation slides"), by Betty DuBois
*   32: [Writing a Wireshark Dissector: 3 Ways to Eat Bytes](/retrospective/sfus/presentations17/32.7z), by Graham Bloice
*   33: Wireshark & Time: Accurate Handling of Timing When Capturing Frames, by Werner Fischer
*   34: [How tshark saved my SDN Forensics: Hands-on tshark Usage with a Minor Python Connection](/retrospective/sfus/presentations17/34.pdf), by Mike McAlister, Joseph Bull
*   35: My Life as a Troubleshooter: So what did you do today, Dad?, by Graeme Bailey
*   36: [Validating Your Packet Capture: How to be sure you’ve captured correct & complete data for analysis](/retrospective/sfus/presentations17/36.pdf), by Scott Haugdahl, Mike Canney
*   37: [Back to the Packet Trenches (Part 1)](/retrospective/sfus/presentations17/37-40.zip), by Hansang Bae
*   38: Wireshark Tips & Tricks, by Laura Chappell
*   39: [Knowing the Unknown: How to Monitor & Troubleshoot an Unfamiliar Network](/retrospective/sfus/presentations17/10.pdf), by Luca Deri

*   [Presentation Video](https://youtu.be/4jZw3kFQ0jI?list=PLz_ZpPUgiXqNzojkgOSULR7RK8X7vdID- "Presentation video on YouTube") (1:18:28)

*   40: [Back to the Packet Trenches (Part 2)](/retrospective/sfus/presentations17/37-40.zip), by Hansang Bae
*   41: Analyzing Exploit Kit Traffic with Wireshark, by Bradley Duncan
*   42: [TCP SACK Overview & Impact on Performance](/retrospective/sfus/presentations17/42.pdf), by John Pittle

* * *

#### A Word of Thanks

SharkFest'17 US, the tenth anniversary of the conference, was a roaring success thanks to the highly engaged community of core developers and Wireshark users in attendance. Special thanks to Gerald Combs for tirelessly, fearlessly guiding the Wireshark open source project and maintaining its relevancy, to core developers for traveling long distances and braving severe storm-related delays to advance Wireshark code together, to Laura Chappell for delivering another excellent troubleshooting class, jumping in at the last minute to replace presenters unable to keep their commitments due to travel complications, and for creating another highly-anticipated Packet Challenge, to Dr. Peter Steenkiste for his provocative keynote, to Sake Blok for the many man-hours dedicated to creating a thrilling group packet competition, to a staff and volunteer crew who went far beyond caring to serve attendees during the conference, to instructors who voluntarily shuffled lives and schedules to educate participants and learn from one another, to sponsors who so generously provided resources that made the conference possible, to the CMU social hosts who made our social events truly social, and to the Carnegie Mellon Conference Events team for working through months of minutiae to help stage the conference on the rare and beautiful CMU campus.

#### Host Sponsors

[![](/img/sponsors/riverbed.png)](http://www.riverbed.com/) [![](/img/sponsors/wiresharku-white.png)](http://www.wiresharktraining.com/)

#### Angel Shark Sponsors

[![](/img/sponsors/bigswitch.png)](http://www.bigswitch.com/) [![](/img/sponsors/datacom.png)](http://datacomsystems.com/) [](https://www.endace.com/)[](https://www.extrahop.com/)[![](/img/sponsors/gigamon-new.png)](https://www.gigamon.com/) [![](/img/sponsors/ixia.png)](http://www.ixiacom.com/)[![](/img/sponsors/pv.png)](https://www.performancevision.com/)

#### Wireshark Group Packet Challenge Sponsor

[![](/img/sponsors/garland.png)](https://www.garlandtechnology.com/)

#### Welcome Gift Sponsor

[![](/img/sponsors/fmadio.jpg)](http://fmad.io/) [![](/retrospective/sfus/img/sponsors16eu/ntop-no-space.png)](https://ntop.org/)

#### Breaks Sponsor

[![](/img/sponsors/truepath.png)](http://truepathtechnologies.com/)

#### In Absentia Sponsors

[![](/img/sponsors/accolade.png)](http://accoladetechnology.com/)

#### Honorary Sponsors

[![](/img/sponsors/lovemtytool_Logo.png)](http://www.lovemytool.com/)