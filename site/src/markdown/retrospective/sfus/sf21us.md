SharkFest'21 Virtual US Retrospective
=====================================

September 12th - 17th, 2021  
Online

### **Keynote Presentations**

#### **Latest Wireshark Developments & Road Map**  
Gerald Combs
<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/MwTUuQ3lRUs" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0;"></iframe></div>

#### **The Zed Project: Stumbling Upon a New Data Model for Search and Analytics while Hacking Packets**  
Steve McCanne, Coding CEO, Brim Security
<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/3eHiZ6omzsE" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0;"></iframe></div>

### **Thursday Classes**

*   01: [Analyzing DNS from the Server Perspective](/retrospective/sfus/presentations21/01.zip), by Betty DuBois
*   [Presentation Video](https://www.youtube.com/watch?v=5X97uC42iXE "Presentation video on YouTube")

*   02: [Network Forensics Analysis](/retrospective/sfus/presentations21/02.pdf), by Rami AlTalhi
*   [Presentation Video](https://www.youtube.com/watch?v=rL8V0_mXwDA "Presentation video on YouTube")

*   03: [Visualizing TLS Encryption - making sense of TLS in Wireshark](/retrospective/sfus/presentations21/03.pptx), by Ross Bagurdes
*   [Presentation Video](https://www.youtube.com/watch?v=nmOGc44w96E "Presentation video on YouTube")

*   04: Analyzing Megalodon Files, by Jasper Bongertz
*   [Presentation Video](https://www.youtube.com/watch?v=9k9uVHkRxAw "Presentation video on YouTube")

*   05: [Hello, what’s your name? An overview of Wireshark’s name resolution options (and it is not only for IP addresses!)](/retrospective/sfus/presentations21/05.pdf), by Sake Blok
*   [Presentation Video](https://www.youtube.com/watch?v=QihcWey0HUo "Presentation video on YouTube")

*   06: [Wireshark in use on LTE and 5G networks](/retrospective/sfus/presentations21/06.pptx), by Mark Stout
*   [Presentation Video](https://www.youtube.com/watch?v=uNmcGNzJ2xc "Presentation video on YouTube")

*   07: [Intro to QUIC - The TCP Killer?](/retrospective/sfus/presentations21/07.pptx), by Chris Greer
*   [Presentation Video](https://www.youtube.com/watch?v=jQ1GCkhwGTg "Presentation video on YouTube")

*   08: Network Forensic Case Studies: Those Who Don't Learn from the Past Are Doomed to Repeat It, by Phill Shade
*   [Presentation Video](https://www.youtube.com/watch?v=mHQo5ETTtHI "Presentation video on YouTube")

*   09: Looking for "Packets" in all the "Right" Places, by Patrick Kinnison
*   [Presentation Video](https://www.youtube.com/watch?v=l2xW1ETofQg "Presentation video on YouTube")

*   10: Back to the Packet Trenches, by Hansang Bae
*   [Presentation Video](https://www.youtube.com/watch?v=PfRihPtQ6S0 "Presentation video on YouTube")

*   11: [School from home: Watching the Wire with Wireshark](/retrospective/sfus/presentations21/11.zip), by Anthony Efantis
*   [Presentation Video](https://www.youtube.com/watch?v=a8gf7ZXLxRo "Presentation video on YouTube")

*   12: Wireshark and Enterprise Packet Capture, by Dr. Stephen Donnelly
*   [Presentation Video](https://www.youtube.com/watch?v=3e6AITbLEgM "Presentation video on YouTube")

### **Friday Classes**

*   13: [Analysis and Troubleshooting of IPsec VPNs](/retrospective/sfus/presentations21/13.zip), by Jean-Paul Archier
*   [Presentation Video](https://www.youtube.com/watch?v=wx82fqQQ6rA "Presentation video on YouTube")

*   14: [How smart are my “things”? A traffic analysis of IoT Devices](/retrospective/sfus/presentations21/14.zip), by Simone Mainardi
*   [Presentation Video](https://www.youtube.com/watch?v=oMfDWzURUx0 "Presentation video on YouTube")

*   15: The Packet Doctors are in! Packet trace examinations with the experts, by Chris Greer, Sake Blok, Betty DuBois, and Kary Rogers

*   17: [When it’s NOT a “Network Problem” – Identifying Higher-Layer Issues in Packet Data](/retrospective/sfus/presentations21/17.pdf), by Wes Morgan
*   [Presentation Video](https://www.youtube.com/watch?v=kp-JTpPdpzk "Presentation video on YouTube")

*   18: [Intrusion Analysis and Threat Hunting with Suricata](/retrospective/sfus/presentations21/18.pptx), by Josh Stroschein and Peter Manev
*   [Presentation Video](https://www.youtube.com/watch?v=mbwU8y6H5Zo "Presentation video on YouTube")

*   19: [How I Learned to Stop Worrying and Love the PCAP](/retrospective/sfus/presentations21/19.zip "Presentation slides"), by Kary Rogers
*   [Presentation Video](https://www.youtube.com/watch?v=1JR1WnxtLGA "Presentation video on YouTube")

*   20: Build Your Own IPv6 Learning Lab – for FREE (part 1), by Jeff Carrell
*   [Presentation Video](https://www.youtube.com/watch?v=RzL4VYq8DOM "Presentation video on YouTube")

*   21: [TCP SACK overview & impact on performance](/retrospective/sfus/presentations21/21.zip), by John Pittle
*   [Presentation Video](https://www.youtube.com/watch?v=hfxM_kriYXc "Presentation video on YouTube")

*   22: Build Your Own IPv6 Learning Lab – for FREE (part 2), by Jeff Carrell
*   [Presentation Video](https://www.youtube.com/watch?v=GP8dNLdtBPg "Presentation video on YouTube")

*   23: [Wireshark and WiFi: capture techniques and challenges](/retrospective/sfus/presentations21/07-23.pdf), by George Cragg
*   [Presentation Video](https://www.youtube.com/watch?v=p3Ik_pcwp9c "Presentation video on YouTube")

*   24: [Capturing goodies: Wireshark on iPad pro and utilization of extcap interfaces](/retrospective/sfus/presentations21/24.pdf), by Megumi Takeshita
*   [Presentation Video](https://www.youtube.com/watch?v=bToY7Hxq7WY "Presentation video on YouTube")


* * *

#### Host Sponsor

[![](/img/sponsors/riverbed.png)](http://www.riverbed.com/)

#### Angel Shark Sponsors

[![](/img/sponsors/endace.png)](https://www.endace.com/) [![](/img/sponsors/gnet.png)](https://gnet-inc.com/) [![](/img/sponsors/fmadio.jpg)](https://fmad.io/)