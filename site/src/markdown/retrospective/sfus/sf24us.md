SharkFest'24 US Retrospective
=====================================

June 15-20
Fairfax, VA

### **Keynote Presentations**

#### **Ecosystem Expansion**  
Gerald Combs & Friends
<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/wbXvUYXeIzk" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0;"></iframe></div>

[Keynote Slides](/retrospective/sfus/presentations24/gerald-keynote.pdf)

#### **Steering the Wireshark project into the future**  
Roland Knall, Chris Greer, Stephen Donnelly, Ron Groulx, Nigel Douglas, and John Modlin
<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/cVc84Bmz1EA" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0;"></iframe></div>

<!-- ### [Presentation Video Playlist](https://www.youtube.com/playlist?list=PLz_ZpPUgiXqPLdY17eKQGQLe8NVgcJTt6) -->

### **Tuesday Classes**

*   01: [Finding Duplications with Wireshark](/retrospective/sfus/presentations24/01.pdf), by Megumi Takeshita
    <!-- *   [Presentation Video](https://youtu.be/4hMT0kcW39g "Presentation video on YouTube") -->
*   02: [Real-world post-quantum TLS](/retrospective/sfus/presentations24/02.pdf), by Peter Wu
    <!-- *   [Presentation Video](https://youtu.be/L94vuo-5wSQ "Presentation video on YouTube") -->
*   03: [3GPP and me! A walk through of mobility](/retrospective/sfus/presentations24/03.pptx), by Mark Stout
*   04: [Monitoring and Troubleshooting Without Packet Traces](/retrospective/sfus/presentations24/04.pdf), by Chris Hull
    <!-- *   [Presentation Video](https://youtu.be/a6rs1kPK5lc "Presentation video on YouTube") -->
*   05: [Using Wireshark to Solve Real Problems for Real People](https://packetbomb.com/sf24us/), by Kary Rogers
    *   [Presentation PDF](/retrospective/sfus/presentations24/05.pdf)
    <!-- *   [Presentation Video](https://youtu.be/wG0tN9BDh1A "Presentation video on YouTube") -->
*   06: [Bake your own Pi: Building a TLS Decrypting Wireless Traffic Sniffer](/retrospective/sfus/presentations24/06.pdf), by Ross Bagurdes
    <!-- *   [Presentation Video](https://youtu.be/25h6it4I254 "Presentation video on YouTube") -->
*   07: [Gotta catch 'em all! A field test of portable gigabit taps](/retrospective/sfus/presentations24/07.pdf), by Sake Blok
    <!-- *   [Presentation Video](https://youtu.be/rAgy3nb9Ucs "Presentation video on YouTube") -->
*   08: [Wireshark plus Advanced Analytics – Better Together (2 part session)](/retrospective/sfus/presentations24/08-10.zip), by John Pittle
    <!-- *   [Presentation Video](https://youtu.be/A5qiUuE-paw "Presentation video on YouTube") -->
*   09: [Cloud doesn’t have Packets!](/retrospective/sfus/presentations24/09.pptx), by Stephen Donnelly
    <!-- *   [Presentation Video](https://youtu.be/akDHB6s4KYU "Presentation video on YouTube") -->
*   10: [Wireshark plus Advanced Analytics – Better Together (2 part session)](/retrospective/sfus/presentations24/08-10.zip), by John Pittle
    <!-- *   [Presentation Video](https://youtu.be/1eJHqyyjHqk "Presentation video on YouTube") -->
    <!-- *   [Presentation Video](https://youtu.be/BHNiQ-QLVVw "Presentation video on YouTube") -->

### **Wednesday Classes**
*   11: [Enhancing Wi-Fi Networks with AI: A Deep Dive into Machine Learning for Wi-Fi Health Checks](/retrospective/sfus/presentations24/11.pptx), by Murat Bilgic
    <!-- *   [Presentation Video](https://youtu.be/4lSLLo5btOQ "Presentation video on YouTube") -->
*   12: [TCP Retransmissions - How many is "too" many?](/retrospective/sfus/presentations24/12.pdf), by Betty DuBois\
        &nbsp;&nbsp;&nbsp;&nbsp; • [Presentation Files](https://workdrive.zohoexternal.com/external/9cc9288ea672ae0e7493a4b711e6fe4f65c79e4c730670886135b2ea6d3beaf2)\
        &nbsp;&nbsp;&nbsp;&nbsp; • [Presentation Profiles](https://gitlab.com/WiresharkProfiles/profiles)
*   13: [Will QUIC Kill TCP?](https://github.com/packetpioneer/sfus24), by Chris Greer
*   14: [Passive Fingerprinting Methods for IoT Profiling](/retrospective/sfus/presentations24/14.pptx), by Asaf Fried
*   15: The Packet Doctors are in! Packet trace examinations with the experts
*   16: [Filters from a novice; Back to the Basics](/retrospective/sfus/presentations24/16.pptx), by Kirsten Stoner & Karinne Bessette
    <!-- *   [Presentation Video](https://youtu.be/4hMT0kcW39g "Presentation video on YouTube") -->
*   17: [Dissector developer design notes](/retrospective/sfus/presentations24/17.pdf), by Jaap Keuter
    <!-- *   [Presentation Video](https://youtu.be/Cq6yj9se9M4 "Presentation video on YouTube") -->
*   18: [Smart Networks: Automating Analysis and Troubleshooting with AI Chatbots](#), by Roland Knall
*   19:  [An API-Driven approach to automating packet captures in cloud-native systems](/retrospective/sfus/presentations24/19.pdf), by Nigel Douglas
    <!-- *   [Presentation Video](https://youtu.be/q7RdLva_244 "Presentation video on YouTube") -->

### **Thursday Classes**
*   20:  [Get comfy with TCP handshakes](https://github.com/packetpioneer/sfus24), by Chris Greer
    <!-- *   [Presentation Video](https://youtu.be/nTQYA432h_k "Presentation video on YouTube") -->
*   21: [Packet-Guided Infrastructure Optimization](https://github.com/je-clark/sf24us_infrastructure_optimization), by Josh Clark
*   22: [Advanced TCP Troubleshooting](/retrospective/sfus/presentations24/22.pdf), by Jasper Bongertz
    <!-- *   [Presentation Video](https://youtu.be/dttGKC0qEZk "Presentation video on YouTube") -->
*   23: [Three-dimensional display filters with MATE](/retrospective/sfus/presentations24/23.pdf), by Chuck Craft
    <!-- *   [Presentation Video](https://youtu.be/ZBh2nVSfdnQ "Presentation video on YouTube") -->

* * *

#### Angel Shark Sponsors

[![](/img/sponsors/allegro.png)](https://www.allegropackets.com/) [![](/img/sponsors/b-yond.webp)](https://www.b-yond.com/) [![](/img/sponsors/cpacket.png)](http://www.cpacket.com/index.php) [![](/img/sponsors/endace.png)](https://endace.com/) [![](/img/sponsors/liveaction.png)](https://www.liveaction.com/) [![](/img/sponsors/profitap.jpg)](https://profitap.com/)[![](/img/sponsors/sysdig.png)](http://www.sysdig.com/) [![](/img/sponsors/riverbed.png)](https://www.riverbed.com/) [![](/img/sponsors/veeam.png)](https://www.veeam.com/)
