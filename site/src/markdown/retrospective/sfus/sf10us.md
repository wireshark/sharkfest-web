SharkFest'10 Retrospective
==========================

June 14th - 17th, 2010  
Stanford University | Stanford, California

### **Post Fest Reviews and Press**

[Love My Tool](http://www.lovemytool.com/blog/sharkfest/)

### **Pictures**

Joke Snelders [Photos](http://www.facebook.com/album.php?aid=182664&id=51502216870)  

### **Keynote Presentation**

[Key-1 (Saal) Reading the Traffic: Learning Behavior from Watching Bits](/retrospective/sfus/presentations10/Key-2_Saal%20Reading%20the%20Traffic%20-%20Learning%20Behavior%20from%20Watching%20Bits.pdf)

### **Basic Track Presentations**

*   [B-1 (Bardwell) 802.11 Secrets Revealed – Part 1: RF Signal Propagation](/retrospective/sfus/presentations10/B-1_Bardwell-Secrets_Revealed-PART_1.pdf)
*   [B-2 (Tompkins) Analyzing TCPIP Networks with Wireshark](/retrospective/sfus/presentations10/B-2_Tompkins%20Analyzing%20TCPIP%20Networks%20with%20Wireshark.pdf)
*   [B-3 (Patterson) Where NetFlow and Packet Capture Complement Each Other](/retrospective/sfus/presentations10/B-3_Patterson%20Where%20NetFlow%20and%20Packet%20Capture%20Complement%20Each%20Other.ppt)
*   [B-4 (Carrell) Network Access Security – It's Broken, Now What?](/retrospective/sfus/presentations10/B-4_Carrell_Network_Access_Security-Broken_Now_What.pdf)
*   [B-5 (Parsons) HANDS-ON LAB: WLAN Analysis with Wireshark & AirPcap  
    Exercises](/retrospective/sfus/presentations10/B-5_Parsons%20HANDS-ON%20LAB%20-%20WLAN%20Analysis%20with%20Wireshark%20&%20AirPcap.pdf)
*   [B-5 (Parsons) Exercise Files](/retrospective/sfus/presentations10/B-5_Parsons%20HANDS-ON%20LAB%20-%20WLAN%20Analysis%20with%20Wireshark%20&%20AirPcap%20Exercises.pdf)
*   [B-6 (Leutert) Discovering IPv6 with Wireshark](/retrospective/sfus/presentations10/B-6_Leutert%20Discovering%20IPv6%20with%20Wireshark.pdf)
*   [B-7 (Battaglia) TAPS Demystified](/retrospective/sfus/presentations10/B-7_Battaglia%20TAPS%20Demystified.ppt)
*   [B-8 (Chappell) 10 Cool Things You Should Know How To Do with Wireshark](/retrospective/sfus/presentations10/B-8_Chappell%2010%20Cool%20Things%20You%20Should%20Know%20How%20To%20Do%20with%20Wireshark.pdf)
*   [B-9 (Bardwell) 802.11 Secrets Revealed – Part 2: Bit Transmission & Channel Capacity](/retrospective/sfus/presentations10/B-9_Bardwell-Secrets_Revealed-PART_2.pdf)
*   [B-10 (Bongertz) Wireshark vs. "the Cloud"](/retrospective/sfus/presentations10/B-10_Bongertz%20Wireshark%20vs.%20the%20Cloud.pdf)
*   [B-11 (Bae) Basic TCP/IP Analysis](/retrospective/sfus/presentations10/B-11_Bae%20Basic%20TCPIP%20Analysis.ppt)

### **Advanced Track Presentations**

*   [A-1 (Bae) Stump the Expert! Packet Trace Whispering](/retrospective/sfus/presentations10/A-1_Bae%20Stump%20the%20Expert!%20Packet%20Trace%20Whispering.ppt)
*   [A-2 (Belcher) Advanced Threat Intelligence and Session Analysis](/retrospective/sfus/presentations10/A-2_Belcher%20Advanced%20Threat%20Intelligence%20and%20Session%20Analysis.pptx)
*   [A-3 (Walberg) VoIP Troubleshooting](/retrospective/sfus/presentations10/A-3_Walberg%20VoIP%20Troubleshooting.ppt)
*   [A-4 (Desai) Visibility into the DMZ with Wireshark](/retrospective/sfus/presentations10/A-4_Desai%20Visibility%20into%20the%20DMZ%20with%20Wireshark.pdf)
*   [A-5 (LDegioanni) The Shark Distributed Monitoring System](/retrospective/sfus/presentations10/A-5_LDegioanni%20The%20Shark%20Distributed%20Monitoring%20System.ppt)
*   [A-6 (Blok) HANDS-ON LAB: Using Wireshark Command Line Tools and Scripting](/retrospective/sfus/presentations10/A-6_Blok%20HANDS-ON%20LAB%20-%20Using%20Wireshark%20Command%20Line%20Tools%20and%20Scripting.zip)
*   [A-6 (Blok) Exercise Files](/retrospective/sfus/presentations10/A-6_Blok%20Lab%20Guide.pdf)
*   [A-7 (John) He SPAN Out of the Box](/retrospective/sfus/presentations10/A-7_John%20He%20SPAN%20Out%20of%20the%20Box.pdf)
*   [A-8 (Bae) CASE STUDY: Wireshark in the Large Enterprise](/retrospective/sfus/presentations10/A-8_Bae%20CASE%20STUDY%20-%20Wireshark%20in%20the%20Large%20Enterprise.ppt)
*   [A-9 (Donnelly) Operating a Flexible Network Montioring Infrastructure](/retrospective/sfus/presentations10/A-9_Donnelly%20Operating%20a%20Flexible%20Network%20Montioring%20Infrastructure.pptx)
*   [A-10 (Leutert) WLAN 802.11n MIMO Analysis](/retrospective/sfus/presentations10/A-10_Leutert%20WLAN%20802.11n%20MIMO%20Analysis.pdf)
*   [A-11 (Chappell) HANDS-ON LAB- Death of a Network: Identify the Hidden  
    Cause of Crappy Network Performance](/retrospective/sfus/presentations10/A-11_Chappell%20HANDS-ON%20LAB-%20Death%20of%20a%20Network%20-%20Identify%20the%20Hidden%20Cause%20of%20Crappy%20Netwokr%20Performance.pdf)

### **Developer Track Presentations**

*   [D-1 (Combs) Basic Dissector Development](/retrospective/sfus/presentations10/D-1_Combs%20Basic%20Dissector%20Development.pdf)
*   [D-2 (Chung) A Variety of Ways to Capture and Analyze Packets](/retrospective/sfus/presentations10/D-2_Chung%20A%20Variety%20of%20Ways%20to%20Capture%20and%20Analyze%20Packets.pdf)
*   [D-3 (Woodings) Visualizing RF](/retrospective/sfus/presentations10/D-3_Woodings%20Visualizing%20RF.ppt)
*   [D-4 (Leong) Power-Boosting Your Tools, by Adding Meta-Data Tags to Packets](/retrospective/sfus/presentations10/D-4_Leong%20Power-Boosting%20Your%20Tools%20by%20Adding%20Meta-Data%20Tags%20to%20Packets.zip)
*   [D-5 (Kersha)w Scripting Wireless Utilities](/retrospective/sfus/presentations10/D-5_Kershaw%20Scripting%20Wireless%20Utilities.ppt)
*   [D-6 (d'Otreppe) Wireless Security](/retrospective/sfus/presentations10/D-6_d'Otreppe%20Wireless%20Security.ppt)
*   [D-7 (Combs) Wireshark and Lua](/retrospective/sfus/presentations10/D-7_Combs%20Wireshark%20and%20Lua.zip)
*   [D-7 (Lyon) Scripting and Extending Nmap and Wireshark with Lua](/retrospective/sfus/presentations10/D-7_Lyon%20Scripting%20and%20Extending%20Nmap%20and%20Wireshark%20with%20Lua.pdf)
*   [D-8 (Combs) Advanced Dissector Developmen](/retrospective/sfus/presentations10/D-8_Combs%20Advanced%20Dissector%20Development.pdf)t
*   [D-9 (Sanders) Using Specialized Network Adapters to Improve Accuracy of  
    Network Analysis in Highly-Utlized Networks](/retrospective/sfus/presentations10/D-9_Sanders%20Using%20Specialized%20Network%20Adapters%20to%20Improve%20A%20ccuracy%20of%20Network%20Analysis%20in%20Highly-Utlized%20Networks.pdf)
*   [D-10 (Degioanni) To the Terabyte and Beyond](/retrospective/sfus/presentations10/D-10_LDegioanni%20To%20the%20Terabyte%20and%20Beyond.ppt)

* * *

#### A Word of Thanks

Another SharkFest has come and gone, and we thank each participant, sponsor, presenter, keynote speaker, coordinator, caterer, staff and general support person for making this a successful, richly educational event once again. All session presentations are available from this page, and recordings of many sessions can be found at [www.lovemytool.com](www.lovemytool.com).