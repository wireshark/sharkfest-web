SharkFest'18 Retrospective
==========================

June 25th - 28th, 2018  
Computer History Museum | Mountain View, California

### **Keynote Presentations**

#### **Twenty Years Of Code And Community**  
Gerald Combs
<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/YBOQXQT5dEA" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0;"></iframe></div>

#### **Wireshark: The Microscope of the 21st Century**  
Usman Muzaffar
<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/O-ZJ3gaBZkE" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0;"></iframe></div>

### **Blogs**

[SharkFest US 2018 Review](https://youtu.be/7Sno828oTkk), by Denise Fishburne

### **SharkBytes**
<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/videoseries?list=PLz_ZpPUgiXqPOFTJfNCfuzCwU67AUBgeN" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0;"></iframe></div>

### **Tuesday Classes**

*   01: [In the Packet Trenches - (Part 1)](#), by Hansang Bae

*   [Presentation Video](https://www.youtube.com/watch?v=u9n1KG7f3N8 "Presentation video on YouTube") (1:16:46)

*   02: [An Introduction to Wireshark: Rookie to Veteran in 2 sessions (Part 1)](https://prezi.com/view/ggGOl7RHv0Xuen5YwS82/), by Betty DuBois
*   03: [Writing a Wireshark Dissector: 3 ways to eat bytes](/retrospective/sfus/presentations18/03.7z), by Graham Bloice

*   [Presentation Video](https://youtu.be/biNdEqWoxrE "Presentation video on YouTube") (1:14:11)

*   04: [In the Packet Trenches - (Part 2)](#), by Hansang Bae

*   [Presentation Video](https://youtu.be/DhYwn8iXuf0 "Presentation video on YouTube") (1:14:11)

*   05: [An Introduction to Wireshark: Rookie to Veteran in 2 sessions (Part 2)](https://prezi.com/view/ggGOl7RHv0Xuen5YwS82/), by Betty DuBois
*   06: [Using more of the features of Wireshark to write better dissectors](/retrospective/sfus/presentations18/06.pdf), by Richard Sharpe
*   07: [Using Wireshark to solve real problems for real people: Step by-step case studies in packet analysis](/retrospective/sfus/presentations18/07.pptx), by Kary Rogers

*   [Presentation Video](https://youtu.be/8N6cwWrhGXM "Presentation video on YouTube") (1:12:53)

*   08: [Traffic analysis of cryptocurrency & blockchain networks](/retrospective/sfus/presentations18/31.pdf), by Brad Palm and Brian Greunke
*   09: [Developer Lightning Talks](#), by Wireshark Core Developers
*   10: [Hands-on analysis of multi-point captures](/retrospective/sfus/presentations18/10.pdf), by Christian Landström
*   11: [Augmenting packet capture with contextual meta-data: the what, why, and how](/retrospective/sfus/presentations18/11.pptx), by Stephen Donnelly

*   [Presentation Video](https://youtu.be/er_HZvLZuZA "Presentation video on YouTube") (51:43)

*   12: [Point and Shoot Packet! Point your packet effectively & Shoot the trouble with Wireshark](/retrospective/sfus/presentations18/12.zip), by Megumi Takeshita
*   13: [Practical Tracewrangling: exploring capture file manipulation/extraction scenarios](/retrospective/sfus/presentations18/13.pdf), by Jasper Bongertz

*   [Presentation Video](https://youtu.be/7tGfywGdrIU "Presentation video on YouTube") (1:31:02)

*   14: [BGP is not only a TCP session: Learning about the protocol that holds networks together](#), by Werner Fischer

*   [Presentation Video](https://youtu.be/GNP-sRzvbGY "Presentation video on YouTube") (1:00:04)

*   15: [How to get 100% of your data off the wire](#), by Greg Zemlin

### **Wednesday Classes**

*   16: TCP - Tips, Tricks, & Traces (Part 1), by Chris Greer

*   [Presentation Video](https://youtu.be/P00qomrZfw4 "Presentation video on YouTube") (1:17:23)

*   17: [extcap – Packet capture beyond libpcap/winpcap: bluetooth sniffing, android dumping & other fun stuff!](/retrospective/sfus/presentations18/17.pptx), by Roland Knall

*   [Presentation Video](https://youtu.be/GjseviQJDaU "Presentation video on YouTube") (45:35)

*   18: [Generating Wireshark Dissectors: A status report](/retrospective/sfus/presentations18/18.pdf), by Richard Sharpe

*   [Presentation Video](https://youtu.be/fOseb4BSra8 "Presentation video on YouTube") (56:11)

*   19: TCP - Tips, Tricks, & Traces (Part 2), by Chris Greer

*   [Presentation Video](https://youtu.be/RNYE7DelF3o "Presentation video on YouTube") (1:11:54)

*   20: [Wireshark in the “Real World”: Top ways to use Wireshark in the real world of an IT engineer](# "Presentation slides"), by Patrick Kinnison
*   21: [sFlow: Theory & practice of a sampling technology and its analysis with Wireshark](/retrospective/sfus/presentations18/21.pdf), by Simone Maindardi

*   [Presentation Video](https://youtu.be/SX_LBoGgZK4 "Presentation video on YouTube") (1:10:53)

*   22: [Writing a TCP analysis expert system](/retrospective/sfus/presentations18/22.pdf "Presentation slides"), by Jasper Bongertz

*   [Presentation Video](https://youtu.be/TbADZswg_h8 "Presentation video on YouTube") (1:13:37)

*   23: [Playing with "MATCHES": Using regular expressions for fun & profit](/retrospective/sfus/presentations18/23.zip), by Mike Hammond

*   [Presentation Video](https://youtu.be/tyEEQJ-ikjY) (1:21:08)

*   24: [Know Abnormal, Find Evil: A Wireshark Beginner’s Guide for the Security Professional](/retrospective/sfus/presentations18/24.pdf), by Maher Adib

*   [Presentation Video](https://youtu.be/bLUFZ_ysrYk "Presentation video on YouTube") (1:17:12)

*   25: [A deep dive into SIP: everything you need to know to debug & troubleshoot SIP packets](https://prezi.com/view/McyHI6DRM3V536k84t2o/), by Betty DuBois
*   26: Analyzing Windows malware traffic with Wireshark, by Bradley Duncan

*   [Presentation Video](https://youtu.be/hL04q6fXHV8 "Presentation video on YouTube") (1:37:01)

*   27: My TCP ain’t your TCP: Stack behavior back then & today, by Simon Lindermann

*   [Presentation Video](https://youtu.be/OvV17GQlw-I "Presentation video on YouTube") (1:13:49)

*   28: The Packet Doctors are In! Packet trace examinations, by the experts, by Hansang Bae, Jasper Bongertz, Christian Landström, Sake Blok and Kary Rogers
*   29: [Baselining with Wireshark to identify & stop unwanted communications](#), by Jon Ford

*   [Presentation Video](https://youtu.be/6vK-QDYxQB0 "Presentation video on YouTube") (59:51)

*   30: [BGP is not only a TCP session: Learning about the protocol that holds networks together](/retrospective/sfus/presentations18/30.pdf "Presentation slides"), by Werner Fischer

*   [Presentation Video](https://youtu.be/GNP-sRzvbGY "Presentation video on YouTube") (1:00:04)

### **Thursday Classes**

*   31: [Traffic analysis of cryptocurrency & blockchain networks](/retrospective/sfus/presentations18/31.pdf), by Brad Palm and Brian Greunke

*   [Presentation Video](https://youtu.be/MHEH4KWQtSQ "Presentation video on YouTube") (1:21:15)

*   32: [We’ll never do it right: A look at security, what we’re doing and how we’re trying to fix things](/retrospective/sfus/presentations18/32.pdf), by Mike Kershaw

*   [Presentation Video](https://youtu.be/xp-QwN79dBY "Presentation video on YouTube") (1:26:03)

*   33: [Wireshark CLI tools & scripting](/retrospective/sfus/presentations18/33.zip), by Sake Blok

*   [Presentation Video](https://youtu.be/IZ439VNvJqo "Presentation video on YouTube") (1:11:14)

*   34: [Patterns in TCP retransmissions: Using Wireshark to better understand the retransmission process](#), by Scott Reid

*   [Presentation Video](https://youtu.be/yFlgTWRNdhg "Presentation video on YouTube") (1:11:33)

*   35: Behind the Green Lock: Examining SSL encryption/decryption using Wireshark, by Ross Bagurdes

*   [Presentation Video](https://youtu.be/0X2BVwNX4ks "Presentation video on YouTube") (1:02:20)

*   36: [Wireshark and beyond! Complementing your Wireshark analysis with other open source & low-cost tools](#), by Mike Canney
*   37: [Packet monitoring in the days of IoT and Cloud](#), by Luca Deri

*   [Presentation Video](https://youtu.be/RGt7gWICWGs "Presentation video on YouTube") (1:17:09)

*   38: Baselining with Wireshark to identify & stop unwanted communications, by Jon Ford

*   [Presentation Video](https://youtu.be/6vK-QDYxQB0 "Presentation video on YouTube") (59:51)

*   39: [Introduction to practical network signature development for open source IDS (Part 1)](#), by Jason Williams and Jack Mott
*   40: [Mangling packets on the fly with divert sockets: how to hack a Cisco router ACL](/retrospective/sfus/presentations18/40.pptx), by Kary Rogers

*   [Presentation Video](https://youtu.be/3eBjqRb0FPw "Presentation video on YouTube") (1:04:33)

*   41: My TCP ain’t your TCP: Stack behavior back then and today, by Simon Lindermann

*   [Presentation Video](https://youtu.be/OvV17GQlw-I "Presentation video on YouTube") (1:13:49)

*   42: [Introduction to practical network signature development for open source IDS (Part 2)](#), by Jason Williams and Jack Mott
*   43: [OPEN FORUM: Aha! Moments in packet analysis](#), by Chris Greer
*   44: [Analyzing Windows malware traffic with Wireshark](#), by Bradley Duncan

*   [Presentation Video](https://youtu.be/hL04q6fXHV8 "Presentation video on YouTube") (1:37:01)

*   45: [Introduction to practical network signature development for open source IDS (Part 3)](#), by Jason Williams and Jack Mott

* * *

#### A Word of Thanks

SharkFest'18 US celebrated the 20th anniversary of the Wireshark project and proved a blazing success thanks to the generous, giving community in attendance. Particular thanks to Gerald Combs and his merry band of core developers for inspiring the many first-time participants, by opening with a keynote that illuminated the 20-year history of the project, to Laura Chappell for creating a truly crowd-pleasing Packet Palooza Pub Quiz, to Usman Muzaffar for his thoughtful keynote that explained why Wireshark can be considered the microscope of the 21st century, to instructors who selflessly donated time and wisdom to educate and mentor participants, to sponsors who so generously provided the resources to make the conference possible, to the Computer History Museum Events team for their expert guidance, to tireless caterers who served up varied and delicious daily fare, to Albert our AV Angel, to a staff and volunteer crew who once again went overboard in making the conference as smooth and pleasant an experience as possible for attendees, and to Aaron and Lainey for delighting us all with their golden intelligence!

#### Host Sponsors

[![](/img/sponsors/riverbed.png)](http://www.riverbed.com/) [![](/img/sponsors/wiresharku.png)](http://www.wiresharktraining.com/)

#### Angel Shark Sponsors

[![](/img/sponsors/cloudflare-big.png)](http://www.cloudflare.com/) [![](/img/sponsors/fmadio.jpg)](http://fmad.io/) [![](/img/sponsors/garland.png)](https://www.garlandtechnology.com/) [![](/img/sponsors/metamako.png)](http://www.metamako.com/) [![](/img/sponsors/napatech.png)](http://www.napatech.com/) [![](/img/sponsors/profitap.png)](http://www.profitap.com/)

#### Wireshark Packetpalooza Sponsor

[![](/img/sponsors/endace.png)](https://www.endace.com/)

#### Honorary Sponsor

[![](/img/sponsors/lovemtytool_Logo.png)](http://www.lovemytool.com/)