SharkFest'23 US Retrospective
=====================================

June 10-15 
San Diego, CA

### **Keynote Presentations**

#### **...So That's What we Did**  
Gerald Combs & Friends
<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/Hodn2vAq-QQ" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0;"></iframe></div>

#### **Network Protocol: Myths, Missteps, and Mysteries**  
Radia Perlman
<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/ek7SfLuv8PI" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0;"></iframe></div>

### [Presentation Video Playlist](https://www.youtube.com/playlist?list=PLz_ZpPUgiXqPLdY17eKQGQLe8NVgcJTt6)

### **Tuesday Classes**

*   01: [Capturing Packets in a Kubernetes Container System](/retrospective/sfus/presentations23/1.pdf), by Jeff Carrell
    <!-- *   [Presentation Video](https://youtu.be/4hMT0kcW39g "Presentation video on YouTube") -->
*   02: [More Mileage from Your Tools: Problem Isolation with TLS & TCP (Part 1)](/retrospective/sfus/presentations23/02.zip), by George Cragg
    <!-- *   [Presentation Video](https://youtu.be/L94vuo-5wSQ "Presentation video on YouTube") -->
*   03: [Stylin' with Wireshark Profilin'](/retrospective/sfus/presentations23/03.pdf), by Josh Clark
*   04: [More Mileage from Your Tools: Problem Isolation with TLS & TCP (Part 2)](/retrospective/sfus/presentations23/02.zip), by George Cragg
    <!-- *   [Presentation Video](https://youtu.be/a6rs1kPK5lc "Presentation video on YouTube") -->
*   05: [Packet Capture in Public Cloud](/retrospective/sfus/presentations23/05.pptx), by Dr Stephen Donnelly
    <!-- *   [Presentation Video](https://youtu.be/wG0tN9BDh1A "Presentation video on YouTube") -->
*   06: [TCP Case Study Packet Analysis exhibits from high visibility, high stakes critical problems](/retrospective/sfus/presentations23/06.pdf), by Bill Alderson
    <!-- *   [Presentation Video](https://youtu.be/25h6it4I254 "Presentation video on YouTube") -->
*   07: ["I wish Wireshark" - add the missing pieces with Lua](/retrospective/sfus/presentations23/07.pdf), by Chuck Craft
    <!-- *   [Presentation Video](https://youtu.be/rAgy3nb9Ucs "Presentation video on YouTube") -->
*   08: [Wireshark plus Advanced Analytics - Better Together (Part 1 of 2-part workshop)](/retrospective/sfus/presentations23/08.pdf), by John Pittle
    <!-- *   [Presentation Video](https://youtu.be/A5qiUuE-paw "Presentation video on YouTube") -->
*   09: [Capturing WiFi6E with Wireshark](/retrospective/sfus/presentations23/09.zip), by Megumi Takeshita
    <!-- *   [Presentation Video](https://youtu.be/akDHB6s4KYU "Presentation video on YouTube") -->
*   10: [Wireshark plus Advanced Analytics - Better Together (Part 2 of 2-part workshop)](/retrospective/sfus/presentations23/10.pdf), by John Pittle
    <!-- *   [Presentation Video](https://youtu.be/1eJHqyyjHqk "Presentation video on YouTube") -->
    <!-- *   [Presentation Video](https://youtu.be/BHNiQ-QLVVw "Presentation video on YouTube") -->

### **Wednesday Classes**
*   11: [Maintaining Dissector Quality](/retrospective/sfus/presentations23/11.pptx), by Martin Mathieson
    <!-- *   [Presentation Video](https://youtu.be/4lSLLo5btOQ "Presentation video on YouTube") -->
*   12: [Beyond Network Latency: Chasing Latency up the Stack](/retrospective/sfus/presentations23/12.zip), by Josh Clark
*   13: The Packet Doctors are in! Packet trace examinations with the experts 
*   14: [Dive into DNS over QUIC - DoQ](https://learn.zohopublic.com/external/manual/protocols-doq/article/pcaps-on-cloudshark?p=83c8d98ac893abee0a60c07da81397eb871377309a88dea1caac3adcbb947fd5), by Betty DuBois
*   15: [Your IPv6 is Being Attacked, How Do You Know?](/retrospective/sfus/presentations23/15.pdf), by Jeff Carrell
*   16: [Stepping up your packet analysis game, by leveraging the Wireshark CLI tools (part 1 of 2-part workshop)](/retrospective/sfus/presentations23/16-18.zip), by Sake Blok
    <!-- *   [Presentation Video](https://youtu.be/4hMT0kcW39g "Presentation video on YouTube") -->
*   17: Kubeshark: The API Traffic Analyzer for Kubernetes, by Alon Gimorsky
    <!-- *   [Presentation Video](https://youtu.be/Cq6yj9se9M4 "Presentation video on YouTube") -->
*   18: [Stepping up your packet analysis game, by leveraging the Wireshark CLI tools (part 2 of 2-part workshop)](/retrospective/sfus/presentations23/16-18.zip), by Sake Blok
*   19: Examining Efficiency Improvements of TLS 1.3 using Wireshark, by Ross Bagurdes
    <!-- *   [Presentation Video](https://youtu.be/q7RdLva_244 "Presentation video on YouTube") -->

### **Thursday Classes**
*   20: [How I Learned to Stop Worrying & Love the PCAP](#), by Kary Rogers
    <!-- *   [Presentation Video](https://youtu.be/nTQYA432h_k "Presentation video on YouTube") -->
*   21: [Applied Machine Learning for processing and reporting on large PCAP files](/retrospective/sfus/presentations23/21.pdf), by  Anand Ravi and Johnny Ghibril
*   22: Wireshark and AI, by Roland Knall
    <!-- *   [Presentation Video](https://youtu.be/dttGKC0qEZk "Presentation video on YouTube") -->
*   23: QUIC Protocol Enterprise Overview & Security Cautions, by Bill Alderson
    <!-- *   [Presentation Video](https://youtu.be/ZBh2nVSfdnQ "Presentation video on YouTube") -->
*   24: [Smart Move! - Tips and Tricks for Network Analysts](/retrospective/sfus/presentations23/24.pdf), by Jasper Bongertz
    <!-- *   [Presentation Video](https://youtu.be/KYxVYtZO5yU "Presentation video on YouTube") -->
*   25: SolarWinds Breach Report with Packet Analysis, by Bill Alderson
<!-- *   [Presentation Video](https://youtu.be/KYxVYtZO5yU "Presentation video on YouTube") -->

* * *

#### Angel Shark Sponsors

[![](/img/sponsors/b-yond.webp)](https://www.b-yond.com/) [![](/img/sponsors/endace.png)](https://endace.com/) [![](/img/sponsors/profitap.jpg)](https://profitap.com/)[![](/img/sponsors/sysdig.png)](http://www.sysdig.com/)

#### Silky Shark Welcome Gift Sponsor

[![](/img/sponsors/fmadio-dark.jpg)](https://www.fmad.io/) 

#### esPCAPe Challenge Sponsor
[![](/img/sponsors/riverbed.png)](https://www.riverbed.com/)