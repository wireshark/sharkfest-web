SharkFest'13 Retrospective
==========================

June 16th - 19th, 2013  
UC Berkeley, Clark Kerr Campus | Berkeley, California

### **Blogs**

[Your Network is Not a Black Box](http://blog.riverbed.com/2013/06/gerald-combs-your-network-is-not-a-black-box-.html), by Gerald Combs  
[12 Ways to Go Deep with Wireshark at SharkFest 2013](http://www.riverbed.com/blogs/12-ways-to-go-deep-with-wireshark-at-sharkfest-2013.html), by Dormain Drewitz  
[Wireshark at 15: How to Start a Project That Lasts](http://blog.riverbed.com/2013/02/wireshark-at-15-how-to-start-a-project-that-lasts.html), by Liz Padula  
[Wireshark Tips and Tricks Used, by Insiders and Veterans](http://blog.riverbed.com/2012/10/wireshark-tutorial-series.html), by Hansang Bae

### **Press Release**

[SharkFest 2013 Marks 15th Anniversary of Wireshark](http://www.riverbed.com/about/news-articles/press-releases/sharkfest-2013-marks-15th-anniversary-of-wireshark.html)

### **Packet Challenge**

The SharkFest 2013 [Packet Challenge answer key](http://www.wiresharktraining.com/sharkfest2013challenge.html) is online at Wireshark University.

CONGRATULATIONS to the winners of the Packet Challenge (and an AirPcap Nx and Cascade Pilot PE license): Clay Maddox, University of North Florida (100%, but missed the bonus Challenge #7) Gareth Sydie, G-Research (Missed 1 Challenge but nailed the bonus one)

### **Keynote Presentations**

**History of the Wireshark Project - Reminiscing on the 15-Year-Old Project**  
Gerald Combs, Ethereal/Wireshark Project Founder + Special Guests
<iframe width="560" height="315" style="margin: 10px 0;" src="https://www.youtube.com/embed/zNuND7JuZZE" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0;"></iframe></div>

**Ode to Gerald: "Gerald Combs, Man...Myth...Mystery"**  
A Wireshark University Production
<iframe width="560" height="315" style="margin: 10px 0;" src="https://www.youtube.com/embed/YhqrtGlmfhQ" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0;"></iframe></div>

**"The Nice Thing About Standards is That There are so Many of Them!” Musings of an Early Networker**  
Rich Seifert, M.S.E.E., M.B.A., J.D., President of Networks and Communications Consulting
<iframe width="560" height="315" style="margin: 10px 0;" src="https://www.youtube.com/embed/rzHGrCCY8ow" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0;"></iframe></div>

**To Engineer is Human…So is Being Lazy: A Conversation on Network Vulnerability**  
Charles Kaplan, Senior Technical Director, Office of the CTO, Riverbed
<iframe width="560" height="315" style="margin: 10px 0;" src="https://www.youtube.com/embed/St8RR87F1MU" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0;"></iframe></div>

### **Network & Application Performance Track Presentations**

*   NAP-01: [Application Performance Analysis](/retrospective/sfus/presentations13/NAP-01_Application-Performance-Analysis_Mike-Canney.pdf "Presentation slides"), by Mike Canney
*   NAP-02: [It's Not the Network! The Value of Root Cause Analysis](/retrospective/sfus/presentations13/NAP-02_Its-Not-the-Network_The-Value-of-Root-Cause-Analysis_Graeme-Bailey.pdf "Presentation slides"), by Graeme Bailey
*   NAP-03: [Microsoft SMB Troubleshooting](/retrospective/sfus/presentations13/NAP-03_Microsoft-SMB-Troubleshooting_Rolf-Leutert.pdf "Presentation slides"), by Rolf Leutert

*   [Presentation Video](http://youtu.be/XbvFXSPig-w "Presentation video on YouTube") (1:16:59)

*   NAP-04: [Wireless Network Optimization](/retrospective/sfus/presentations13/NAP-04_Wireless-Network-Optimization_Trent-Cutler.pdf "Presentation slides"), by Trent Cutler
*   NAP-05: [Correlating Traces From Multiple Tiers](/retrospective/sfus/presentations13/NAP-05_Correlating-Traces-From-Multiple-Tiers_Paul-Offord.pdf "Presentation slides"), by Paul Offord

*   [Presentation Video](http://youtu.be/MYler-T9yGk "Presentation video on YouTube") (1:06:33)

*   NAP-06: Why Pilot?, by Janice Spampinato and Martin Lewald

*   [Presentation Video](http://youtu.be/YUqjPEUyzbk "Presentation video on YouTube") (1:04:04)

*   NAP-08: [Using Wireshark as an Application Engineer](/retrospective/sfus/presentations13/NAP-08_Using-Wireshark-as-an-Application-Engineer_Tim-Poth.pdf "Presentation slides"), by Tim Poth

*   [Presentation Trace Files](/retrospective/sfus/presentations13/NAP-08_Using-Wireshark-as-an-Application-Engineer_Poth_Trace-files.7z "Presentation trace files")

*   NAP-09: [Application Performance Analysis](/retrospective/sfus/presentations13/NAP-01_Application-Performance-Analysis_Mike-Canney.pdf "Presentation slides"), by Mike Canney
*   NAP-10: [Visibility for Wireshark Across Physical, Virtual and SDN](/retrospective/sfus/presentations13/NAP-10_Enabling-Visibility-for-Wireshark-Across-Physical,-Virtual-and-SDN_Patrick-Leong.pdf "Presentation slides"), by Patrick Leong

*   [Presentation Video](http://youtu.be/k7JtruMSPjk "Presentation video on YouTube") (1:10:07)

*   NAP-11: [Expanding Wireshark Beyond Ethernet and Network Interfaces](/retrospective/sfus/presentations13/NAP-11_Expanding-Wireshark-Beyond-Ethernet-and-Network-Interfaces_Kershaw-Ryan.pdf "Presentation slides")  
   , by Mike Kershaw and Mike Ryan

*   [Presentation Video](http://youtu.be/Nn84T506SwU "Presentation video on YouTube") (44:16)

*   NAP-12: [Packet Optimization and Visibility using Wireshark and pcaps](/retrospective/sfus/presentations13/NAP-12_Packet-Optimization-and-Visibility-using-Wireshark-and-pcaps_Gordon-Beith.pdf "Presentation slides"), by Gordon Beith
*   NAP-13: Wireshark Users Ask the Experts! Moderated, by Chris Bidwell

*   [Presentation Audio](http://youtu.be/G7Fg6cwNCaA "Presentation audio on YouTube") (58:10)

*   NAP-14: [Accessing Packet Traces from Multiple Locations](/retrospective/sfus/presentations13/NAP-14_Accessing-Packet-Traces-from-Multiple-Locations_Bill-Eastman.pdf "Presentation slides"), by Bill Eastman

*   [Presentation Video](http://youtu.be/5Iw_66UbD6A "Presentation video on YouTube") (52:53)

*   NAP-15: [Understanding Wireshark's Reassembly Features](/retrospective/sfus/presentations13/NAP-15_Understanding-Wireshark's-Reassembly-Features_Christian-Landstrom.pdf "Presentation slides"), by Christian Landström
*   NAP-16: [Limitations of a Laptop: When Does It Start Dropping Packets?](/retrospective/sfus/presentations13//PA-05_Capture-Limitations-of-a-Laptop--When-Does-It-Start-Dropping-Packets_Chris-Greer.pdf "Presentation slides"), by Chris Greer

*   [Presentation Video](http://youtu.be/_H7PjWqKV0Q "Presentation video on YouTube") (45:02)

*   NAP-17: [Network Virtualization: the SDN You REALLY Want](/retrospective/sfus/presentations13/NAP-17_Network-Virtualization--the-SDN-You-REALLY-Want_Steve-Riley.pdf "Presentation slides"), by Steve Riley

### **Packet Analysis Presentations**

*   PA-01: [Deep-Dive-Packet-Analysis](/retrospective/sfus/presentations13//PA-01_Deep-Dive-Packet-Analysis_Hansang-Bae.pdf "Presentation slides"), by Hansang Bae

*   [Presentation Video](http://youtu.be/w-hTNGZRaSQ "Presentation video on YouTube") (1:16:37)

*   PA-02: [Introduction to IPv6 Addressing](/retrospective/sfus/presentations13//PA-02_Introduction-to-IPv6-Addressing_Nalini-Elkins.pdf "Presentation slides"), by Nalini Elkins

*   [Presentation Video](http://youtu.be/cd4tu_kVQt0 "Presentation video on YouTube") (1:12:54)

*   PA-03: [Debugging Wireless with Wireshark Including Large Trace Files, AirPcap & Cascade Pilot](presentations/PA-03_Debugging-Wireless-with-Wireshark-Including-Large-Trace-Files-AirPcap-and-Cascade-Pilot_Megumi-Takeshita.pdf "Presentation slides"), by Megumi Takeshita
*   PA-04: [Inside the TCP Handshake](/retrospective/sfus/presentations13//PA-04_Inside-the-TCP-Handshake_Betty-DuBois.pdf "Presentation slides"), by Betty DuBois

*   [Presentation Video](http://youtu.be/HGcbhCVZ8MU "Presentation video on YouTube") (1:17:28)
*   [Presentation Trace Files](http://tinyurl.com/tcptraces "Presentation trace files")

*   PA-05: [Limitations of a Laptop: When Does It Start Dropping Packets?](/retrospective/sfus/presentations13//PA-05_Capture-Limitations-of-a-Laptop--When-Does-It-Start-Dropping-Packets_Chris-Greer.pdf "Presentation slides"), by Chris Greer

*   [Presentation Video](http://youtu.be/_H7PjWqKV0Q "Presentation video on YouTube") (45:02)

*   PA-07: [Troubleshooting From the Field](/retrospective/sfus/presentations13//PA-07_Troubleshooting-from-the-field_Herbert-Grabmayer.pdf "Presentation slides"), by Herbert Grabmayer
*   PA-08: [IPv6 Address Planning](/retrospective/sfus/presentations13//PA-08_IPv6-Address-Planning_Nalini-Elkins.pdf "Presentation slides"), by Nalini Elkins
*   PA-10: [Writing a Wireshark Dissector](/retrospective/sfus/presentations13//PA-10_Writing-a-Wireshark-Dissector_Graham-Bloice.zip "Presentation slides and trace files"), by Graham Bloice (Zip archive)
*   PA-11: [How to Use Wireshark to Analyze Video](/retrospective/sfus/presentations13//PA-11_How-to-Use-Wireshark-to-Analyze-Video_Betty-DuBois.pdf "Presentation slides"), by Betty DuBois

*   [Presentation Trace Files](http://tinyurl.com/tcptraces "Presentation trace files")

*   PA-12: [WLAN Troubleshooting with Wireshark and AirPcap](/retrospective/sfus/presentations13//PA-12_WLAN-Troubleshooting-with-Wireshark-and-AirPcap_Rolf-Leutert.pdf "Presentation slides"), by Rolf Leutert
*   PA-13: [Trace Analysis Using Wireshark](/retrospective/sfus/presentations13//PA-13_IPv6-Trace-Analysis-Using-Wireshark_Nalini-Elkins.pdf "Presentation slides"), by Nalini Elkins
*   PA-14: [Top 5 False Positives when Analyzing Networks](/retrospective/sfus/presentations13//PA-14_Top-5-False-Positives-when-Analyzing-Networks_Jasper-Bongertz.pdf "Presentation slides"), by Jasper Bongertz
*   PA-15: So You've Found the Suspect Traffic, But What's Causing It?, by Graeme Bailey

*   [Presentation Video](http://youtu.be/X9xGs7sQpPY "Presentation video on YouTube") (1:17:29)

*   PA-16: [Wireshark in the Large Enterprise](/retrospective/sfus/presentations13//PA-16_Wireshark-in-the-Large-Enterprise_Hansang-Bae.pdf "Presentation slides"), by Hansang Bae

*   [Presentation Video](http://youtu.be/0TTgVmzKUqo "Presentation video on YouTube") (1:19:04)

*   PA-17: [TCP Performance Problem Analysis Through Multiple Network Segments](/retrospective/sfus/presentations13//PA-17_TCP-Performance-Problem-Analysis-Through-Multiple-Network-Segments_Bongertz-and-Landstrom.pdf "Presentation slides"), by Jasper Bongoertz and Christian Landström

### **Security Presentations**

*   SEC-01: [Understanding Encryption Services using Wireshark](/retrospective/sfus/presentations13/SEC-01_Understanding-Encryption-Services-using-Wireshark_Larry-Greenblatt.pdf "Presentation slides"), by Larry Greenblatt

*   [Presentation Video](http://youtu.be/0i192lYNxJs "Presentation video on YouTube") (44:50)

*   SEC-02: [VoIP Fundamentals](/retrospective/sfus/presentations13/SEC-02_VoIP-Fundamentals_Phill-Shade.pdf "Presentation slides"), by Phill Shade
*   SEC-03: [IPv6 Security Assessment Tools & Infrastructure Mitigation](/retrospective/sfus/presentations13/SEC-03_IPv6-Security-Assessment-Tools-and-Infrastructure-Mitigation_Jeff-Carrell.pdf "Presentation slides"), by Jeff-Carrell
*   SEC-04: [Trace File Sanitization NG](/retrospective/sfus/presentations13/SEC-04_Trace-File-Sanitization-NG_Jasper-Bongertz.pdf "Presentation slides"), by Jasper Bongertz

*   [Presentation Video](http://youtu.be/WZLz8meMNgI "Presentation video on YouTube") (1:21:21)

*   SEC-05: [Using Wireshark to Gather Forensic Evidence on Malware Outbreaks in Enterprise Networks](/retrospective/sfus/presentations13/SEC-05_Using-Wireshark-to-Gather-Forensic-Evidence-on-Malware-Outbreaks-in-Enterprise-Networks_Christian-Landstrom.pdf "Presentation slides"), by Christian Landström
*   SEC-06: [I Can Hear You Tunneling…](/retrospective/sfus/presentations13/SEC-06_I-Can-Hear-You-Tunneling_Alex-Weber.pdf "Presentation slides"), by Alex Weber

*   [Presentation Video](https://youtu.be/hFnvIXn0-R0 "Presentation video") (51:23)

*   SEC-07: [Wireshark Network Forensics](/retrospective/sfus/presentations13/SEC-07_Wireshark-Network-Forensics_Laura-Chappell.zip "Presentation slides and trace files"), by Laura Chappell (Zip archive)

*   [Presentation Video](http://youtu.be/UXAHvwouk6Q "Presentation video") (1:16:39)

*   SEC-08: [Why is Cryptography So Hard to Get Right](/retrospective/sfus/presentations13/SEC-08_Why-is-Cryptography-So-Hard-to-Get-Right_Ron-Bowes.pdf "Presentation slides"), by Ron Bowes
*   SEC-09: [Attack Trends & Techniques](/retrospective/sfus/presentations13/SEC-09_Attack-Trends-and-Techniques_Steve-Riley.pdf "Presentation slides"), by Steve Riley

*   [Presentation Video](http://youtu.be/6RGk-Q-S6Zs "Presentation video") (1:09:15)

*   SEC-10: Wireshark Network Forensics, by Laura Chappell

*   [Presentation Video](http://youtu.be/UXAHvwouk6Q "Presentation video on YouTube") (1:16:38)

*   SEC-11: [IPv6 Security](/retrospective/sfus/presentations13/SEC-11_IPv6-Security_Nalini-Elkins.pdf "Presentation slides and trace files"), by Nalini Elkins
*   SEC-12: [Trace File Sanitization NG](/retrospective/sfus/presentations13/SEC-04_Trace-File-Sanitization-NG_Jasper-Bongertz.pdf "Presentation slides"), by Jasper Bongertz

*   [Presentation Video](http://youtu.be/WZLz8meMNgI "Presentation video on YouTube") (1:21:21)

*   SEC-13: [How 802.11ac Will Hide Problems From Wireshark](/retrospective/sfus/presentations13/SEC-13_How-802.11ac-Will-Hide-Problems-From-Wireshark_Joe-Bardwell.pdf "Presentation slides"), by Joe Bardwell
*   SEC-14: [Understanding Encryption Services using Wireshark](/retrospective/sfus/presentations13/SEC-01_Understanding-Encryption-Services-using-Wireshark_Larry-Greenblatt.pdf "Presentation slides"), by Larry Greenblatt

*   [Presentation Video](http://youtu.be/0i192lYNxJs "Presentation video on YouTube") (44:50)

*   SEC-15: [Why is Cryptography So Hard to Get Right](/retrospective/sfus/presentations13/SEC-15_Why-is-Cryptography-So-Hard-to-Get-Right_Ron-Bowes.pdf "Presentation slides"), by Ron Bowes
*   SEC-16: [I Can Hear You Tunneling…](/retrospective/sfus/presentations13/SEC-16_I-Can-Hear-You-Tunneling_Alex-Weber.pdf "Presentation slides"), by Alex Weber

*   [Presentation Video](/retrospective/sfus/presentations13/s/I-Can-Hear-You-Tunneling_Alex-Weber.mp4 "Presentation video") (51:23)

*   SEC-17: [Wireless Intrusion Detection](/retrospective/sfus/presentations13/SEC-17_Wireless-Intrusion-Detection_Mike-Kershaw.pdf "Presentation slides"), by Mike Kershaw

* * *

#### A Word of Thanks

SharkFest’13 has been a smashing event, if post-conference feedback from attendees is to be believed. For this we must thank all presenters for sharing their expertise and knowledge, thank the Wireshark core developers for making our virgin HACKATHON attempt so worthwhile, thank Laura Chappell for holding her inaugural WCNA Bootcamp in conjunction with our conference, thank the attendees for their generous participation, fellowship and camaraderie, thank the staff for working tirelessly to make this a truly wonderful experience for attendees, thank the UC Berkeley Conference Services group for helping ease the process of event planning, and thank Gerald Combs for giving us such a wonderful reason to get together and lift each other up every year.

#### **Platinum Level**

[![](/img/sponsors/cpacket-white.png)](http://www.cpacket.com/index.php)

#### **Gold Level**

[![](/img/sponsors/vss_logo.png)](http://www.vssmonitoring.com/) [![](/img/sponsors/gigamon-new.png)](http://www.gigamon.com/)

#### **Silver Level**

[![](/img/sponsors/dualcomm-white.png)](http://www.dual-comm.com/) [![](/img/sponsors/apposite_logo.png)](http://www.apposite-tech.com/) [![](/img/sponsors/fiberblaze_logo.png)](http://www.fiberblaze.com/) [![](/img/sponsors/lovemtytool_Logo.png)](http://www.lovemytool.com/) [![](/img/sponsors/napatech-white.png)](http://www.napatech.com/) [![](/img/sponsors/arista-bright.png)](http://www.aristanetworks.com/) [![](/img/sponsors/wiresharku-white.png)](http://www.wiresharktraining.com/) [![](/img/sponsors/ixia-white.png)](http://www.ixiacom.com/) [![](/img/sponsors/endace.png)](http://www.endace.com/) [![](/img/sponsors/inside-white.png)](http://www.insidethestack.com/) [![](/img/sponsors/metageek_logo.png)

#### **Hosting Sponsor**

](http://www.metageek.net/)[![](/img/sponsors/riverbed.png)](http://www.riverbed.com/)