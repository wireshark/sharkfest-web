SharkFest'12 Retrospective
==========================

June 24th - 27th, 2012  
UC Berkeley, Clark Kerr Campus | Berkeley, California

### **Blogs**

[Event Photos](http://blog.riverbed.com/2012/06/sharkfest-12-photos.html)

### **Press Release**

[Global Wireshark Developer and User Community Connected Through Successful Sharkfest '12 Educational Conference](http://www.riverbed.com/us/company/news/press_releases/2012/press_080612.php)  
[Riverbed Announces Sharkfest 2012](http://www.riverbed.com/us/company/news/press_releases/2012/press_030812.php)

### **Keynote Presentation**

[In The Cloud, Everything You Think You Know Is Wrong](/retrospective/sfus/presentations/Keynote-Steve_Riley1.pdf "Download presentation PDF") by Steve Riley

### **Beginner/Intermediate Track Presentations**

*   BI-1: [Using Wireshark Software as an Applications Engineer](/retrospective/sfus/presentations12/BI-1_BI-15_Using_Wireshark_Software_as_an_Applications_Engineer.pdf "Download presentation PDF"), by Tim Poth
*   BI-2: [Introduction to IPv6 Addressing - (Part 1 of 4)](/retrospective/sfus/presentations12/BI-2_IPv6_Addressing.pdf "Download presentation PDF"), by Nalini Elkins
*   BI-3: [It's Not the Network! The Value of Root Cause Analysis](/retrospective/sfus/presentations12/BI-3_Its_Not_the_Network-The_Value_of_Root_Cause_Analysis.pdf "Download presentation PDF"), by Graeme Bailey
*   BI-4: [Using Wireshark Software with a Cloudshark Plug-in](/retrospective/sfus/presentations12/BI-4_Using_Wireshark_Software_with_a_Cloudshark_Plug-in.pdf "Download presentation PDF"), by Joe McEachern and Zach Chadwick
*   BI-5: [ICMPv6 - (Part 2 of 4)](/retrospective/sfus/presentations12/BI-5_ICMPv6.pdf "Download presentation PDF"), by Nalini Elkins
*   BI-6: [Wireshark Software and 802.11ac Wireless Evolution](/retrospective/sfus/presentations12/BI-6_Wireshark_Software_and_802.11ac_Wireless_Evolution.pdf "Download presentation PDF"), by Joe Bardwell
*   BI-7: [VoIP Analysis Fundamentals](/retrospective/sfus/presentations12/BI-7_VoIP_Analysis_Fundamentals.pdf "Download presentation PDF"), by Phill Shade
*   BI-8a: [Wireshark Software Case Studies](/retrospective/sfus/presentations12/BI-8a_Wireshark_Software_Case_Studies-Phill_Shade.pdf "Download presentation PDF"), by Phill Shade
*   BI-8b: [Wireshark Software Case Studies](/retrospective/sfus/presentations12/BI-8b_Wireshark_Software_Case_Studies-Tim_Poth.pdf "Download presentation PDF"), by Tim Poth
*   BI-8c: [Wireshark Software Case Studies](/retrospective/sfus/presentations12/BI-8c_Wireshark_Software_Case_Studies-Megumi_Takeshita.pdf "Download presentation PDF"), by Megumi Takeshita
*   BI-8d: [Wireshark Software Case Studies](/retrospective/sfus/presentations12/BI-8d_Wireshark_Software_Case_Studies-Graeme_Bailey.pdf "Download presentation PDF"), by Graeme Bailey
*   BI-9: [Application Performance Analysis using Wireshark Software and Riverbed Technology Cascade® Software](/retrospective/sfus/presentations12/BI-9_Application_Performance_Analysis.pdf "Download presentation PDF"), by Mike Canney
*   BI-10: [Build a HOT Security Profile Using Cool New Features!](/retrospective/sfus/presentations12/BI-10_Build_a_HOT_Security_Profile_Using_Cool_New_Features.pdf "Download presentation PDF"), by Laura Chappell
*   BI-11: [Inside the TCP Handshake](/retrospective/sfus/presentations12/BI-11_Inside_the_TCP_Handshake.pdf "Download presentation PDF"), by Betty DuBois, BI-11 [Trace Files](/retrospective/sfus/presentations12/Inside_TCP_handshake_traces.zip "Download trace files")
*   BI-12 [Wireshark 1.8 – 16 New Wireshark Features to Drool Over!](/retrospective/sfus/presentations12/BI-12_Wireshark_1.8–16_New_Wireshark_Features_to_Drool_Over.pdf "Download presentation PDF"), by Laura Chappell
*   BI-13: [IPv6 Transition Techniques (Part 3 of 4)](/retrospective/sfus/presentations12/BI-13_IPv6_Transition_Techniques.pdf "Download presentation PDF"), by Nalini Elkins
*   BI-14 [IPv6 Address Planning (Part 4 of 4)](/retrospective/sfus/presentations12/BI-14_IPv6_Address_Planning.pdf "IPv6 Address Planning"), by Nalini Elkins
*   BI-15: [Using Wireshark Software as an Applications Engineer](/retrospective/sfus/presentations12/BI-1_BI-15_Using_Wireshark_Software_as_an_Applications_Engineer.pdf "Download presentation PDF"), by Tim Poth
*   BI-16: [Using Lua to Implement the Cloudshark Plug-in](/retrospective/sfus/presentations12/BI-16_Using_Lua_to_Implement_the_CloudShark_Plug-in.pdf "Download presentation PDF"), by Joe McEachern and Zach Chadwick
*   BI-17: [It's Not the Network! The Value of Root Cause Analysis](/retrospective/sfus/presentations12/BI-3_Its_Not_the_Network-The_Value_of_Root_Cause_Analysis.pdf "Download presentation PDF"), by Graeme Bailey
*   BI-18: [Understanding Encryption Services Using Wireshark Software](/retrospective/sfus/presentations12/BI-18_Understanding_Encryption_Services_Using_Wireshark.pdf "Download presentation PDF"), by Larry Greenblatt

### **Advanced Track Presentations**

*   A-1: [Deep Dive Packet Analysis](/retrospective/sfus/presentations12/A-1_Deep_Dive_Packet_Analysis.pdf "Download presentation PDF"), by Hansang Bae
*   A-2: [Understanding Encryption Services Using Wireshark Software](/retrospective/sfus/presentations12/A-2_Understanding_Encryption_Services_Using_Wireshark.pdf "Download presentation PDF"), by Larry Greenblatt
*   A-3: [Tuning Win7 Using Wireshark's TCP Stream Graph (case study)](/retrospective/sfus/presentations12/A-3_A-10_Tuning_Win7_Using_Wireshark_TCP_Stream_Graph_a_Case_Study.pdf "Download presentation PDF"), by Rolf Leutert
*   A-4: [Leveraging Openflow to create a Large Scale and Cost Effective, Packet Capture Network](/retrospective/sfus/presentations12/A-4_Leveraging_Openflow_to_create_a_Large_Scale_and_Cost_Effective_Packet_Capture_Network.pdf "Download presentation PDF"), by Rich Groves
*   A-5: [Analyzing WLAN Roaming Problems (case study)](/retrospective/sfus/presentations12/A-5_Analyzing_WLAN_Roaming_Problems.pdf "Download presentation PDF"), by Rolf Leutert
*   A-6: [Open WIPS-ng](/retrospective/sfus/presentations12/A-6_Open_WIPS-ng.pdf "Download presentation PDF"), by Thomas D'Otreppe
*   A-7: [Wireshark Software in the Large Enterprise](/retrospective/sfus/presentations12/A-7_Wireshark_Software_in_the_Large_Enterprise.pdf "Download presentation PDF"), by Hansang Bae
*   A-8: [SMB/CIFS Analysis](/retrospective/sfus/presentations12/A-8_SMB_CIFS_Analysis.pdf "Download presentation PDF"), by Betty DuBois, A-8 [Traces](/retrospective/sfus/presentations12/A-8_CifsTraces.zip "Download Trace Files") and [Profile](/retrospective/sfus/presentations12/A-8_SharkfestCIFSprofile.zip "Download Profile Files") Files
*   A-9: [Spectrum Analysis & Visual Packet Analysis](/retrospective/sfus/presentations12/A-9_Visualizing_802.11_WireShark_Data.pdf "Download presentation PDF"), by Ryan Woodings
*   A-10: [Tuning Win7 Using Wireshark's TCP Stream Graph (case study)](/retrospective/sfus/presentations12/A-3_A-10_Tuning_Win7_Using_Wireshark_TCP_Stream_Graph_a_Case_Study.pdf "Download presentation PDF"), by Rolf Leutert
*   A-11: [Pervasive Visibility in the Clouded Data Center - Distributed Real-Time Monitoring and Wireshark Software Drill Down on-Demand](/retrospective/sfus/presentations12/A-11_Pervasive_Visibility_in_the_Clouded_Data_Center.pdf "Download presentation PDF"), by Rony Kay
*   A-12: [Effects of Receiver-Side Window Scaling on Enterprise Networks](/retrospective/sfus/presentations12/A-12_Effects_of_Receiver-Side_Window_Scaling_on_Enterprise_Networks.pdf "Download presentation PDF"), by Christian Landstroem
*   A-13: [Secrets of Vulnerability Scanning: Nessus, Nmap and More](/retrospective/sfus/presentations12/A-13_A-17_Secrets_of_Vulnerability_Scanning_Nessus_Nmap_and_More.pdf "Download presentation PDF"), by Ron Bowes
*   A-14: [SSL Troubleshooting with Wireshark Software](/retrospective/sfus/presentations12/MB-1_SSL_Troubleshooting_with _Wireshark_Software.pdf "Download presentation PDF"), by Sake Blok, Additional [Lab Files](sharkfest.12/presentations13/MB-2_SSL_Troubleshooting_Hands-on_Lab_Files.tgz)
*   A-15: [SMB/CIFS Analysis](/retrospective/sfus/presentations12/A-8_SMB_CIFS_Analysis.pdf "Download presentation PDF"), by Betty DuBois, A-15 [Traces](/retrospective/sfus/presentations12/A-8_CifsTraces.zip "Download Trace Files") and [Profile](/retrospective/sfus/presentations12/A-8_SharkfestCIFSprofile.zip "Download Profile Files") Files
*   A-16: [Deep Dive Packet Analysis](/retrospective/sfus/presentations12/A-1_Deep_Dive_Packet_Analysis.pdf "Download presentation PDF"), by Hansang Bae
*   A-17: [Secrets of Vulnerability Scanning: Nessus, Nmap and More](/retrospective/sfus/presentations12/A-13_A-17_Secrets_of_Vulnerability_Scanning_Nessus_Nmap_and_More.pdf "Download presentation PDF"), by Ron Bowes
*   A-18: [Effects of Receiver-Side Window Scaling on Enterprise Networks](/retrospective/sfus/presentations12/A-18_Effects_of_Receiver-Side_Window_Scaling_on_Enterprise_Networks.pdf "Download presentation PDF"), by Christian Landstroem

### **Mixed Bag Track Presentations**

*   MB-1: [SSL Troubleshooting with Wireshark Software](/retrospective/sfus/presentations12/MB-1_SSL_Troubleshooting_with _Wireshark_Software.pdf "Download presentation PDF"), by Sake Blok
*   MB-2: [SSL Troubleshooting Hands-on Lab Files](/retrospective/sfus/presentations12/MB-2_SSL_Troubleshooting_Hands-on_Lab_Files.tgz "Download Lab Files"), by Sake Blok
*   MB-3: [Trace File Case Files](/retrospective/sfus/presentations12/MB-3_Trace_File_Case_Files.pdf "Download presentation PDF"), by Jasper Bongertz
*   MB-4: [More Trace Files Case Files - Hands-on Lab](/retrospective/sfus/presentations12/MB-4_More_Trace_Files_Case_Files_Hands-on_Lab.pdf "Download presentation PDF"), by Jasper Bongertz
*   MB-5: [Troubleshooting from the Field](/retrospective/sfus/presentations12/MB-5_Troubleshooting_from_the_Field.pdf "Download presentation PDF"), by Herbert Grabmayer
*   MB-6: [Introduction to WiFi Security and Aircrack-ng](/retrospective/sfus/presentations12/MB-6_Introduction_to_WiFi_Security_and_Aircrack-ng.pdf "Download presentation PDF"), by Thomas D'Otreppe, MB-6 [Trace Files](/retrospective/sfus/presentations12/MB-6_Network_Interaction.pcap.zip "Download the presentation trace files")
*   MB-7: [Network Forensics Analysis: A Hands-On Look at the New Paradigm in Network Security](/retrospective/sfus/presentations12/MB-7_Network_Forensics_Analysis-A_Hands-on_Look.pdf "Download presentation PDF"), by Phill Shade
*   MB-8: [Powershell: The New Command Shell for Windows in Combination with T-Shark](/retrospective/sfus/presentations12/MB-8_Powershell-The_New_Command_Shell_for_Windows_in_Combination_with_T-Shark.pdf "Download presentation PDF"), by Graham Bloice, MB-8 [Presentation Capture Files](/retrospective/sfus/presentations12/MB-8-Captures.zip "Download MB-8 sample capture files")
*   MB-11: [Trace File Case Files](/retrospective/sfus/presentations12/MB-11_Trace_File_Case_Files.pdf "Download presentation PDF"), by Jasper Bongertz
*   MB-12: [More Trace File Case Files](/retrospective/sfus/presentations12/MB-12_Trace_File_Case_Files-Hands_On_Lab.pdf "Download presentation PDF"), by Jasper Bongertz
*   MB-13: [Building Your Own Wireless Packet Capture Platform](https://kismetwireless.net/sharkfest/ "Presentations, videos and sample files at the Kismet Wireless website"), by Mike Kershaw
*   MB-14: [What's Old is New Again: Evolving Network Security Threats](/retrospective/sfus/presentations12/MB-14_Whats_Old_is_New_Again–Evolving_Network_Security_Threats.pdf "IPv6 Address Planning"), by Phill Shade

* * *

#### A Word of Thanks

Another Sharkfest has come and gone, and we thank each participant, sponsor, presenter, keynote speaker, coordinator, caterer, staff and general support person for making this a successful, richly educational event once again. All session presentations are available from this page, and recordings of many sessions can be found at www.lovemytool.com.

#### **Platinum Level**

[![](/img/sponsors/cpacket.png)](http://www.cpacket.com/index.php) [![](/img/sponsors/metageek_logo.png)

#### **Silver Level**

](http://www.metageek.net/)[![](/img/sponsors/gigamon-new.png)](http://www.gigamon.com/) [![](/img/sponsors/cloudshark_logo.png)](http://appliance.cloudshark.org/) [![](/img/sponsors/lovemtytool_Logo.png)](http://www.lovemytool.com/) [![](/img/sponsors/napatech.png)](http://www.napatech.com/) [![](/img/sponsors/inside.png)](http://www.insidethestack.com/) [![](/img/sponsors/wiresharku.png)](http://www.wiresharktraining.com/) [![](/img/sponsors/bigswitch.png)](http://www.bigswitch.com/)

#### **Hosting Sponsor**

[![](/img/sponsors/riverbed.png)](http://www.riverbed.com/)