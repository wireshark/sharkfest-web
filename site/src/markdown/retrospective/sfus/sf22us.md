SharkFest'22 US Retrospective
=====================================

July 9-14 
Kansas City, MO

### **Keynote Presentations**

#### **Latest Wireshark Developments & Road Map**  
Gerald Combs
<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/3exklGpT_mI" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0;"></iframe></div>

#### **The Zed Project: Introducing Logray | The Future of Packet Analysis**  
Gerald Combs and Loris Degioanni
<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/7bfUSXJPHPs" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0;"></iframe></div>

### **Tuesday Classes**

*   01: Network Troubleshooting from Scratch, by Jasper Bongertz
 *   [Presentation Video](https://youtu.be/4hMT0kcW39g "Presentation video on YouTube")
*   02: Wireshark and WiFi - Multicast Case Study, by George Cragg
 *   [Presentation Video](https://youtu.be/L94vuo-5wSQ "Presentation video on YouTube")
*   03: [Dissecting WPA3](/retrospective/sfus/presentations22/03DissectingWPA3.pdf), by Megumi Takeshita
*   04: Build Your Own: Remotely accessible packet-capture drop box for troubleshooting networks with <$100, by Anthony Efantis
    *   [Presentation Video](https://youtu.be/a6rs1kPK5lc "Presentation video on YouTube")
*   05: [Duct tape and baling wire: Extending Wireshark with Lua"](/retrospective/sfus/presentations22/05.pdf), by Chuck Craft
    *   [Presentation Video](https://youtu.be/wG0tN9BDh1A "Presentation video on YouTube")
*   06: LOG4SHELL: Getting to know your adversaries (/retrospective/sfus/presentations22/09.pdf), by Sake Blok
    *   [Presentation Video](https://youtu.be/25h6it4I254 "Presentation video on YouTube")
*   07: [Wireshark at Enterprise Scale](/retrospective/sfus/presentations22/07.pptx), by Dr. Stephen Donnelly
    *   [Presentation Video](https://youtu.be/rAgy3nb9Ucs "Presentation video on YouTube")
*   08: Wireshark with LTE and 5G Packet Core, by Mark Stout
    *   [Presentation Video](https://youtu.be/A5qiUuE-paw "Presentation video on YouTube")
*   09: Abusing the Network – An Overview of Malicious Network Activity and How to Detect It, by Phill Shade
    *   [Presentation Video](https://youtu.be/akDHB6s4KYU "Presentation video on YouTube")
*   10: Understanding TCP Throughput, by Kary Rogers
    *   [Presentation Video](https://youtu.be/1eJHqyyjHqk "Presentation video on YouTube")
*   11: Using Wireshark to learn IPv6, by Jeff Carrell
    *   [Presentation Video](https://youtu.be/4lSLLo5btOQ "Presentation video on YouTube")
*   12: DEVELOPER DEN DROP IN
*   13: [Troubleshoot like a Doctor](/retrospective/sfus/presentations22/13.pdf), by Josh Clark
*   14: [Hands on Deep Dive](/retrospective/sfus/presentations22/14-30.zip), by Hansang Bae
    *   [Presentation Video](https://youtu.be/BHNiQ-QLVVw "Presentation video on YouTube")

### **Wednesday Classes**

*   15: [Contribute to Wireshark: the low hanging fruits](/retrospective/sfus/presentations22/15.pdf), by Uli Heilmeier
*   16: Analyzing Capture files in Python with PyShark, by Dor Green
    *   [Presentation Video](https://youtu.be/4hMT0kcW39g "Presentation video on YouTube")
*   17: Visualizing and Decrypting TLS 1.3, by Ross Bagurdes
    *   [Presentation Video](https://youtu.be/Cq6yj9se9M4 "Presentation video on YouTube")
*   18: The Packet Doctors are in! Packet trace examinations with the experts
*   19: TCP Conversation Completeness – What it is, how to use it., by Chris Greer
    *   [Presentation Video](https://youtu.be/q7RdLva_244 "Presentation video on YouTube")
*   20: [TCP SACK Overview and Impact on Performance](/retrospective/sfus/presentations22/20.zip), by John Pittle
    *   [Presentation Video](https://youtu.be/nTQYA432h_k "Presentation video on YouTube")
*   21: Build Your Own Wireshark Learning Lab – Part 1/2/3, by Jeff Carrell
*   22: The Life of a Packet, The art of the trace file synchronization, by Mike Canney
    *   [Presentation Video](https://youtu.be/dttGKC0qEZk "Presentation video on YouTube")
*   23: WFH Update – Analysis of VPN Network Performance, by Chris Hull
    *   [Presentation Video](https://youtu.be/ZBh2nVSfdnQ "Presentation video on YouTube")
*   24: 10 Tools I use that Compliment Wireshark, by Anthony Efantis
    *   [Presentation Video](https://youtu.be/KYxVYtZO5yU "Presentation video on YouTube")
*   25: Advanced dissector features, by Roland Knall
    *   [Presentation Video](https://youtu.be/QM0ZpWi-iKk "Presentation video on YouTube")

### **Thursday Classes**
*   26: Packet Capture 101, by Jasper Bongertz
    *   [Presentation Video](https://youtu.be/rWHWOat5_Xg "Presentation video on YouTube")
*   27: When TCP reassembly gets complicated, by Tom Peterson
    *   [Presentation Video](https://youtu.be/PmxiAmaM_3w "Presentation video on YouTube")
*   28: [When a packet is not a packet](/retrospective/sfus/presentations22/28.pdf), by Mike Kershaw
    *   [Presentation Video](https://youtu.be/aPtkBucGY0o "Presentation video on YouTube")Intro to QUIC - The TCP Killer?
*   29: Intro to QUIC - The TCP Killer?, by Chris Greer
    *   [Presentation Video](https://youtu.be/LuaDEB3AdT4 "Presentation video on YouTube")
*   30: Hands on Deep Dive, by Hansang Bae
*   31: Developer Den Drop In (In-person/Zoom/Discord)

* * *

#### Host Sponsor

[![](/img/sponsors/sysdig.png)](http://www.sysdig.com/)

#### Angel Shark Sponsors

[![](/img/sponsors/allegro.png)](https://www.allegropackets.com/) [![](/img/sponsors/endace.png)](https://endace.com/) [![](/img/sponsors/profitap.jpg)](https://profitap.com/)[![](/img/sponsors/progress-flowmon.png)](https://www.progress.com/)

#### Bull Shark esPCAPe Challenge Sponsor

[![](/img/sponsors/qa-cafe.png)](https://www.qacafe.com/) 

#### Welcome Dinner Sponsor
[![](/img/sponsors/riverbed.png)](https://www.riverbed.com/) 

#### Honorary Sponsor
[![](/img/sponsors/networkdatapedia.png)](https://www.networkdatapedia.com/) 