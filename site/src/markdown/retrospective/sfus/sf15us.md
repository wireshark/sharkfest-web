SharkFest'15 Retrospective
==========================

June 22nd - 25th, 2015  
Computer History Museum | Mountain View, California

### **[Packet Challenge](/retrospective/sfus/presentations15/packetchallenge.pdf)**

Click [HERE](/retrospective/sfus/presentations15/packetchallenge.zip) to download the Packet Challenge questions/answers and the pcap files!

### **Keynote Presentations**

#### **High Impact: How Computer Geeks, Geniuses and Renegades are Changing the World**  
John C Hollar
<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/YJCsgh6OIaY" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0;"></iframe></div>

### **Blogs**

#### [SharkFest'15 Recap](https://blog.packet-foo.com/2015/07/sharkfest-2015-recap/), by Jasper Bongertz  
[SharkFest'15 Recap](http://www.unadulteratednerdery.com/2015/06/26/sharkfest-2015/), by John Schreiner

### **SharkBytes**
<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/Q5gDOozxnY0" frameborder="0" allowfullscreen style="position: absolute; top: 0; left: 0;"></iframe></div>

### **Tuesday Classes**

*   01: [Troubleshooting Fundamentals: Time-Tested Technique to Finding Root Cause - How I Approach Every Packet-Level Troubleshooting Exercise](/retrospective/sfus/presentations15/01.pptx), by Hansang Bae
*   02: [Introduction to Wireshark: Making Sense of the Matrix](/retrospective/sfus/presentations15/02.pptx), by Chris Greer

*   [Presentation Video](https://youtu.be/635snt797bQ "Presentation video on YouTube") (1:13:00)

*   03: [Writing a Wireshark Dissector Using WSGD, Lua and C](/retrospective/sfus/presentations15/03.pptx), by Graham Bloice

*   [Presentation Materials](/retrospective/sfus/presentations15/grahambloice.zip)

*   [Presentation Video](https://youtu.be/bwqv_OzCZC8 "Presentation video on YouTube") (1:14:39)

*   04: [Tracewrangling: Preparing Food for the Shark](/retrospective/sfus/presentations15/04.pdf), by Jasper Bongertz

*   [Presentation Video](https://youtu.be/_-iMd-UAhtY "Presentation video on YouTube") (1:11:00)

*   05: [Visualizations & Correlations in Troubleshooting](/retrospective/sfus/presentations15/05.pdf), by Kevin Burns
*   06: [Integrating L2/L3 Diagnostics: Deducing Network Configuration](/retrospective/sfus/presentations15/06.pdf), by Nalini Elkins
*   07: [TCP Analysis: Tips & Case Studies](/retrospective/sfus/presentations15/07.pptx), by Chris Greer

*   [Presentation Video](https://youtu.be/MIPS0RPn-aw "Presentation video on YouTube") (1:05:00)

*   08: [IPv6 in Wireshark](/retrospective/sfus/presentations15/08.pdf), by Jeff Carrell
*   09: [The Wireless Side of Wireshark](/retrospective/sfus/presentations15/09.pptx), by Thomas d'Otreppe de Bouvette
*   10: [Analyzing Huge Data for Suspicious Traffic](/retrospective/sfus/presentations15/10.pdf), by Christian Landström
*   11: [Changing Wireshark with Lua: Writing a Lua Plug-in to Create a Custom Decoder](/retrospective/sfus/presentations15/11.pptx), by Hadriel Kaplan

*   [Presentation Video](https://youtu.be/HTtVHxIh6ww "Presentation video on YouTube") (1:19:03)

*   12: [Matching Packets in Multiple Network Traces](/retrospective/sfus/presentations15/12.pdf), by Paul Offord

*   [Presentation Video](https://youtu.be/xlJXr3dpnWs "Presentation video on YouTube") (1:09:41)

### **Wednesday Classes**

*   [13: Advanced TCP Stuff: We're not in RFC 793 Anymore!](/retrospective/sfus/presentations15/13.pdf), by Jasper Bongertz

*   [Presentation Video](https://youtu.be/L5cbn2iF91s "Presentation video on YouTube") (1:06:13)

*   14: Simple Wireless Packet Capture on Android, by Mike Kershaw
*   15: [How To Get the Packets: Packet Capture Techniques for the Enterprise, by Paul Offord](/retrospective/sfus/presentations15/15.pdf)

*   [Presentation Video](https://youtu.be/pb1yb1eUlgY "Presentation video on YouTube") (1:09:00)

*   16: Network Troubleshooting Using ntop \[[PDF](/retrospective/sfus/presentations15/16.pdf) || [PPT](/retrospective/sfus/presentations15/16.pptx)\], by Luca Deri

*   [Presentation Video](https://youtu.be/RWg1mNYRkmc "Presentation video on YouTube") (1:09:22)

*   17: Troubleshooting Routing Protocols with Wireshark, by John Schreiner

*   [Presentation Video](https://youtu.be/Mx7K1wPOm2Q "Presentation video on YouTube") (1:13:31)

*   18: [De-duping Packet Capture Files at Layer 3: Using Wiretap to Create a Custom De-duping App, by Rober Bullen](/retrospective/sfus/presentations15/18.pptx)
*   19: [SSL Does Not Mean SOL](/retrospective/sfus/presentations15/19.pdf "Presentation slides"), by Scott Haugdahl
*   20: [Understanding Wireshark's Reassembly Features](/retrospective/sfus/presentations15/20.pdf "Presentation slides"), by Christian Landström
*   21: [TCP Analysis: Tips & Case Studies](/retrospective/sfus/presentations15/07.pptx), by Chris Greer

*   [Presentation Video](https://youtu.be/MIPS0RPn-aw "Presentation video on YouTube") (1:05:00)

*   22: [Network Forensics 101: What You Don't See Will Hurt You!](/retrospective/sfus/presentations15/22.pdf "Presentation slides"), by Phill Shade
*   23: [Discover WLANs with Wireshark, AirPcap & WiSpy: Troubleshooting WLANs using 802.11 Management & Control Frames](/retrospective/sfus/presentations15/23.pdf), by Rolf Leutert
*   24: Capturing Mobile Device Traffic with Wireshark, by Tom Updegrove

### **Thursday Classes**

*   25: [Troubleshooting the TCP Handshake](/retrospective/sfus/presentations15/25.pdf), by Betty DuBois

*   [Presentation Video](https://youtu.be/HGcbhCVZ8MU "Presentation video on YouTube") (1:17:27)

*   26: [Root Cause in Complex Networks: Tips & Lessons from Fast-Paced & Enterprise Financials by](/retrospective/sfus/presentations15/26.pdf) Chris Bidwell

*   [Presentation Video](https://youtu.be/CaIAb462a3w "Presentation video on YouTube") (1:16:36)

*   27: Network Troubleshooting Using ntop \[[PDF](/retrospective/sfus/presentations15/16.pdf) || [PPT](/retrospective/sfus/presentations15/16.pptx)\], by Luca Deri
*   28: [T-Shark for the Win!](/retrospective/sfus/presentations15/28.pdf), by Christian Landström
*   29: [Wireless Troubleshooting Tips using AirPcaps: DFS & Module Debugging](/retrospective/sfus/presentations15/29.pdf), by Megumi Takeshita
*   30: [How Did That Happen? Practical Techniques for Analyzing Suspicious Traffic](/retrospective/sfus/presentations15/30.pdf "Presentation slides"), by Phill Shade
*   31: [Zen & The Art of Packet Capturing: Packet Capture Techniques in Different Scenarios](/retrospective/sfus/presentations15/31.pdf "Presentation slides"), by Sake Blok
*   32: [Why is FTP So Slow? Which End is Holding it Up?](/retrospective/sfus/presentations15/32.pdf), by Nalini Elkins
*   33: [Using the TRANSUM Wireshark Plugin to Measure Application Performance](/retrospective/sfus/presentations15/33.pdf), by Paul Offord

*   [Presentation Video Part 1](https://www.youtube.com/watch?v=7PO-9HxHGdo&t=7m48s "Presentation video on YouTube") (45:25)

*   34: [Wireshark vs. the Cloud: Capturing Packets in Virtual Environments](/retrospective/sfus/presentations15/34.pdf), by Jasper Bongertz
*   35: [The Anatomy of a Vulnerability](/retrospective/sfus/presentations15/35.pdf), by Ron Bowes
*   36: [Wireshark and the Art of Packet Analysis](/retrospective/sfus/presentations15/36.pptx), by Hansang Bae

* * *

#### A Word of Thanks

SharkFest’15, in our new home at the Computer History Museum in Mountain View, proved to be a rousing success, with over 300 IT Professionals from 25 countries in attendance. A good time was had, by all, thanks to the dedication of our very capable SharkFest coordinating crew, sponsors, volunteers, instructors, and Wireshark core developers. Thanks for giving us a reason to come together again, Gerald!

#### **Reef Sponsors**

[![](/img/sponsors/riverbed.png)](http://www.riverbed.com/) [![](/img/sponsors/cpacket.png)](http://www.cpacket.com/index.php) [![](/img/sponsors/lovemtytool_Logo.png)](http://www.lovemytool.com/) [![](/img/sponsors/wiresharku.png)](http://www.wiresharktraining.com/)

#### **Angel Shark Sponsors**

[![](/img/sponsors/accolade.png)](http://accoladetechnology.com/) [![](/img/sponsors/bigswitch.png)](http://www.bigswitch.com/) [![](/img/sponsors/datacom.png)](http://datacomsystems.com/) [![](/img/sponsors/garland.png)](http://www.garlandtechnology.com/) [![](/img/sponsors/gigamon-new.png)](http://www.gigamon.com/) [![](/img/sponsors/inside.png)](http://www.insidethestack.com/) [![](/img/sponsors/ixia.png)](http://www.ixiacom.com/) [![](/img/sponsors/napatech.png)](http://www.napatech.com/) [![](/img/sponsors/pluribus-white.png)](http://www.pluribusnetworks.com/) [![](/img/sponsors/teledyne-white.png)](http://www.teledynelecroy.com)