import { stripeRedirects } from "../utils/links";
import { stripePromise } from "../utils/stripe";

export type PaymentModes = "subscription" | "payment";

const useStripe = () => {
  // Not used atm because switching to payment links which will automatically 
  // switch currency based on user's location
  const handleDonateClick = async (
    priceId: string,
    paymentMode: PaymentModes
  ) => {
    const stripe = await stripePromise;
    const { error } = await stripe.redirectToCheckout({
      lineItems: [
        {
          price: priceId,
          quantity: 1,
        },
      ],
      mode: paymentMode,
      successUrl: stripeRedirects.prodSuccess,
      cancelUrl: stripeRedirects.prodCancel,
    });
  };

  const handleLodgingPurchase = async (
    priceId: string,
  ) => {
    const stripe = await stripePromise;
    const { error } = await stripe.redirectToCheckout({
      lineItems: [
        {
          price: priceId,
          quantity: 1,
        },
      ],
      mode: "payment",
      successUrl: stripeRedirects.prodSuccessLodging,
      cancelUrl: stripeRedirects.prodCancelLodging,
    });
  };

  return { handleDonateClick, handleLodgingPurchase };
};

export default useStripe;
