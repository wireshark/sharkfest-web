import { useState, useEffect } from 'preact/hooks';

const useGetWindowInfo = () => {
    const [pathname, setPathname] = useState<string>("")
    const [theme, setTheme] = useState<string>("")

    useEffect(() => {
        setPathname(window.location.pathname)
        setTheme(window.localStorage.getItem("theme"))
    }, [])
 
    return { pathname, theme };
};

export default useGetWindowInfo;
