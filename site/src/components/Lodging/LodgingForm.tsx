import { useMemo, useState } from "preact/hooks";
import useStripe from "../../hooks/useStripe"
import { paymentLinks } from "../../utils/links";
import styles from "../DonatePage/DonatePage.module.scss"
import lodgingStyles from "./Lodging.module.scss"

const LodgingForm: preact.FunctionComponent = () => {
    const [activeButton, setActiveButton] = useState("single");

    const { handleLodgingPurchase } = useStripe();

    const renderPaymentLinks = useMemo(() => {
        return paymentLinks.lodgingSF23US.map((item, idx) => (
          <a
            onClick={() =>
              handleLodgingPurchase(
                activeButton === "single"
                  ? item.priceIdSingle
                  : item.priceIdDouble,
              )
            }
            key={idx}
          >
            <button className="primary-button">{item.text} {activeButton === "single" ? item.priceSingle : item.priceDouble}</button>
          </a>
        ));
      }, [activeButton]);

      return (
        <section id={`${styles.lodgingPage}`}>
          <div className={lodgingStyles.lodgingFormWrapper}>
            <p>Once you've completed your purchase, please email <a href="mailto:sheri@wireshark.org">sheri@wireshark.org</a> to advise of your check-in and check-out dates.</p>
            {renderPaymentLinks}
            {/* <div className={styles.monthlySection}>
              <h4>Choose Single or Double Occupancy</h4>
              <div className={styles.monthlyButtonContainer}>
                <button
                  onClick={() => setActiveButton("single")}
                  className={activeButton === "single" ? styles.active : null}
                >
                  {activeButton === "single" && <img src="/icons-sf/phosphor/check.svg" />}
                  Single
                </button>
                <button
                  onClick={() => setActiveButton("double")}
                  className={activeButton === "double" ? styles.active : null}
                >
                  {activeButton === "double" && <img src="/icons-sf/phosphor/check.svg" />}
                  Double
                </button>
              </div>
            </div> */}
          </div>
        </section>
      );
}

export default LodgingForm