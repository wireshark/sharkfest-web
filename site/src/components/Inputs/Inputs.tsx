import { ComponentChildren } from "preact";

interface TextInputProps {
  label: string;
  name: string;
  type?: string;
  placeholder?: string;
  required?: boolean;
  onFormInputChange: (e: any) => void;
}

export const TextInput: preact.FunctionComponent<TextInputProps> = ({
  label,
  name,
  type = "text",
  placeholder,
  required,
  onFormInputChange
}) => {
  return (
    <>
      <label htmlFor={name}>{label}{ required ? " *" : "" }</label>
      <input
        name={name}
        type={type}
        placeholder={placeholder ? placeholder : label}
        required={required}
        onChange={onFormInputChange}
      />
    </>
  );
};

export const TextAreaInput: preact.FunctionComponent<TextInputProps> = ({
  label,
  name,
  type,
  placeholder,
  required,
  onFormInputChange
}) => {
  return (
    <>
      <label htmlFor={name}>{label}{ required ? " *" : "" }</label>
      <textarea
        name={name}
        type={type}
        placeholder={placeholder ? placeholder : label}
        required={required}
        onChange={onFormInputChange}
      />
    </>
  );
};

interface SelectInputProps {
  label: string;
  name: string;
  children: ComponentChildren;
  required?: boolean;
  onFormInputChange: (e: any) => void;
}

export const SelectInput: preact.FunctionComponent<SelectInputProps> = ({
  label,
  name,
  children,
  onFormInputChange,
  required,
}) => {
  return (
    <>
      <label htmlFor={name}>{label} { required ? " *" : "" }</label>
      <select onChange={onFormInputChange} name={name} required={required}>
        {children}
      </select>
    </>
  );
};
