import { useState } from "preact/hooks";
import styles from "./Accordion.module.scss";

type AccordionVariants = "bordered" | "simple";

interface AccordionProps {
  title: string;
  content?: { title: string; link: string; icon?: string }[];
  headerContent?: { name: string; subLinks: { name: string; url: string }[] };
  variant: AccordionVariants;
  active?: boolean;
}

const Accordion: preact.FunctionComponent<AccordionProps> = ({
  title,
  content,
  headerContent,
  variant,
  active,
}) => {
  if (typeof (active) === 'undefined') {
    const active = false;
  }
  const [isActive, setIsActive] = useState(active);

  return (
    <div
      className={
        variant === "bordered"
          ? styles.accordionItemBordered
          : styles.accordionItem
      }
    >
      <div
        className={variant === "bordered" ? styles.accordionTitle : ""}
        onClick={() => setIsActive(!isActive)}
      >
        <div>
          {title || headerContent.name}{" "}
          {/*
            Use right- and downward-pointing triangles as described at
            https://developer.apple.com/design/human-interface-guidelines/components/layout-and-organization/disclosure-controls/
          */}
          <span className={styles.caretIcon}>{isActive ? "▼" : "▶"}</span>
        </div>
      </div>
      {isActive && (
        <div className={styles.accordionContent}>
          <ul>
            {!!content
              ? content.map((item) => (
                <li>
                  <a href={item.link}>
                    <img src={item.icon} /> {item.title}
                  </a>
                </li>
              ))
              : headerContent.subLinks.map((headerItem) => {
                return headerItem.name !== "false" &&
                  (<li>
                    <a href={headerItem.url}>{headerItem.name}</a>
                  </li>)
              })}
          </ul>
        </div>
      )}
    </div>
  );
};

export default Accordion;
