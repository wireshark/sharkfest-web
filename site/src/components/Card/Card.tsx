import styles from "./Card.module.scss";

interface CardProps {
  title: string;
  subtitle: string;
  imgUrl?: string;
  link: string;
  videoUrl?: string;
}

const Card: preact.FunctionComponent<CardProps> = ({
  title,
  subtitle,
  imgUrl,
  link,
  videoUrl,
}) => {
  const renderVideoEmbed = () => {
    return (
      <div className={styles.videoWrapper}>
        <iframe
          src={videoUrl}
          allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen={true}
          width="100%"
          height="339px"
          frameBorder="0"
        ></iframe>
      </div>
    );
  };

  return (
    <div className={styles.cardWrapper}>
      <a href={link}>
        {(imgUrl || videoUrl) && (
          <div className={styles.imgSection}>
            {imgUrl && <img src={imgUrl} />}
            {videoUrl && renderVideoEmbed()}
          </div>
        )}
        <div className={styles.cardContent}>
          <h2>{title}</h2>
          <p>{subtitle}</p>
        </div>
      </a>
    </div>
  );
};

export default Card;
