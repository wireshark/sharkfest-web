import { TextAreaInput, TextInput, SelectInput } from "../Inputs/Inputs";
import styles from "./RegisterForm.module.scss";
import { AsiaRegOptions, AsiaRegOptionsVirtual, USRegOptions, EURegOptions } from "./types"

interface FormInputsProps {
  inPersonAttendee: boolean;
  onFormInputChange: (e: any) => void;
  windowUrl: string;
}

const FormInputs: preact.FunctionComponent<FormInputsProps> = ({
  inPersonAttendee,
  onFormInputChange,
  windowUrl
}) => {

  return (
    <>
      <div className={styles.formInputWrapper}>
        <TextInput
          onFormInputChange={onFormInputChange}
          label="Full Name"
          name="name"
          required={true}
        />
        <TextInput
          onFormInputChange={onFormInputChange}
          label="Business Title"
          name="business_title"
          required={true}
        />
      </div>
      <div className={styles.formInputWrapper}>
        <TextInput
          onFormInputChange={onFormInputChange}
          label="Email"
          name="email"
          type="email"
          required={true}
        />
        <TextInput
          onFormInputChange={onFormInputChange}
          label="Phone"
          name="phone"
          type="phone"
          required={true}
        />
      </div>
      <div className={styles.formInputWrapper}>
        <TextInput
          onFormInputChange={onFormInputChange}
          label="Company"
          name="company"
          required={true}
        />
        <TextInput
          onFormInputChange={onFormInputChange}
          label="Address"
          name="address"
          required={true}
        />
      </div>
      <div className={styles.formInputWrapper}>
        <TextInput
          onFormInputChange={onFormInputChange}
          label="Address2"
          name="address2"
        />
        <TextInput
          onFormInputChange={onFormInputChange}
          label="Country"
          name="country"
          required={true}
        />
      </div>
      <div className={styles.formInputWrapper}>
        <TextInput
          onFormInputChange={onFormInputChange}
          label="City"
          name="city"
          required={true}
        />
        <TextInput
          onFormInputChange={onFormInputChange}
          label="State/Province"
          name="state"
          required={false}
        />
      </div>
      <div className={styles.formInputWrapper}>
          <TextInput
            onFormInputChange={onFormInputChange}
            label="Postal Code"
            name="postal_code"
            required={true}
          />
        <TextAreaInput
          label="Please let us know how you heard about SharkFest"
          name="heard_about_sharkfest"
          required={true}
          onFormInputChange={onFormInputChange}
        />
      </div>
      <div className={styles.formInputWrapper}>
        <SelectInput
          label="Choose your Registration"
          name="type_of_registration"
          required={true}
          onFormInputChange={onFormInputChange}
        >
          <option value="" disabled selected hidden>
            Please choose...
          </option>
          {inPersonAttendee ? (
            <>
              {windowUrl.includes("sfus") && USRegOptions.map(regOption => <option>{regOption}</option>)}
              {windowUrl.includes("sfasia") && AsiaRegOptions.map(regOption => <option>{regOption}</option>)}
              {windowUrl.includes("sfeu") && EURegOptions.map(regOption => <option>{regOption}</option>)}
            </>
          ) : (
            <>
              {windowUrl.includes("sfasia") && AsiaRegOptionsVirtual.map(regOption => <option>{regOption}</option>)}
            </>
          )}
        </SelectInput>
        <SelectInput
          label="Is this your first SharkFest?"
          name="first_time"
          required={true}
          onFormInputChange={onFormInputChange}
        >
          <option value="" disabled selected hidden>
            Please choose...
          </option>
          <option>Yes</option>
          <option>No</option>
        </SelectInput>
      </div>
      {inPersonAttendee && (
        <>
          <div className={styles.formInputWrapper}>
            <SelectInput
              onFormInputChange={onFormInputChange}
              name="shirt"
              label="Please enter your shirt size"
              required={true}
            >
              <option value="" disabled selected hidden>
                Please choose...
              </option>
              <option>Small</option>
              <option>Medium</option>
              <option>Large</option>
              <option>XL</option>
              <option>XXL</option>
              <option>XXXL</option>
            </SelectInput>
            <TextInput
              onFormInputChange={onFormInputChange}
              name="dietary_restrictions"
              label="Please list any dietary restrictions"
            />
          </div>
        </>
      )}
    </>
  );
};

export default FormInputs;
