import "../LandingPage/LandingPage.css"

const HybridConference = ({ handleHybridButtonClick }) => {
    return (
        <div className="hybrid-wrapper">
            <h2>Will you be attending SharkFest US in person or virtually?</h2>
            <div className="landing-wrapper">
                <div className="landing-button-wrapper">
                    <button onClick={handleHybridButtonClick} className="sfvus-button">In Person</button>
                </div>
                <div className="landing-button-wrapper">
                    <button onClick={handleHybridButtonClick} className="sfvus-button">Virtual</button>
                </div>
            </div>
        </div>
    )
}

export default HybridConference