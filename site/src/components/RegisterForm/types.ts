export interface RegisterFormTypes {
    name: string;
    business_title: string;
    email: string;
    phone: string;
    company: string;
    shirt: string;
    address: string;
    address2?: string;
    country: string;
    city: string;
    state: string;
    postal_code: string;
    first_time: boolean;
    heard_about_sharkfest: string;
    type_of_registration: string;
    agree_to_terms?: boolean;
    consent_to_contact?: boolean;
    in_person?: boolean;
    dietary_restrictions?: string;
}

export const initialRegFormState = {
    name: "",
    business_title: "",
    email: "",
    phone: "",
    company: "",
    shirt: "",
    address: "",
    country: "",
    city: "",
    state: "",
    postal_code: "",
    first_time: false,
    heard_about_sharkfest: "",
    type_of_registration: "",
}

const formValuesMapToFormattedText = {
    name: "Name",
    business_title: "Business Title",
    email: "Email",
    phone: "Phone",
    company: "Company",
    shirt: "Shirt size",
    address: "Address",
    country: "Country",
    city: "City",
    state: "State",
    postal_code: "Postal Code",
    first_time: "The first time question",
    heard_about_sharkfest: "How you heard about SharkFest",
    type_of_registration: "The type of registration",
}

export const hasValidInputs = (formData, inPersonAttendee = true) => {
    const errorObj = {}

    Object.keys(formData).map(regFormDataType => {
        if (regFormDataType === "address2" || regFormDataType === "state" || regFormDataType === "dietary_restrictions") return
        if (!inPersonAttendee && regFormDataType === "shirt") return

        if (!formData[regFormDataType]) {
            errorObj[regFormDataType] = `${formValuesMapToFormattedText[regFormDataType]} cannot be blank.`
        }
    })

    return Object.keys(errorObj).length > 0 ? false : true
}

export const USRegOptions = ["SharkFest'23 US ($1095)", "Pre-conference Class 1 ($895)", "Pre-conference Class 2 ($595)", "SharkFest'23 US + Pre-conference Class 1 ($1990)", "SharkFest'23 US + Pre-conference Class 2 ($1690)", "SharkFest'23 US + both Pre-conference classes ($2585)", "Both Pre-conference classes ($1490)"]

export const AsiaRegOptions = ["SharkFest'23 ASIA ($795)", "Pre-conference Class 1 ($595)", "Pre-conference Class 2 ($595)", "SharkFest'23 ASIA + Pre-conference Class 1 ($1390)", "SharkFest'23 ASIA + Pre-conference Class 2 ($1390)"]

export const AsiaRegOptionsVirtual = ["SharkFest'23 ASIA Virtual ($445)", "Pre-conference Class 1 Virtual ($545)", "Pre-conference Class 2 Virtual ($545)", "SharkFest'23 ASIA Virtual + Pre-conference Class 1 Virtual ($990)", "SharkFest'23 ASIA Virtual + Pre-conference Class 2 Virtual ($990)"];

export const EURegOptions = ["SharkFest'23 EUROPE (€1095)", "Pre-conference Class 1 (€895)", "Pre-conference Class 2 (€695)", "Pre-conference Class 3 (€695)", "SharkFest'23 EUROPE + Pre-conference Class 1 (€1990)", "SharkFest'23 EUROPE + Pre-conference Class 2 (€1790)", "SharkFest'23 EUROPE + Pre-conference Class 3 (€1790)", "SharkFest'23 EUROPE + Pre-conference Class 2 + Pre-conference Class 3 (€2485)", "Pre-conference Class 2 + Pre-conference Class 3 (€1390)"];