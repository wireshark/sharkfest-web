import { useEffect, useState } from "preact/hooks";
import { wretchHelper } from "../../utils/wretchHelper";
import { loadStripe } from "@stripe/stripe-js";
import styles from "./RegisterForm.module.scss";

import { initialRegFormState, RegisterFormTypes, hasValidInputs } from "./types";
import HybridConference from "./HybridConference";
import FormInputs from "./FormInputs";
import Loader from "../Loader/Loader";

const RegisterForm: preact.FunctionComponent = () => {
  const [successCaptcha, setSuccessCaptcha] = useState(false);
  const [agreeToTerms, setAgreeToTerms] = useState(false);
  const [loading, setLoading] = useState(true);
  const [inPersonAttendee, setInPersonAttendee] = useState(true);
  const [url, setUrl] = useState<string>("")
  const [hideHybridQuestion, setHideHybridQuestion] = useState(true);
  const [stripePublicKey, setStripePublicKey] = useState("");
  const [recaptchaSiteKey, setRecaptchaSiteKey] = useState("");
  const [registerFormData, setRegisterFormData] = useState<RegisterFormTypes>(initialRegFormState);
  const [formErrors, setFormErrors] = useState({})


  useEffect(() => {
    // Secret Key was null without nesting it in useEffect
    setStripePublicKey(import.meta.env.PUBLIC_STRIPE_KEY);
    setRecaptchaSiteKey(import.meta.env.PUBLIC_RECAPTCHA_SITE_KEY);

    setUrl(window.location.pathname)
    // Disabling virtual for ASIA 
    // if (window.location.pathname.includes("sfasia")) {
    //   setHideHybridQuestion(false)
    // }
    setLoading(false)
  }, []);

  useEffect(() => {
    // Adding recaptcha script
    if (window && document && recaptchaSiteKey) {
      const script = document.createElement("script");
      const body = document.getElementsByTagName("body")[0];
      script.src = `https://www.google.com/recaptcha/api.js?render=${recaptchaSiteKey}`;
      body.appendChild(script);
    }
  }, [recaptchaSiteKey]);

  const stripePromise = stripePublicKey ? loadStripe(stripePublicKey) : null;

  const onRecaptchaChange = (e) => {
    e.preventDefault();

    grecaptcha.ready(function () {
      grecaptcha
        .execute(recaptchaSiteKey, { action: "submit" })
        .then(function (token) {
          wretchHelper
            .url("validate-human")
            .post({
              token,
            })
            .json((resp) => {
              handlingSuccessfulCaptchaValidation(resp.success);
              if (resp.success) {
                console.log(formErrors)
                if (Object.keys(formErrors).length > 0) {
                  return
                }
                createUser(registerFormData);
              }
            });
        });
    });
  };

  const onFormInputChange = (e) => {
    setRegisterFormData((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }));
  };

  const handlingSuccessfulCaptchaValidation = (success) => {
    // It wasn't letting me use the setSuccessCaptcha hook
    // directly after the POST request, so I had to make it
    // a separate function
    setSuccessCaptcha(success);
  };

  const handleAgreeToTerms = (e) => {
    if (e.target.checked) {
      setAgreeToTerms(true);
    } else {
      setAgreeToTerms(false);
    }
  };

  const createUser = async (data) => {
    setLoading(true);

    const firstTimeBoolean = data.first_time === "Yes" ? true : false;

    data.first_time = firstTimeBoolean;
    data.in_person = inPersonAttendee;
    data.agree_to_terms = agreeToTerms;
    data.consent_to_contact = data.agree_to_terms;

    const {
      name,
      business_title,
      email,
      phone,
      company,
      shirt,
      address,
      address2,
      country,
      city,
      state,
      postal_code,
      first_time,
      heard_about_sharkfest,
      type_of_registration,
      agree_to_terms,
      consent_to_contact,
      in_person,
      dietary_restrictions,
    } = data;

    wretchHelper
      .url("users")
      .post({
        name,
        business_title,
        email,
        phone,
        company,
        shirt,
        address,
        address2,
        country,
        city,
        state,
        postal_code,
        first_time,
        heard_about_sharkfest,
        type_of_registration,
        consent_to_contact,
        agree_to_terms,
        in_person,
        dietary_restrictions,
      })
      .forbidden(() => {
        alert(
          "The conference is sold out, but the pre-conference classes are still available!"
        );
        setLoading(false);
        return;
      })
      .json((resp) => {
        createCheckoutSession(data, resp.id);
      })
      .catch((err) => {
        const errorMsgs = [];
        err.message.split(/\]|\[|\"/).map((msg) => {
          if (msg.length >= 10) {
            errorMsgs.push(msg);
          }
        });
        alert(errorMsgs.join("\n"));
        setLoading(false);
        return;
      });
  };

  const createCheckoutSession = async (data, user_id) => {
    const stripe = await stripePromise;

    const {
      name,
      business_title,
      email,
      phone,
      company,
      shirt,
      address,
      address2,
      country,
      city,
      state,
      postal_code,
      first_time,
      heard_about_sharkfest,
      type_of_registration,
      consent_to_contact,
      agree_to_terms,
      in_person,
      dietary_restrictions,
    } = data;

    wretchHelper
      .url("checkout")
      .post({
        name,
        business_title,
        email,
        phone,
        company,
        shirt,
        address,
        address2,
        country,
        city,
        state,
        postal_code,
        first_time,
        heard_about_sharkfest,
        type_of_registration,
        user_id,
        consent_to_contact,
        agree_to_terms,
        in_person,
        dietary_restrictions,
      })
      .json(async (resp) => {
        setLoading(false);

        let session = resp;

        const result = stripe.redirectToCheckout({
          sessionId: session.id,
        });

        if ((await result).error) {
        }
      })
      .catch((err) => {
        setLoading(false);
      });
  };

  const handleHybridButtonClick = (e) => {
    e.target.innerText === "In Person"
      ? setInPersonAttendee(true)
      : setInPersonAttendee(false);
    setHideHybridQuestion(true);
  };

  if (loading) {
    return <Loader />
  }

  return (
    <div>
      {!hideHybridQuestion && (
        <HybridConference handleHybridButtonClick={handleHybridButtonClick} />
      )}
      {hideHybridQuestion && (
        <>
          <h1 className={styles.regHeader}>SharkFest Registration</h1>
          <form className={styles.registerForm}>
            <FormInputs
              onFormInputChange={onFormInputChange}
              inPersonAttendee={inPersonAttendee}
              windowUrl={url}
            />
            <div className={styles.termsAndConditions}>
              <label htmlFor="agree_to_terms">
                <input
                  type="checkbox"
                  id={styles.agreeToTerms}
                  name="agree_to_terms"
                  onChange={handleAgreeToTerms}
                  required
                />
                *By checking this box you are consenting for us to contact you
                via email in regards to future SharkFest conferences and all
                conference related information.
              </label>
            </div>
            <div className={styles.buttonWrapper}>
              <button
                className={`g-recaptcha primary-button ${styles.registerButton}`}
                data-sitekey={import.meta.env.PUBLIC_RECAPTCHA_SITE_KEY}
                onClick={onRecaptchaChange}
                type="submit"
                disabled={loading || !agreeToTerms || !hasValidInputs(registerFormData, inPersonAttendee)}
              >
                Submit
              </button>
            </div>
          </form>
          <p>Have questions or require assistance with registration? Email us at <a href="mailto:sharkfest@wireshark.org">sharkfest@wireshark.org</a></p>
        </>
      )}
    </div>
  );
};

export default RegisterForm;
