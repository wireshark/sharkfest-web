import styles from "./Loader.module.scss"

const Spinner: preact.FunctionComponent = () => {
  const defaultStyles = {
    border: "20px solid #f3f3f3",
    borderTop: "20px solid #3498db",
    width: "200px",
    position: "absolute",
    top: "0",
    bottom: "0",
    left: "0",
    right: "0",
    height: "200px",
    margin: "auto",
    borderRadius: "50%",
    animation: "spin 1s ease-in-out infinite",
  };

  return (
    <div className={styles.loaderWrapper}>
      <div className="loader" style={{ ...defaultStyles }}></div>
    </div>
  );
};

export default Spinner;
