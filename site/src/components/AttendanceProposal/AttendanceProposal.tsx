import styles from "./AttendanceProposal.module.scss"

const AttendanceProposal: preact.FunctionComponent = () => {

    return (
        <a className={styles.proposalLink} href={"/assets/SharkFest25US-Attendance-Proposal.docx"} target="_blank"><b>See Attendance Proposal Letter<img src="/icons-sf/phosphor/file-doc.svg" alt="Doc download icon" /></b></a>
    )
}

export default AttendanceProposal