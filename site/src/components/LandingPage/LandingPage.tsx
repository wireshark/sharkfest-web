import './LandingPage.css'

const LandingPage = () => {
	return (
		<div className="hybrid-wrapper">
			<div className="landing-wrapper">
				<div className="landing-button-wrapper">
					<a href="/sfus"><button className="sfvus-button">SharkFest'25 US <br /> <span>Richmond Marriott Downtown <br/>Richmond, VA <br />(June 14-19)</span></button></a>
				</div>
				<div className="landing-button-wrapper">
					<a href="/sfeu"><button className="sfveu-button">SharkFest'25 EUROPE <br /><span>Sheraton Grand Warsaw<br />Warsaw, Poland<br/>(3-7 November)</span></button></a>
				</div>
			</div>
			<div className="landing-wrapper">
					<div className="landing-button-wrapper">
						<a href="/retrospective"><button className="sfretro-button">SharkFest Retrospective <br /> <span>Check out content from past conferences!</span></button></a>
					</div></div>
		</div>
	);
}

export default LandingPage