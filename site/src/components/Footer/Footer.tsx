import styles from "./Footer.module.scss";
import { genericLinks } from "../../utils/links";

const Footer = () => {
  return (
    <footer>
      <div>
        <ul className={styles.footerSocial}>
          <li><a target="_blank" href="https://www.facebook.com/SHARKFEST-Wireshark-Network-Analysis-Conference-51502216870/"><img src="/socials/facebook.svg" width="40" height="40" alt="Facebook" rel="me" /></a></li>
          <li><a target="_blank" href="https://twitter.com/wiresharkfest"><img src="/socials/twitter.svg" width="40" height="40" alt="Twitter" rel="me" /></a></li>
          <li><a target="_blank" href="https://www.linkedin.com/groups/1802965"><img src="/socials/linkedin.svg" width="40" height="40" alt="LinkedIn" rel="me" /></a></li>
          <li><a target="_blank" href={genericLinks.sharkFestYouTube.url}><img src="/socials/youtube.svg" width="40" height="40" alt="YouTube" rel="me" /></a></li>
          <li><a target="_blank" href="https://ioc.exchange/@wireshark"><img src="/socials/mastodon.png" width="40" height="40" alt="Mastodon" rel="me" /></a></li>
          <li><a target="_blank" href="mailto:sharkfest@wireshark.org"><img src="/socials/email.svg" width="40" height="40" alt="Email" /></a></li>
        </ul>
        <a href="https://wiresharkfoundation.org/privacy-policy/"><p>Privacy Policy</p></a>
      </div>
    </footer>
  )
}

export default Footer
