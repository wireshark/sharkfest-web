import styles from "./Table.module.scss";

const RegTable: preact.FunctionComponent = () => {
  return (
    <table className={styles.sharkFestTable}>
      <tbody>
        <tr>
          <th scope="row"></th>
          {/* <th>Early Bird <br /> (through 31 July)</th> */}
          <th>Regular</th>
        </tr>
        <tr>
          <td scope="row">SharkFest Only</td>
          {/* <td>€995</td> */}
          <td>€1295</td>
        </tr>
        <tr>
          <td scope="row">Pre-Conference I</td>
          {/* <td>€845</td> */}
          <td>€1145</td>
        </tr>
        <tr>
          <td scope="row">Pre-Conference II</td>
          {/* <td>€645</td> */}
          <td>€945</td>
        </tr>
        <tr>
          <td scope="row">Pre-Conference III</td>
          {/* <td>€645</td> */}
          <td>€945</td>
        </tr>
      </tbody>
    </table>
  );
};

export default RegTable;
