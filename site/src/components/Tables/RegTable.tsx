import styles from "./Table.module.scss";

const RegTable: preact.FunctionComponent = () => {
  return (
    <table className={styles.sharkFestTable}>
      <tbody>
        <tr>
          <th scope="row"></th>
          <th>Registration Fee</th>
        </tr>
        <tr>
          <td scope="row">SharkFest Only</td>
          <td>$1295</td>
        </tr>
        <tr>
          <td scope="row">Pre-Conference I</td>
          <td>$1145</td>
        </tr>
        <tr>
          <td scope="row">Pre-Conference II</td>
          <td>$945</td>
        </tr>
        <tr>
          <td scope="row">Pre-Conference III</td>
          <td>$945</td>
        </tr>
      </tbody>
    </table>
  );
};

export default RegTable;
