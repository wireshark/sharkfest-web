import { Conference } from "src/utils/types";
import styles from "./Table.module.scss";

interface CancellationTableProps {
  conference?: Conference;
}

const CancellationTable: preact.FunctionComponent<CancellationTableProps> = ({ conference }) => {
  const isEUSite = conference === "sfeu"

  return (
    <table className={styles.sharkFestTable}>
        <thead>
            <th>Dates</th>
            <th>Refund Amount</th>
        </thead>
      <tbody>
        <tr>
          <td>{isEUSite ? "Until September 26th, 2025" : "Until May 26th, 2025"}</td>
          <td>Full Refund (Minus {isEUSite ? `€` : `$`}100 fee)</td>
        </tr>
        <tr>
          <td>{isEUSite ? "September 27th, 2025 or later" : "May 26th, 2025 or later"}</td>
          <td>No refund</td>
        </tr>
      </tbody>
    </table>
  );
};

export default CancellationTable;
