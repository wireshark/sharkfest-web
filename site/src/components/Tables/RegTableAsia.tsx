import styles from "./Table.module.scss";

const RegTableAsia: preact.FunctionComponent = () => {
  return (
    <>
      <table className={styles.sharkFestTable}>
        <tbody>
          <tr>
            <th scope="row">IN PERSON</th>
            <th>Regular</th>
          </tr>
          <tr>
            <td scope="row">SharkFest Only</td>
            <td>$795</td>
          </tr>
          <tr>
            <td scope="row">Pre-Conference I</td>
            <td>$595</td>
          </tr>
          <tr>
            <td scope="row">Pre-Conference II</td>
            <td>$595</td>
          </tr>
          <tr>
            <td scope="row">SharkFest + Pre-Conference I</td>
            <td>$1390</td>
          </tr>
          <tr>
            <td scope="row">SharkFest + Pre-Conference II</td>
            <td>$1390</td>
          </tr>
        </tbody>
      </table>
    </>
  );
};

export default RegTableAsia;
