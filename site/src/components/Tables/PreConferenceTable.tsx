import styles from "./Table.module.scss";

interface PreConferenceTableProps {
  instructor: string;
  dates: string;
  description: string;
  title: string;
  whoShouldAttend?: string;
  prerequesites?: string;
}

const PreConferenceTable: preact.FunctionComponent<PreConferenceTableProps> = ({
  instructor,
  dates,
  description,
  title,
  whoShouldAttend,
  prerequesites,
}) => {
  return (
    <div className={styles.preConferenceTable}>
      <p>{description}</p>
      <table className={styles.sharkFestTable}>
        <thead>
          <th colSpan={2}>{title}</th>
        </thead>
        <tbody>
          <tr>
            <td>Dates</td>
            <td>{dates}</td>
          </tr>
          <tr>
            <td>{instructor.includes("&") ? "Instructors" : "Instructor"}</td>
            <td>{instructor}</td>
          </tr>
          {whoShouldAttend &&
            <tr>
                <td>Who should attend?</td>
                <td>{whoShouldAttend}</td>
            </tr>
          }
          {prerequesites &&
            <tr>
                <td>Recommended Prerequisites</td>
                <td>{prerequesites}</td>
            </tr>
          }
        </tbody>
      </table>
    </div>
  );
};

export default PreConferenceTable;
