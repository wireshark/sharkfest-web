import styles from "./Button.module.scss";

interface ButtonProps {
  link: string;
  text: string;
  variant: string;
}

const Button: preact.FunctionComponent<ButtonProps> = ({ link, text, variant }) => {
  return (
    <div class={styles.buttonWrapper}>
      <a href={link}>
        <button class={`${styles[variant]} ${styles.primaryButton}`}>{text}</button>
      </a>
    </div>
  );
};

export default Button;
