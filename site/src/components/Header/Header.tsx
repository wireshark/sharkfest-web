import { useState } from "preact/hooks";
import { FunctionComponent } from "preact";
import Accordion from "../Accordion/Accordion";
import styles from "./Header.module.scss";
import { SFlinks } from "../../utils/links";
import { THEME_ASIA, THEME_EUROPE } from "src/utils/constants";

interface HeaderProps {
  isAsiaSite: boolean;
  isEUSite: boolean;
}

const Header: FunctionComponent<HeaderProps> = ({ isAsiaSite, isEUSite }) => {
  const [responsiveMenuOpen, setResponsiveMenuOpen] = useState<boolean>(false);
  const [responsiveSubMenuOpen, setResponsiveSubMenuOpen] =
    useState<boolean>(false);
  const [hoverMenuOpen, setHoverMenuOpen] = useState<boolean>(false);
  const [hoveredElementText, setHoveredElementText] = useState<string>("");

  let links;

  if (isAsiaSite || (typeof window !== "undefined" && localStorage?.getItem("theme")) === THEME_ASIA) links = SFlinks("sfasia")
  else if (isEUSite || (typeof window !== "undefined" && localStorage?.getItem("theme")) === THEME_EUROPE) links = SFlinks("sfeu")
  else links = SFlinks("sfus")

  const handleHamburgerClick = (): void => {
    setResponsiveMenuOpen(true);
  };

  const handleCloseResponsiveMenu = (): void => {
    setResponsiveMenuOpen(false);
    setResponsiveSubMenuOpen(false);
  };

  const onHoverAndClickMain = (e): void => {
    setHoverMenuOpen(true);
    setHoveredElementText(e.target.text);
  };

  const calculateHoveredMenu = () => {
    if (hoveredElementText === "Attend") {
      return renderSubMenu(links.attend.subLinks);
    }
  };

  const renderSubMenu = (subLinks) => {
    return subLinks.map((link, idx) => {
      if (link.url !== "false") {
        return (
          <a key={idx} href={link?.url} rel="noopenner noreferrer">
            <li>{link?.name}</li>
          </a>
        );
      }
    });
  };

  return (
    <nav className={styles.mainNav}>
      <a href={links.donate.url}>
        <div className={styles.callToAction}>
          Support the Wireshark Foundation & open source packet analysis by
          making a donation.
        </div>
      </a>
      <header className={styles.header}>
        <div className={styles.headerWrapper}>
          <a href="/">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width={60}
              height={60}
              viewBox="0 0 400 400"
            >
              <path d="M176.7 2C118.8 9.3 67.2 41.1 34.8 89.4c-44.2 66-44.7 152.1-1.2 219.4 3.6 5.7 6.8 10.1 7 9.9.3-.2-.2-3.5-1-7.3-4-18.9-5.1-31.7-5-56.9 0-21.2.4-26.8 2.2-37.3 9.1-50.1 32.1-91.5 73.3-131.7C139.3 57 175 31.7 217.8 9.2c7.3-3.8 10.9-6.2 10.1-6.7-1.8-1.1-43.1-1.5-51.2-.5zM229 13.1c-7.9 12-11.1 21.8-11.7 35.9-.9 18.2 3.2 34.1 14.6 57.1 22 44.2 68.9 97.2 139.1 157.2 7.4 6.3 13.8 11.3 14.2 11.1 1.3-.8 8-22.8 10.4-34.3 7.1-34.5 4.9-69.9-6.5-104.2-21.2-63.4-74.2-112.2-140-129-6.2-1.6-11.9-2.9-12.7-2.9-.8 0-4.1 4.1-7.4 9.1zM215.5 253.6c-8.9 1-26.5 4.7-35.5 7.6-37.5 11.9-70.8 38.5-96.7 77.3-6.1 9.1-9.7 15.5-9.1 15.9.2.2 3.7 2.8 7.8 5.8 53.2 38.6 119.9 49.2 181.5 28.7 47.5-15.8 85.3-47.3 111.8-93l2.7-4.7-7.8-3.7c-21.5-10.3-54-22.1-74.4-27-21.7-5.2-33.2-6.7-55.3-7-11.5-.2-22.8-.1-25 .1z" />
            </svg>
          </a>
          <ul>
            <li>
              <a target="_blank" href={links.wireshark.url}>
                {links.wireshark.name}
              </a>
            </li>
            <li>
              <a
                onClick={onHoverAndClickMain}
                onMouseEnter={onHoverAndClickMain}
              >
                {links.attend.name}
                <img
                  src="/icons-sf/angle-arrow-down.svg"
                  alt="Angle arrow down"
                />
              </a>
            </li>
            <li>
              <a href={links.shop.url}>{links.shop.name}</a>
            </li>
            <li>
              <a target="_blank" href={links.speakerSubmission.url}>
                {links.speakerSubmission.name}
              </a>
            </li>
            <li>
              <a href={links.sponsors.url}>{links.sponsors.name}</a>
            </li>
            <li>
              <a href={links.mailingList.url}>{links.mailingList.name}</a>
            </li>
            <li>
              <a href={links.donate.url}>{links.donate.name}</a>
            </li>
          </ul>
          <a href={links.registrationOptions.url}>
            <button type="button" className={styles.primaryButton}>
              {links.register.name}
            </button>
          </a>
          <img
            onClick={handleHamburgerClick}
            className={styles.headerHamburger}
            alt="Hamburger icon for responsive menu"
            src="/icons-sf/hamburger.svg"
          />
        </div>
        {responsiveMenuOpen ? (
          <div className={styles.responsiveHeader}>
            <span onClick={handleCloseResponsiveMenu}>X</span>
            <ul className={styles.responsiveList}>
              <li>
                <a target="_blank" href={links.wireshark.url}>{links.wireshark.name}</a>
              </li>
              <li>
                <a>
                  <Accordion
                    title={links.attend.name}
                    headerContent={links.attend}
                    variant="simple"
                  />
                </a>
              </li>
              <li>
                <a href={links.shop.url}>{links.shop.name}</a>
              </li>
              <li>
                <a href={links.sponsors.url}>{links.sponsors.name}</a>
              </li>
              <li>
                <a target="_blank" href={links.speakerSubmission.url}>
                  {links.speakerSubmission.name}
                </a>
              </li>
              <li>
                <a href={links.mailingList.url}>{links.mailingList.name}</a>
              </li>
              <li>
                <a href={links.registrationOptions.url}>
                  {links.registrationOptions.name}
                </a>
              </li>
              <li>
                <a href={links.donate.url}>{links.donate.name}</a>
              </li>
            </ul>
          </div>
        ) : null}
      </header>
      {hoverMenuOpen ? (
        <div
          className={styles.hoveredMenu}
          onMouseLeave={() => setHoverMenuOpen(false)}
        >
          <ul>{calculateHoveredMenu()}</ul>
        </div>
      ) : null}
    </nav>
  );
};

export default Header;
