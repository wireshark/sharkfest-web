import { h, Fragment } from 'preact';
import "./HeaderLanding.scss";

const Header = () => {
  return (
    <header>
      <div className="header-text-wrapper">
        <h1>SharkFest'25</h1>
        <h3>Wireshark Developer and User Conference</h3>
      </div>
    </header>
  )
}

export default Header