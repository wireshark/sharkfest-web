import { useEffect, useState } from "preact/hooks"
import { wretchHelper } from "../../utils/wretchHelper";
import useGetWindowInfo from "../../hooks/useGetWindowInfo";
import { THEME_NORMAL } from "../../utils/constants";
import Loader from "../Loader/Loader"
import LodgingForm from "../Lodging/LodgingForm";

const SuccessPage: preact.FunctionComponent = () => {
    const [loading, setLoading] = useState(false)

    const { theme } = useGetWindowInfo();

    useEffect(() => {
        const urlParams = new URLSearchParams(window.location.search)
        setLoading(true)
  
        wretchHelper
        .url(`success?session_id=${urlParams.get('session_id')}`)
        .get()
        .json(resp => { 
          setLoading(false)
        })
        .catch(err => {
          setLoading(false)
        })
    }, []);

    if (loading) {
      return <Loader />
    }

    return (
      <div class="container">
        <h1>Thank you for registering for SharkFest!</h1>
        <h4>We look forward to welcoming you at the conference soon. Look out for emails with more info about the event as we get closer to the start date.</h4>
        {theme === THEME_NORMAL && (
          <>
            <h3>Attending SFUS and want to book a dorm room at USD?</h3>
            <p>To get more information on the dorm rooms before booking, please visit our <a href="/sfus/lodging/">lodging page</a>.</p>
            <LodgingForm />
          </>
        )}
        <p>Please contact <a href="mailto:sharkfest@wireshark.org">sharkfest@wireshark.org</a> with any further questions.</p>
      </div>
    )
}

export default SuccessPage