import wretch from 'wretch'

const wretchHelper = wretch()
  // Set the base url
  // .url("http://localhost:3001/api/v1/")
  // .url("https://sharkfestapi.up.railway.app/api/v1/")
  .url("https://sharkfest21-eu-reg-api.herokuapp.com/api/v1/")

export { wretchHelper }