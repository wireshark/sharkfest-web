import { loadStripe } from '@stripe/stripe-js';

export const stripePromise = loadStripe(import.meta.env.PUBLIC_STRIPE_KEY)