import { Conference } from "./types";

export const genericLinks = {
  sharkFest: {
    name: "SharkFest",
    url: "/",
  },
  mailingList: {
    name: "Mailing List",
    url: "/mailing-list",
  },
  wireshark: {
    name: "Wireshark",
    url: "https://www.wireshark.org",
  },
  sharkFestYouTube: {
    name: "SharkFest YouTube",
    url: "https://www.youtube.com/c/SharkFestWiresharkDeveloperandUserConference",
  },
  shop: {
    name: "Shop",
    url: "https://wiresharkfoundation.org/shop/#!/",
  },
  donate: {
    name: "Donate",
    url: "https://wiresharkfoundation.org/donate/",
  },
  speakerSubmission: {
    name: "Speaker Submission",
    url: "https://conference.wireshark.org/sharkfest-25-us-2024/cfp",
  }
};

export const SFlinks = (conference: Conference) => {
  const isSFUS = conference === "sfus"
  const isSFEU = conference === "sfeu"

  return (
    {
      ...genericLinks,
      speakerSubmission: {
        name: "Submit a talk",
        url: `${isSFUS ? "https://conference.wireshark.org/sharkfest-25-us-2024/cfp" : "https://conference.wireshark.org/sharkfest-25-europe-2025/cfp"}`,
      },
      sponsors: {
        name: "Sponsors",
        url: `/${conference}/sponsors`
      },
      register: {
        name: "Register",
        url: `/${conference}/register`
      },
      registrationOptions: {
        name: "Register",
        url: `/${conference}/registration-options`
      },
      attend: {
        name: "Attend",
        subLinks: [
          {
            name: "About",
            url: "/about"
          },
          {
            name: "Agenda",
            url: `/${conference}/agenda`
          },
          {
            name: "Register",
            url: `/${conference}/registration-options`
          },
          {
            name: "Lodging",
            url: `/${conference}/lodging`
          },
          {
            name: `${isSFUS && "Women in Tech Scholarship"}`,
            url: `${isSFUS && "/sfus/women-in-tech"}`
          },
          {
            name: `${isSFEU && "Women in Tech Scholarship"}`,
            url: `${isSFEU && "https://forms.gle/uDq3wREW56zVhKj26"}`
          },
          {
            name: "Retrospective",
            url: "/retrospective"
          },
        ],
      },
    })
}

export const stripeRedirects = {
  localSuccess: "http://localhost:3000/thankyou",
  localCancel: "http://localhost:3000/donate",
  prodSuccess: "https://sharkfest.wireshark.org/thank-you",
  prodCancel: "https://sharkfest.wireshark.org/donate",
  prodSuccessLodging: "https://sharkfest.wireshark.org/success-lodging",
  prodCancelLodging: "https://sharkfest.wireshark.org/sfus/lodging",
};

export const sponsorLinks = [
  {
    url: "https://sysdig.com",
    name: "Sysdig",
    imgPath: "img/sponsors/sysdig-white.png",
  },
  {
    url: "https://scos.nl/",
    name: "SCOS",
    imgPath: "img/sponsors/scos-white.png",
  },
  {
    url: "https://endace.com",
    name: "Endace",
    imgPath: "img/sponsors/endace.png",
  },
  {
    url: "https://www.fmad.io/",
    name: "fmadio",
    imgPath: "img/sponsors/fmadio.png",
  },
];

export const paymentLinks = {
  lodgingSF23US: [
    {
      text: "3 nights",
      priceSingle: "$204",
      priceDouble: "$156",
      priceIdDouble: import.meta.env.PUBLIC_DOUBLE_3_DAY_STAY,
      priceIdSingle: import.meta.env.PUBLIC_SINGLE_3_DAY_STAY,
    },
    {
      text: "4 nights",
      priceSingle: "$272",
      priceDouble: "$208",
      priceIdDouble: import.meta.env.PUBLIC_DOUBLE_4_DAY_STAY,
      priceIdSingle: import.meta.env.PUBLIC_SINGLE_4_DAY_STAY,
    },
    {
      text: "5 nights",
      priceSingle: "$340",
      priceDouble: "$260",
      priceIdDouble: import.meta.env.PUBLIC_DOUBLE_5_DAY_STAY,
      priceIdSingle: import.meta.env.PUBLIC_SINGLE_5_DAY_STAY,
    },
    {
      text: "6 nights",
      priceSingle: "$408",
      priceDouble: "$312",
      priceIdDouble: import.meta.env.PUBLIC_DOUBLE_6_DAY_STAY,
      priceIdSingle: import.meta.env.PUBLIC_SINGLE_6_DAY_STAY,
    },
    {
      text: "7 nights",
      priceSingle: "$472",
      priceDouble: "$364",
      priceIdDouble: import.meta.env.PUBLIC_DOUBLE_7_DAY_STAY,
      priceIdSingle: import.meta.env.PUBLIC_SINGLE_7_DAY_STAY,
    },
  ],
};

export const foundationLinks = {
  privacyPolicy: "https://wiresharkfoundation.org/privacy-policy/",
};
