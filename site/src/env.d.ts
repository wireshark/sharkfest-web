/// <reference path="../.astro/types.d.ts" />
/// <reference types="astro/client" />

// Env variables for intellisense autofill go here
interface ImportMetaEnv {
    readonly PUBLIC_STRIPE_TEST: string;
    readonly PUBLIC_STRIPE_KEY: string;
    readonly PUBLIC_RECAPTCHA_SITE_KEY: string;
    readonly PUBLIC_SINGLE_3_DAY_STAY: string;
    readonly PUBLIC_SINGLE_4_DAY_STAY: string;
    readonly PUBLIC_SINGLE_5_DAY_STAY: string;
    readonly PUBLIC_SINGLE_6_DAY_STAY: string;
    readonly PUBLIC_SINGLE_7_DAY_STAY: string;
    readonly PUBLIC_DOUBLE_3_DAY_STAY: string;
    readonly PUBLIC_DOUBLE_4_DAY_STAY: string;
    readonly PUBLIC_DOUBLE_5_DAY_STAY: string;
    readonly PUBLIC_DOUBLE_6_DAY_STAY: string;
    readonly PUBLIC_DOUBLE_7_DAY_STAY: string;
}
  
interface ImportMeta {
    readonly env: ImportMetaEnv;
}